module.exports = {
  purge: ['./src/**/*.{js,jsx,ts,tsx}', './public/index.html'],
  darkMode: false, // or 'media' or 'class'
  theme: {
    textColor: {
      'primary': '#3490dc',
      'secondary': '#ffed4a',
      'danger': '#e3342f',
      'orange': '#FFA500',
      'blue': '#263A73',
      'basecolor': '#B20841',
      'agentGreen': '#028E7C',
      'j-magenta': '#500E4B',
      "j-orange": "#EA725B",
      "j-gray": "#666666",
      "j-gray-lighter": "#CCCCCC",
      "j-red-light": "#AA432F",
      "j-green": "#378649",
      "j-penBlue": "#473CC7"
    },
    extend: {},
    backgroundColor: theme => ({
      ...theme('colors'),
      'primary': '#3490dc',
      'secondary': '#ffed4a',
      'danger': '#e3342f',
      'orange': '#FFA500',
      'blue': '#263A73',
      'basecolor': '#B20844',
      'cardbg': '#FCFCFC',
      'grey': '#E1E1E1',
      'agentGreen': '#028E7C',
      'magenta': '#500E4B',
      'blue-lighter': "#F0F4F9",
      "j-red-light": "#AA432F",
      "j-orange": "#EA725B",
      "j-gray-lighter": "#CCCCCC",
      "j-ghost-white": "#f9fafc"
     }),
    gradientColorStops: theme => ({
      ...theme('colors'),
      'primary': '#3490dc',
      'secondary': '#ffed4a',
      'danger': '#e3342f',
      'greenlight': '#0E7A7A',
      'greendark': '#3BC7DD',
      'pinklight': '#D800FF',
      'pinkdark': '#8400FF',
      'bluelight': '#457DF4',
      'bluedark': '#711FFC',
      "j-orange": "#EA725B",
      "j-red-light": "#AA432F",
     }),
     borderColor: theme => ({
      ...theme('colors'),
     'primary': '#3490dc',
     'secondary': '#ffed4a',
     'danger': '#e3342f',
     'j-magenta': '#500E4B',
     "j-gray": "#666666",
     "j-gray-lighter": "#CCCCCC",
     "j-orange": "#EA725B",
     }),
     fontSize: {
      tiny: "0.625rem",
      huge: "2rem",
      biggest: "2.5rem",
      gigantic: "5.625rem",
      "4xl": "2.375rem",
    },
    
  },
  variants: {
    extend: {
      opacity: ['disabled'],
    }
  },
  plugins: [],
}

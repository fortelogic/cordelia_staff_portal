import React, { useState, useEffect, useMemo, useRef } from "react";
import Pagination from "@material-ui/lab/Pagination";
import { useTable, useExpanded } from "react-table";
import ReactLoading from 'react-loading';
import moment from "moment";
import { useHistory } from "react-router-dom";
import Badge from '@material-ui/core/Badge';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import {
  withStyles,
} from '@material-ui/core/styles';

var dummySetdata= [];
const ReactTableLeads = ({tableData, loading, renderRowSubComponent, totalPages, bookingListApi}) => {
 
  let history = useHistory();
  let moreData;
  const [newtableData, setNewTableData] = useState(tableData)
  const [page, setPage] = useState(1);
  const [count, setCount] = useState(0);
  const [search, setSearch] = useState("");
  const [showFilter, setShowFilter] = useState(false);
  const [openTab, setOpenTab] = useState(1);
  const [sDate, setsDate] = useState("");
  const [eDate, seteDate] = useState("");
  const [cDate, setCruiceDate] = useState("");
  const [source, setSource] = useState([]);
  const [open, setOpen] = React.useState(false);
  const [isExpand,setisExapndable] = React.useState()

  useEffect(()=> {
    callSetDataFunction()
  })

  const handleClickOpen = (newtableData) => {
    console.log(newtableData)
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const callSetDataFunction = () => {
    console.log(totalPages)
    setCount(totalPages)
    setNewTableData(tableData)
  }
  console.log(newtableData)

  let data = newtableData;

  const columns = React.useMemo(
    () => [
      {
        Header: 'Quotation ID',
        accessor: 'id', // accessor is the "key" in the data
      },
      {
        Header: 'Name',
        accessor: 'name', // accessor is the "key" in the data
        maxWidth: 150,
        Cell: (props) => {
          return <p style={{width:'120px'}}>{props.value}</p>
        }
      },
      {
        Header: 'Email ID',
        accessor: 'email', // accessor is the "key" in the data
        Cell: (props) => {
          return <p style={{width:'120px'}}>{props.value}</p>
        }
      },
      {
        Header: 'Contact Number',
        accessor: 'contact_phone', // accessor is the "key" in the data
      },
      {
        Header: 'Total Quotation',
        accessor: 'total_quotations', 
        width: 60,
        Cell: (props) => {
          return (<>
              <div className="flex justify-center"><Badge badgeContent={props.value} color="error" /></div>
          </>)
        },
      },
      {
        Header: () => null,
        id: 'expander',
        Cell: ({row}) => {
          // const rowIdx = props.row.id;
          return (
            <div {...row.getToggleRowExpandedProps()}>
              <button className="bg-j-orange text-white p-2" onClick={()=>handleClickOpen()}>Details</button>
            </div>
          );
        },
      },
    ],
    []
  )

  const setExapand = () => {
    setisExapndable(!isExpand)
  }

  const handlePageChange = (event, value) => {
    console.log(value)
      // setPage(value);
      // bookingListApi(value,true, search, sDate, eDate, cDate, null)
  };

const tableInstance = useTable({ columns, data }, useExpanded)
 const {
   getTableProps,
   getTableBodyProps,
   headerGroups,
   rows,
   prepareRow,
   visibleColumns,
   state: { expanded },
 } = tableInstance
      return (
        <div className="list row">
            <div className="col-md-8 mr-20">
              <div
                className={
                  "relative flex flex-col min-w-0 break-words w-full mb-6 shadow-lg rounded bg-white"
                }
              >
                <div className="block w-full overflow-x-auto">
                  <table
                    className="table table-striped table-bordered w-full"
                    {...getTableProps()}
                  >
                    <thead>
                      {headerGroups.map((headerGroup) => (
                        <tr {...headerGroup.getHeaderGroupProps()}>
                          {headerGroup.headers.map((column) => (
                            <th className="px-6 align-middle border border-solid py-3 text-xs uppercase border-l-0 border-r-0 whitespace-no-wrap font-semibold text-left bg-j-orange text-white border-gray-200"
                            {...column.getHeaderProps({
                              style: { width: column.width}
                            })}>
                            {column.render("Header")}
                            </th>
                          ))}
                        </tr>
                      ))}
                    </thead>
                    <tbody {...getTableBodyProps()}>
                      {(rows.map((row, i) => {
                        prepareRow(row);
                        return (
                          <React.Fragment>
                            <tr {...row.getRowProps()}>
                              {row.cells.map((cell) => {
                                return (
                                  <td className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs p-4 border-b" {...cell.getCellProps()}>{cell.render("Cell")}</td>
                                );
                              })}
                          </tr>
                          {row.isExpanded ? (
                            <tr>
                              <td colSpan={visibleColumns.length}>
                                  {renderRowSubComponent({ tableData, i })}
                                </td>
                            </tr>
                          ):null}
                        </React.Fragment>
                        );
                      })
                       )}
                       {loading ? <tr className="w-full h-full">
                            <td colspan="6"><div className="opacity-60 flex justify-center"><ReactLoading type={'bubbles'} color={'blue'} /></div></td>
                         </tr> : null}
                    </tbody>
                  </table>
                  <div className="pt-2 flex justify-end mr-6">
                    <Pagination
                      className="my-3"
                      count={count}
                      page={page}
                      siblingCount={1}
                      boundaryCount={1}
                      shape="rounded"
                      onChange={handlePageChange}
                      color="primary"
                    />
                  </div>
                </div>
              </div>
            </div> 
        </div>
      )
}

export default ReactTableLeads;

import React, { Component, useRef, useMemo, useState, useEffect } from "react";
import ReactLoading from 'react-loading';
import Pagination from "@material-ui/lab/Pagination";
import { useTable, useExpanded } from "react-table";
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    ul: {
        // "& .MuiPaginationItem-root": {
        //   backgroundColor: "#500E4B",
        //   color:'#fff'
        // }
    }
  }));

export default function ReactTableComponent({columns, data, loading, renderRowSubComponent, count, page, handlePageChange}) {

    const classes = useStyles();

    console.log(loading)

    const tableInstance = useTable({ columns, data },  useExpanded)
    const {
        getTableProps,
        getTableBodyProps,
        headerGroups,
        rows,
        prepareRow,
        visibleColumns,
        state: { expanded },
      } = tableInstance

    return (
        <>
            <div className="w-full lg:w-5/6">
                <div className="bg-white shadow-lg rounded my-4">
                    <table class="w-full table-auto" {...getTableProps()}>
                        <thead>
                            {headerGroups.map((headerGroup) => (
                                <tr class="bg-gray-300 text-j-magenta text-sm" {...headerGroup.getHeaderGroupProps()}>
                                    {headerGroup.headers.map((column) => (
                                        <th class="py-3 px-6 text-left" 
                                            {...column.getHeaderProps({
                                                style: { maxWidth: column.maxWidth}
                                            })}>{column.render("Header")}</th>
                                    ))}
                                </tr>
                            ))}
                                    </thead>
                                    <tbody class="text-gray-600 text-sm font-light" {...getTableBodyProps()}>
                                        {(rows.map((row, i) => {
                                            prepareRow(row);
                                            return (
                                                <React.Fragment>
                                                    <tr class="border-b border-gray-200 hover:bg-gray-100" {...row.getRowProps()}>
                                                        {row.cells.map((cell) => {
                                                        return (
                                                            <td class="py-3 px-6 text-left text-gray-900" {...cell.getCellProps()}>
                                                                {cell.render("Cell")}
                                                            </td>
                                                        )})}
                                                    </tr>
                                                    {row.isExpanded ? (
                                                        <tr>
                                                        <td colSpan={visibleColumns.length}>
                                                            {renderRowSubComponent({ i })}
                                                            </td>
                                                        </tr>
                                                    ):null}
                                                </React.Fragment>
                                            )
                                        }))}
                                        {loading ? <tr class="border-b border-gray-200 hover:bg-gray-100">
                                            <td class="py-3 px-6 text-left whitespace-nowrap text-gray-900" colSpan="6"><div className="opacity-60 flex justify-center"><ReactLoading type={'bubbles'} color={'#500E4B'} /></div></td>
                                        </tr> : null}
                                    </tbody>
                                </table>
                                {data.length == 0 && <div className="w-full">
                                    {!loading && <p className="w-full text-center text-4xl text-gray-400 pt-10 pb-10">No Result Found</p>}
                                    </div>
                                }
                            </div>
                            <div className="pt-2 flex justify-end mr-6">
                                <Pagination
                                    className="my-3"
                                    count={count}
                                    page={page}
                                    siblingCount={1}
                                    boundaryCount={1}
                                    shape="rounded"
                                    onChange={handlePageChange}
                                    color="primary"
                                />
                            </div>
                        </div>
        </>
    )
}
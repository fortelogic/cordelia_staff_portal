import React, { useState, useEffect, useMemo, useRef } from "react";
import Pagination from "@material-ui/lab/Pagination";
import { useTable, useExpanded } from "react-table";
import ReactLoading from 'react-loading';
import moment from "moment";
import { useHistory } from "react-router-dom";
import Alert from '@material-ui/lab/Alert';

var dummySetdata= [];
const ReactTable = ({tableData, loading, renderRowSubComponent, totalPages, bookingListApi}) => {
 
  let history = useHistory();
  let moreData;
  const [newtableData, setNewTableData] = useState(tableData)
  const [page, setPage] = useState(1);
  const [count, setCount] = useState(0);
  const [search, setSearch] = useState("");
  const [showFilter, setShowFilter] = useState(false);
  const [openTab, setOpenTab] = useState(1);
  const [sDate, setsDate] = useState("");
  const [eDate, seteDate] = useState("");
  const [cDate, setCruiceDate] = useState("");
  const [source, setSource] = useState([]);
  const [checkEmpty, setCheckEmpty] = useState(false);

  useEffect(()=> {
    callSetDataFunction()
  })

  const viewDetails = (id) => {
    history.push({
      pathname: '/admin/booking-details',
      state: { id: id }
    })
  }

  const callSetDataFunction = () => {
    console.log(totalPages)
    setCount(totalPages)
    setNewTableData(tableData)
  }
  console.log(newtableData)

  let data = newtableData;

  const columns = React.useMemo(
    () => [
      {
        Header: () => null,
        id: 'expander',
        Cell: ({ row }) => (
          <span {...row.getToggleRowExpandedProps()}>
            {row.isExpanded ? <i className="fas fa-sort-up"></i> : <i className="fas fa-caret-down"></i>}
          </span>
        ),
      },
      {
        Header: 'Ref Number',
        accessor: 'number',
      },
      {
        Header: 'Cruice name',
        accessor: 'name',
        maxWidth: 150,
        Cell: (props) => {
          return <p style={{width:'150px'}}>{props.value}</p>
        }
      },
      {
        Header: 'Booing Date',
        accessor: 'booked_date',
      },
      {
        Header: 'Status',
        accessor: 'status', 
        Cell: (props) => {
          return (<>
            {props.value=="RESERVED" &&  <>
              <i className="fas fa-circle text-orange-500 mr-2"></i>Reserved
            </>}
            {props.value=="CONFIRMED" &&  <>
              <i className="fas fa-circle text-green-500 mr-2"></i>Confirmed
            </>}
            {props.value=="CANCELLED" &&  <>
              <i className="fas fa-circle text-red-500 mr-2"></i>Cancelled
            </>}
            {props.value=="MODIFIED" &&  <>
              <i className="fas fa-circle text-blue-500 mr-2"></i>Modified
            </>}
          </>)
        },
      },
      {
        Header: "",
        accessor: "id",
        Cell: (props) => {
          return (
            <div>
              <button className="bg-j-orange text-white p-2" onClick={()=>viewDetails(props.value)}>View Details</button>
            </div>
          );
        },
      },
    ],
    []
  )


const handleStartReset = (e) => {
    e.preventDefault();
    e.target.reset();
    setsDate("")
    seteDate("")
    // setCruiceDate("")
    // bookingListApi(1, moreData = false,search, "", "", cDate, null)
}

const handleCruiceReset = (e) => {
  e.preventDefault();
  e.target.reset();
  setCruiceDate("")
  // bookingListApi(1, moreData = false,search, sDate, eDate, "", null)
}

const handleBookingSourceReset = (e) => {
  e.preventDefault();
  e.target.reset();
  setSource([])
  // bookingListApi(1, moreData = false, search, sDate, eDate, "", null)
}

const showFilterToggle = () => {
  // setsDate("")
  // seteDate("")
  // setCruiceDate("")
  setShowFilter(!showFilter);
}

const handleBookingSourceChange = (value) => {
  setSource(value)
  // bookingListApi(1, search, moreData = false, sDate, eDate, cDate, value)
}

const handlePageChange = (event, value) => {
  console.log(value)
    setPage(value);
    bookingListApi(value,true, search, sDate, eDate, cDate, null)
};

const searchhandleSubmit = (e) => {
  e.preventDefault();
  setSearch("")
  e.target.reset();
  bookingListApi(1,moreData = false, "", sDate, eDate, cDate, null)
}

const searchData = (e) => {
  if(search=="" && sDate=="" && eDate=="" && cDate=="") {
    setCheckEmpty(true)
  } else {
    setCheckEmpty(false)
    bookingListApi(1,moreData = false, search, sDate, eDate, cDate, null)
  }
}

const tableInstance = useTable({ columns, data },  useExpanded)
 
 const {
   getTableProps,
   getTableBodyProps,
   headerGroups,
   rows,
   prepareRow,
   visibleColumns,
   state: { expanded },
 } = tableInstance
      return (
        <div className="list row">
            <div className="col-md-8 mr-20">
              <div
                className={
                  "relative flex flex-col min-w-0 break-words w-full mb-6 shadow-lg rounded bg-white"
                }
              >
                <div className="flex mb-6">
                  <form onSubmit={searchhandleSubmit} className="w-full flex border-2 rounded-lg ml-10 mr-6">
                    <input className="w-full rounded p-2 " type="text" placeholder="Try 'Los Angeles' or 'Refernce No' or 'Date'" onChange={e => setSearch(e.target.value)} />
                    {search.length > 0 &&
                      <button className=" text-white p-2 pl-4 pr-4">
                        <span className="w-auto flex justify-end items-center text-gray-500 p-2">
                          <i className="fas fa-times-circle text-2xl"></i>
                        </span>
                      </button>
                    }
                  </form>
                  <button className="bg-j-orange hover:bg-red-300 rounded text-white p-2 pl-4 pr-4" onClick={searchData}>
                    <p className="font-semibold text-xs">Search</p>
                  </button>
                  <button type="button" class="btn btn-light mr-2 w-60 shadow-sm" onClick={showFilterToggle}>Filters <i class="fa fa-filter"></i></button>
                </div>
                {checkEmpty && <div className="ml-10 mr-20 mb-6">
                  <Alert severity="error">Please select Search/Any filter!</Alert>
                </div>}
                <div>
                {showFilter && <div className="flex mb-6 ml-4">
                <div className="w-9/12 flex">
                  <div className="bg-white w-full rounded ml-2">
                    <div className="px-4 py-1">
                      <form onSubmit={handleStartReset}>
                        <div className={openTab === 1 ? "flex" : "hidden"} id="link1">
                          <div className="w-5/12 ">
                            <label className="pl-4 text-xs">Start Date:</label>
                            <input
                              type="date"
                              className="h-8 rounded-lg p-4 ml-2 border"
                              onChange={e => setsDate(e.target.value)}
                            />
                          </div>
                          <div className="w-5/12">
                            <React.Fragment>
                              <label className="pl-4 text-xs">End Date:</label>
                              <input
                                type="date"
                                className="h-8 rounded-xl mr-6 ml-2 p-4 border"
                                min={moment(sDate).format(
                                  "YYYY-MM-DD"
                                )}
                                onChange={e => seteDate(e.target.value)}
                              />
                            </React.Fragment>
                          </div>
                          <div className="w-2/12 self-center">
                            <button className="bg-j-orange hover:bg-red-300 rounded text-white p-2 pl-4 pr-4 mt-6">
                              <p className="font-semibold text-xs">Reset Date</p>
                            </button>
                          </div>
                        </div>
                      </form>
                      <form onSubmit={handleCruiceReset}>
                        <div className={openTab === 2 ? "flex" : "hidden"} id="link2">
                          <div className="w-5/12">
                            <label className="pl-4 text-xs">Cruice Date:</label>
                            <input
                              type="date"
                              className="h-8 rounded-lg p-4 ml-2 border"
                              onChange={e => setCruiceDate(e.target.value)}
                            />
                          </div>
                          <div className="w-2/12 self-center">
                            <button className="bg-j-orange hover:bg-red-300 rounded text-white p-2 pl-4 pr-4 text mt-6">
                              <p className="font-semibold text-xs">Reset Date</p>
                            </button>
                          </div>
                        </div>
                    </form>
                    <form onSubmit={handleBookingSourceReset}>
                      <div className={openTab === 3 ? "flex" : "hidden"} id="link3">
                        <div className="w-5/12">
                          <label className="text-xs">Booking Source</label>
                          <select
                            name="booking_source"
                            id="booking_source"
                            style={{
                              width: "100%",
                              padding: "0.625rem",
                              borderRadius: "0.625rem",
                              boxShadow:
                                "0 1px 3px 0 rgba(0, 0, 0, 0.1), 0 1px 2px 0 rgba(0, 0, 0, 0.06)",
                            }}
                            placeholder="Booking Source"
                            defaultValue={source}
                            onChange={e => handleBookingSourceChange(e.target.value)}

                          >
                            <option value={[]} selected disabled>Choose Here</option>
                            <option value="api">API</option>
                            <option value="b2b">B2B</option>
                          </select>
                        </div>
                        <div className="w-2/12 self-center">
                          <button className="bg-j-orange hover:bg-red-300 rounded text-white p-2 pl-4 pr-4 text mt-6 ml-2">
                            <p className="font-semibold text-xs">Reset</p>
                          </button>
                        </div>
                      </div>
                    </form>
                    </div>
                  </div>
                </div>
                  <div className="text-center bg-white w-3/12 self-center mr-14">
                    <a
                      className={
                        "text-xs font-bold uppercase px-5 py-3 block leading-normal" +
                        (openTab === 1
                          ? "text-white bg-j-orange"
                          : "text-black bg-grey")
                      }
                      onClick={e => {
                        e.preventDefault();
                        setOpenTab(1);
                      }}
                      data-toggle="tab"
                      href="#link1"
                      role="tablist"
                    >
                      Filter by date
                    </a>
                    <a
                      className={
                        "text-xs font-bold uppercase px-5 py-3 block leading-normal" +
                        (openTab === 2
                          ? "text-white bg-j-orange"
                          : "text-black bg-grey")
                      }
                      onClick={e => {
                        e.preventDefault();
                        setOpenTab(2);
                      }}
                      data-toggle="tab"
                      href="#link2"
                      role="tablist"
                    >
                      Filter by cruise date
                    </a>
                    <a
                      className={
                        "text-xs font-bold uppercase px-5 py-3 block leading-normal" +
                        (openTab === 3
                          ? "text-white bg-j-orange"
                          : "text-black bg-grey")
                      }
                      onClick={e => {
                        e.preventDefault();
                        setOpenTab(3);
                      }}
                      data-toggle="tab"
                      href="#link3"
                      role="tablist"
                    >
                    Filter by Source
                  </a>
                  </div>
                </div>
                }
                </div>
                <div className="block w-full overflow-x-auto">
                  <table
                    className="table table-striped table-bordered w-full"
                    {...getTableProps()}
                  >
                    <thead>
                      {headerGroups.map((headerGroup) => (
                        <tr {...headerGroup.getHeaderGroupProps()}>
                          {headerGroup.headers.map((column) => (
                            <th className="px-6 align-middle border border-solid py-3 text-xs uppercase border-l-0 border-r-0 whitespace-no-wrap font-semibold text-left bg-j-orange text-white border-gray-200"
                            {...column.getHeaderProps({
                              style: { maxWidth: column.maxWidth}
                            })}>
                            {column.render("Header")}
                            </th>
                          ))}
                        </tr>
                      ))}
                    </thead>
                    {newtableData.length!=0 && <tbody {...getTableBodyProps()}>
                      {(rows.map((row, i) => {
                        prepareRow(row);
                        return (
                          <React.Fragment>
                            <tr {...row.getRowProps()}>
                              {row.cells.map((cell) => {
                                return (
                                  <td className="border-t-0 px-6 align-middle border-l-0 border-r-0 text-xs p-4 border-b" {...cell.getCellProps()}>{cell.render("Cell")}</td>
                                );
                              })}
                          </tr>
                          {row.isExpanded ? (
                            <tr>
                              <td colSpan={visibleColumns.length}>
                                  {renderRowSubComponent({ tableData, i })}
                                </td>
                            </tr>
                          ):null}
                        </React.Fragment>
                        );
                      })
                       )}
                       {loading ? <tr className="w-full h-full">
                            <td colspan="6"><div className="opacity-60 flex justify-center"><ReactLoading type={'bubbles'} color={'blue'} /></div></td>
                         </tr> : null}
                    </tbody>
                  }
                  </table>
                  {newtableData == 0 && <div className="w-full">
                    {loading ? <div className="opacity-60 flex justify-center"><ReactLoading type={'bubbles'} color={'blue'} /></div> : <p className="w-full text-center text-4xl text-gray-400 pt-10 pb-10">No Result Found</p>}</div>}
                  <div className="pt-2 flex justify-end mr-6">
                    <Pagination
                      className="my-3"
                      count={count}
                      page={page}
                      siblingCount={1}
                      boundaryCount={1}
                      shape="rounded"
                      onChange={handlePageChange}
                      color="primary"
                    />
                  </div>
                </div>
              </div>
            </div>
        </div>
      )
}

export default ReactTable;

import React, { Component, useRef, useMemo, useState, useEffect } from "react";

export default function TopNavigation({icon, name}) {
    return(
        <>
            <div>
                <span>
                    <i className={icon} />
                </span>
                <span className="pl-2 pr-2">
                    <i className="fas fa-chevron-right text-sm text-gray-500"></i>
                </span>
                <span className="text-sm text-gray-500">
                    {name}
                </span>
            </div>
        </>
    )
}
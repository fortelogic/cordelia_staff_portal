import React, { useEffect, useState } from "react";
import { getToken } from "../../utils/common";

// components

import CardStats from "../Cards/CardStats";

export default function HeaderStats() {

  const [amount, setAmount] = useState(null);

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_ENDPOINT}/agent_portal/wallet/balance`, {
      method: "GET",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization:
          `Bearer ${getToken()}`,
      },
    })
      .then((response) => response.json())
      .then((response) => {
        setAmount(response.amount)
      })
  })

  return (
    <>
      {/* Header */}
      <div className="relative bg-magenta pt-16 pb-16 ">
        <div className="px-4 md:px-10 mx-auto w-full">
          <div>
            {/* Card stats */}
            <div className="flex flex-wrap">
              <div className="w-1/3 lg:w-1/3 xl:w-1/3 px-4">
                <CardStats
                  statSubtitle="Wallet Balance"
                  statTitle={amount}
                  statArrow="up"
                  cashstatus={true}
                  // statPercent="3.48"
                  // statPercentColor="text-green-500"
                  statDescripiron="Recharge Now"
                  statIconName="far fa-chart-bar"
                  statIconColor="bg-red-500"
                />
              </div>
              {/* <div className="w-full lg:w-6/12 xl:w-3/12 px-4">
                <CardStats
                  statSubtitle="NEW USERS"
                  statTitle="2,356"
                  statArrow="down"
                  statPercent="3.48"
                  statPercentColor="text-red-500"
                  statDescripiron="Since last week"
                  statIconName="fas fa-chart-pie"
                  statIconColor="bg-orange-500"
                />
              </div>
              <div className="w-full lg:w-6/12 xl:w-3/12 px-4">
                <CardStats
                  statSubtitle="SALES"
                  statTitle="924"
                  statArrow="down"
                  statPercent="1.10"
                  statPercentColor="text-orange-500"
                  statDescripiron="Since yesterday"
                  statIconName="fas fa-users"
                  statIconColor="bg-pink-500"
                />
              </div>
              <div className="w-full lg:w-6/12 xl:w-3/12 px-4">
                <CardStats
                  statSubtitle="PERFORMANCE"
                  statTitle="49,65%"
                  statArrow="up"
                  statPercent="12"
                  statPercentColor="text-green-500"
                  statDescripiron="Since last month"
                  statIconName="fas fa-percent"
                  statIconColor="bg-blue-500"
                />
              </div> */}
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

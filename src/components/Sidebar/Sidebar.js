
import React from "react";
import { Link, useHistory  } from "react-router-dom";
import { getUser, removeUserSession } from '../../utils/common';
import LogoImage from "../../assets/images/logo.png";
import cx from "classnames";
import styles from "./Slider.Module.css";

export default function Sidebar() {
  const [collapseShow, setCollapseShow] = React.useState("hidden");
  const [adminOpen, setAdminOpen] = React.useState(false);
  let history = useHistory();
  const handleLogout= () => {
      removeUserSession();
      history.push('/login');
  }
  return (
    <>
      <nav 
        className={cx(styles.scrollbars,"md:left-0 md:block md:fixed md:top-0 md:bottom-0 md:overflow-y-auto md:flex-row md:flex-no-wrap md:overflow-hidden shadow-xl bg-white flex flex-wrap items-center justify-between relative md:w-52 z-10 py-4 px-6 border-r-2" )}>
          <div 
            className="md:flex-col md:items-stretch md:min-h-full md:flex-no-wrap px-0 flex flex-wrap items-center justify-between w-full mx-auto">
              <button
                className="cursor-pointer text-black opacity-50 md:hidden px-3 py-1 text-xl leading-none bg-transparent rounded border border-solid border-transparent"
                type="button"
                onClick={() => setCollapseShow("bg-white m-2 py-3 px-6")}
              >
                <i className="fas fa-bars"></i>
              </button>
              <Link
                className="md:block text-left md:pb-2 text-gray-700 mr-0 inline-block whitespace-no-wrap text-sm  font-bold p-4 px-0  self-center"
                to="/"
              >
                <img 
                  src={LogoImage} 
                  className={cx(styles.imageProps,"h-12 text-center self-center")}
                />
                <p className="text-sm pt-4">Cordelia staff portal</p>
              </Link>
              <div
                className={
                  "md:flex md:flex-col md:items-stretch md:opacity-100 md:relative md:mt-4 md:shadow-none shadow absolute top-0 left-0 right-0 z-40 overflow-y-auto overflow-x-hidden h-auto items-center flex-1 rounded " +
                  collapseShow
                }
              >
                <div className="md:min-w-full md:hidden block pb-4 mb-4 border-b border-solid border-gray-300">
                  <div className="flex flex-wrap">
                    <div className="w-6/12">
                      <Link
                        className="md:block text-left md:pb-2 text-gray-700 mr-0 inline-block whitespace-no-wrap text-sm uppercase font-bold p-4 px-0"
                        to="/"
                      >
                        Cordelia cruises
                      </Link>
                    </div>
                    <div className="w-6/12 flex justify-end">
                      <button
                        type="button"
                        className="cursor-pointer text-black opacity-50 md:hidden px-3 py-1 text-xl leading-none bg-transparent rounded border border-solid border-transparent"
                        onClick={() => setCollapseShow("hidden")}
                      >
                        <i className="fas fa-times"></i>
                      </button>
                    </div>
                  </div>
                </div>
                <hr className="my-2 md:min-w-full" />
                <h6 className="md:min-w-full text-gray-600 text-xs uppercase font-bold block pt-2 pb-4 no-underline">
                  Staff Booking Menu
                </h6>
                <ul className="md:flex-col md:min-w-full flex flex-col list-none">
                  <li className="items-center">
                    <Link
                      className={
                        "text-sm py-3 font-normal block " +
                        (window.location.href.indexOf("/admin/dashboard") !== -1
                          ? "text-j-magenta hover:text-j-magenta"
                          : "text-gray-800 hover:text-gray-600")
                      }
                      to="/admin/dashboard"
                    >
                      <i
                        className={
                          "fas fa-chart-pie mr-2 text-sm " +
                          (window.location.href.indexOf("/admin/dashboard") !== -1
                            ? "opacity-75"
                            : "text-gray-400")
                        }
                      ></i>{" "}
                      Dashboard
                    </Link>
                  </li>
                  <li className="items-center">
                    <Link
                      className={
                        "text-sm py-3 font-normal block " +
                        (window.location.href.indexOf("/admin/sharelead") !== -1
                          ? "text-j-magenta hover:text-j-magenta"
                          : "text-gray-800 hover:text-gray-600")
                      }
                      to="/admin/sharelead"
                    >
                      <i
                        className={
                          "fas fa-headset mr-2 text-sm " +
                          (window.location.href.indexOf("/admin/sharelead") !== -1
                            ? "opacity-75"
                            : "text-gray-400")
                        }
                      ></i>{" "}
                        Add Leads
                    </Link>
                  </li>
                  <li className="items-center">
                      <Link
                        className={
                          "text-sm py-3 font-normal block " +
                          (window.location.href.indexOf("/admin/routes") !== -1
                            ? "text-j-magenta hover:text-j-magenta"
                            : "text-gray-800 hover:text-gray-600")
                        }
                        to="/admin/allbookinglist"
                      >
                        <i
                          className={
                            "fas fa-table mr-2 text-sm " +
                            (window.location.href.indexOf("/admin/bookingList") !== -1
                              ? "opacity-75"
                              : "text-gray-400")
                          }
                        ></i>{" "}
                          All Bookings
                      </Link>
                    </li>
                    {/* <li className="items-center">
                      <Link
                        className={
                          "text-sm py-3 font-normal block " +
                          (window.location.href.indexOf("/admin/contact") !== -1
                            ? "text-j-magenta hover:text-j-magenta"
                            : "text-gray-800 hover:text-gray-600")
                        }
                        to="/admin/contact"
                      >
                        <i
                          className={
                            "fas fa-address-book mr-2 text-sm " +
                            (window.location.href.indexOf("/admin/contact") !== -1
                              ? "opacity-75"
                              : "text-gray-400")
                          }
                        ></i>{" "}
                        Contact Us
                      </Link>
                    </li> */}
                    {/* <li className="items-center">
                      <a
                        className={
                          "text-sm py-3 font-normal block text-gray-800 hover:text-gray-600" 
                        }
                        href="https://drive.google.com/drive/folders/1JMnl2OvtSbXbfyjtpeTaHalqSWsptpKM"
                        target="_blank"
                      >
                        <i
                          className={
                            "fas fa-business-time mr-2 text-sm text-gray-400"
                          }
                        ></i>{" "}
                        Business kit
                      </a>
                    </li> */}
                </ul>
                <hr className="my-4 md:min-w-full" />
                <div
                  className="grid grid-cols-2 grid-cols-2"
                  // onClick={() => {
                  //     setAdminOpen(!adminOpen);
                  // }}
                >
                  <h6 className="md:min-w-full text-gray-600 text-xs uppercase font-bold block pt-1 pb-4 no-underline">Settings</h6>
                  {/* <i
                    style={{ color: "grey", fontSize: 28, textAlign: "right" }}
                    className={cx(
                      "fas cursor-pointer text-j-gray-light text-2xl",
                      {
                        "fa-angle-down": !adminOpen,
                        "fa-angle-up": adminOpen,
                      }
                    )}
                  ></i> */}
                </div>
                <ul className="md:flex-col md:min-w-full flex flex-col list-none md:mb-4">
                  {/* <li className="items-center">
                    <Link
                      className={
                        "text-sm py-2 font-normal block " +
                        (window.location.href.indexOf("/admin/agent-profile") !== -1
                          ? "text-j-magenta hover:text-j-magenta"
                          : "text-gray-800 hover:text-gray-600")
                      }
                      to="/admin/agent-profile"
                    >
                      <i className="fas fa-id-badge text-gray-500 mr-2 text-sm"></i>{" "}
                      My Profile
                    </Link>
                  </li> */}
                  <li className="items-center">
                    <Link
                      className="text-gray-800 hover:text-gray-600 text-sm py-2 font-normal block"
                      onClick={handleLogout}
                    >
                      <i className="fas fa-fingerprint text-gray-500 mr-2 text-sm"></i>{" "}
                      Logout
                    </Link>
                  </li>
                </ul>
          </div>
        </div>
      </nav>
    </>
  );
}

import React, { useState, Component, setUser, useEffect } from 'react';
import BookingFormView from "../Booking/General/BookingFormView";
import Tile from "../Tile/Tile";
import Button from "../Button/Button";
import cx from "classnames";
import scrollTo from "../../utils/scrollTo";
import UserContext from "../../store/UserContext";
import { CancelView } from "../View/FAQView";
import moment from "moment";
import { getToken } from "../../utils/common"
import LoadingIcon from "../Loading/LoadingIcon"
import { useHistory } from "react-router-dom";


class UpgradePaymentSummary extends Component {

  render() {
    const { values } = this.props;
    return (
      <>
        <PaymentSummaryViewAPI nextStep={this.props} />
      </>
    )
  }
}
export default UpgradePaymentSummary;

const PaymentSummaryViewAPI = (nextStep) => {
    let history = useHistory();
    const [user, setUser] = React.useContext(UserContext);
    const [error, setError] = useState(false)
    const bookingMain = user.upgradeBooking;
    const booking = user.upgradeBooking.rooms || {};
    const [individualPrice, setIndividualPrice] = useState(null);
    const [fulldetails, setFullDetails] = useState(null);
    const [loading, setLoading] = useState()
    const [open, setOpen] = useState(null);
    const [fareOpen, setFareOpen] = useState(false);

    console.log(booking);

    useEffect(() => {
      var roomsData=[];
      bookingMain.rooms.map((data,i) => {
        if(data.selected == undefined)
        {
          roomsData.push({
            "room_type": data.roomCode,
            "old_room_no": data.roomNumber,
            "room_no": data.roomNumber
          })
        } else {
          roomsData.push({
            "room_type":data.selected.code,
            "old_room_no": data.roomNumber,
            "room_no": data.room_number
          })
        }
      })

      const bookingInput = {
        booking_reference:  bookingMain.roomNumber,
        rooms: roomsData
      }
 
      fetch(process.env.REACT_APP_API_ENDPOINT+'/agent_portal/upgrade/pricing', {
            method: 'POST',
            headers: {
              Accept: "application/json",
              "Content-Type": "application/json",
              Authorization:
              `Bearer ${getToken()}`,
            },
            body: JSON.stringify(bookingInput)
        }).then((res) => {
          if (res.ok) {
            return res.json()
          } else {
            throw new Error("Something went wrong")
          }
        })
        .then((response) => {
          console.log(response);
          // setFullDetails(response);
          setIndividualPrice(response)
          setLoading(false)
        }).catch((error) => {
          setError(true)
          setLoading(false)
        })
    },[loading])

    if (loading || error || !individualPrice) {
      return (
        <h1 className="text-4xl">
          <LoadingIcon className="py-20 text-j-magenta" />
        </h1>
      )
    }
  
    const backbuttonpressed = (e) => {
      e.preventDefault()
      nextStep.nextStep.prevStep()
    }
  
    const conformingPaymentDetails = (e) => {
      const itineraryData = {
        itineraries: bookingMain.itinerary,
        amount: individualPrice.payable
      }
      localStorage.setItem("itinerary",JSON.stringify(itineraryData))
      e.preventDefault()
      const reshudilingInput = {
        itinerary:  bookingMain.itinaryID.id,
        booking_reference: bookingMain.roomNumber
      }
      fetch(process.env.REACT_APP_API_ENDPOINT+'/agent_portal/upgrade/confirm', {
        method: 'POST',
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json",
          Authorization:
          `Bearer ${getToken()}`,
        },
        body: JSON.stringify(reshudilingInput)
      }).then((response) => response.json())
      .then((response) => {
        console.log(response);
        history.push({
          pathname: '/admin/paymentGateway',
        })
      })
      // nextStep.nextStep.nextStep()
    }
  
    return (
      <>
        <BookingFormView
          buttonText="Continue"
          onClick={conformingPaymentDetails}
          lessSpace={true}
          id="otp-form"
          // disabled={!isComplete}
        >
          <>
        <div className="grid lg:grid-cols-2 md:grid-cols-2 sm:grid-cols-1 gap-2">
          <span>
            <Tile tiny className="mb-5 border border-j-gray-lighter rounded-lg" style={{ marginRight: '5px' }}>
              <Tile.Inner className="pb-0">
                <OrderDetail title="SHIP">
                  {bookingMain.itinerary.name}
                </OrderDetail>
                <div style={{ display: "flex", justifyContent: "space-between" }}>
                <div className="mr-2">
                  <OrderDetail title="Departure">
                    {/* {bookingMain.itinerary.departure}
                    <br></br> */}
                    {moment(bookingMain.itinerary.start_time).format("ddd, D MMM YYYY, h:mm a")}
                  </OrderDetail>
                </div>
                <div className="ml-2">
                  <OrderDetail title="Arrival">
                    {/* {bookingMain.itinerary.destination}
                    <br></br> */}
                    {moment(bookingMain.itinerary.end_time).format("ddd, D MMM YYYY, h:mm a")}
                  </OrderDetail>
                </div>
              </div>
              {Object.keys(booking)
                .map((k, index) =>

                  <OrderDetail title={`Cabin ${index + 1}`}>
                    {booking[index].selected == undefined ? booking[index].roomCatogory :booking[index].selected.name}<br />
                        Room No: {booking[index].selected == undefined ? booking[index].roomNumber:booking[index].room_number}<br />
                    {booking[index].adults + booking[index].children + booking[index].infants}  Guest{(booking[index].adults + booking[index].children + booking[index].infants) > 1 ? "s" : null}
                  </OrderDetail>
                )}
              </Tile.Inner>
            </Tile>
          </span>
          <span>
            <Tile tiny theme="white" className="mb-5 col-span-2 bg-transparent" style={{ marginLeft: '5px', backgroundColor: 'transparent' }}>
              <div className="border border-j-gray-lighter" style={{ backgroundColor: "white", borderRadius: "10px" }}>
                <div
                  style={{ margin: '25px', color: '#500E4B', fontWeight: 'bold' }}
                  className="grid grid-cols-1 grid-cols-2"
                  onClick={() => {
                    setOpen(!open);
                  }}
                >
                  <span className="pt-1"> PRICE DETAILS</span>
                  <i
                    style={{ color: "grey", fontSize: 28, textAlign: 'right' }}
                    className={cx("fas cursor-pointer text-j-gray-light text-4xl", {
                      "fa-angle-down": !open,
                      "fa-angle-up": open,
                    })}
                  ></i>
                </div>
                {individualPrice.rooms.map((details, index) =>
                  <div key={index}>
                    <div style={{ color: "black" }}>
                      {open && details.price_details.map((price, index) =>
                        <div key={index} style={{ margin: "25px" }}>
                          <div style={{ width: '100%' }}>
                            <PaymentDetails content={price.type} details={Math.round((price.total) * 100 / 100)} big={true} />
                            <PaymentDetails content="Cabin Fare:" details={Math.round((price.fare) * 100 / 100)} big={false} />
                            {price.discount != '0' && <div className="grid grid-cols-1 grid-cols-2" style={{ marginBottom: '7px' }}>
                              <div style={{ color: "grey", fontSize: 14 }} className="float-left bold">Individual Discount</div>
                              <div style={{ color: "grey", fontSize: 14, textAlign: 'right' }} className="float-right font-weight-bold text-capitalize">
                                - &#x20B9;{price.discount}
                              </div>
                            </div>}
                            <PaymentDetails content="Service Charges & Levies" details={price.portCharges+price.gratuity} big={false} />
                          </div>
                        </div>
                      )}
                    </div>
                  </div>  
                )}
                {open && <div style={{ margin: '25px' }}>
                        <PaymentDetails content="Promo Code:" details="0" big={true} />
                        <PaymentDetails content="New Year Promo:" details="0" big={false} />
                      </div>
                }
                {open && <div style={{ margin: '25px' }}>
                        <PaymentDetails content="Sub-Total:" details={individualPrice.base_price} big={true} />
                        <PaymentDetails content="Taxable Sub Total:" details={(Math.round(individualPrice.base_price), 0)} big={false} />
                      </div>
                }
                {open && <div style={{ margin: '25px' }}>
                  <PaymentDetails content="Taxes:" details={Math.round(individualPrice.tax)} big={true} />
                  <PaymentDetails content="GST:" details={Math.round(individualPrice.tax)} big={false} />
                </div>
                }

                <div style={{ margin: '25px' }}>
                  <PaymentDetails content="Discount:" details={individualPrice.agent_commission} big={true} />
                </div>
                <div style={{ margin: '25px' }}>
                  <PaymentDetails content="Net Payable to Cordelia:" details={Math.round(individualPrice.payable)} big={true} />
                </div> 
                <div
                style={{
                  margin: "25px",
                  marginBottom: "25px",
                  color: "#500E4B",
                  fontWeight: "bold",
                }}
                className="grid grid-cols-1 grid-cols-2"
                onClick={() => {
                  setFareOpen(!fareOpen);
                }}
              >
                <span className="pt-1">Cancellation and Reschedule policy</span>
                <i
                  style={{ color: "grey", fontSize: 28, textAlign: "right" }}
                  className={cx(
                    "fas cursor-pointer text-j-gray-light text-4xl",
                    {
                      "fa-angle-down": !fareOpen,
                      "fa-angle-up": fareOpen,
                    }
                  )}
                ></i>
              </div>
              {fareOpen && (
                <div style={{ padding: "1rem" }}>
                  <CancelView>
                    <h3 style={{ color: "#500E4B", fontWeight: "600" }}>
                      <u>
                        Rescheduling Fee for sailings May, Jun &amp; July 31, 2021
                </u>
                    </h3>
                    <table style={{ color: "black", borderCollapse: "collapse", width: "100%" }}>
                      <tr>
                        <th style={{ border: "1px solid #dddddd", textAlign: "left", padding: "8px", }}>Days to Departure</th>
                        <th style={{ border: "1px solid #dddddd", textAlign: "left", padding: "8px", }}>Rescheduing Fee</th>
                      </tr>
                      <tr>
                        <td style={{ border: "1px solid #dddddd", textAlign: "left", padding: "8px", }}>4 days and above</td>
                        <td style={{ border: "1px solid #dddddd", textAlign: "left", padding: "8px", }}>NIL Fee</td>
                      </tr>
                      <tr>
                        <td style={{ border: "1px solid #dddddd", textAlign: "left", padding: "8px", }}> 0 to 3 days</td>
                        <td style={{ border: "1px solid #dddddd", textAlign: "left", padding: "8px", }}>INR 5000 per cabin</td>
                      </tr>
                    </table>
                    <br></br>
                    <ul style={{ color: "black", listStyle: "disc", marginLeft: "1rem", }}>
                      <li>
                        Free rescheduling is limited to one time<sup>*</sup>
                      </li>
                      <li>
                        Rescheduled itinerary must commence on or before 31<sup>st</sup> March
                  2022
                </li>
                      <li>
                        Fare difference is applicable for higher cabin category or increase in sailing nights
                </li>
                    </ul>
                    <br></br>
                  </CancelView>
                  <CancelView title="" theme="blue">
                    <h3 style={{ color: "#500E4B", fontWeight: "600" }}>
                      <u>
                        Rescheduling Fee for sailings from 1<sup>st</sup> August 2021 onwards
                </u>
                    </h3>
                    <table style={{ color: "black", borderCollapse: "collapse", width: "100%" }}>
                      <tr>
                        <th style={{ border: "1px solid #dddddd", textAlign: "left", padding: "8px", }}>Days to Departure</th>
                        <th style={{ border: "1px solid #dddddd", textAlign: "left", padding: "8px", }}>Rescheduing Fee</th>
                      </tr>
                      <tr>
                        <td style={{ border: "1px solid #dddddd", textAlign: "left", padding: "8px", }}>31 days and above</td>
                        <td style={{ border: "1px solid #dddddd", textAlign: "left", padding: "8px", }}>NIL Fee</td>
                      </tr>
                      <tr>
                        <td style={{ border: "1px solid #dddddd", textAlign: "left", padding: "8px", }}>0 to 30 days</td>
                        <td style={{ border: "1px solid #dddddd", textAlign: "left", padding: "8px", }}>INR 5000 per cabin</td>
                      </tr>
                    </table>
                    <br></br>
                    <ul style={{ color: "black", listStyle: "disc", marginLeft: "1rem", }}>
                      <li>
                        Free rescheduling is limited to one time<sup>*</sup>
                      </li>
                      <li>
                        Rescheduled itinerary must commence on or before 31<sup>st</sup>
                          March 2022
                        </li>
                      <li>
                        Fare difference is applicable for higher cabin category or increase in sailing nights
                </li>
                    </ul>
                    <br></br>
                  </CancelView>
                  <CancelView title="">
                    <h3 style={{ color: "#500E4B", fontWeight: "600" }}>
                      <u>Cancellation Fees</u>
                    </h3>
                    <br></br>
                    <h3 style={{ fontWeight: "600", color: "black", }}>
                      <u>100% Refund will be offered if Cordelia Cruises cancel any sailing from May &amp; Jun 2021 itineraries due to unforeseen circumstances.</u>
                    </h3>
                    <br></br>
                    <h3 style={{ color: "#500E4B", fontWeight: "600" }}>
                      <u>Cancellation for sailings of May 2021</u>
                    </h3>
                    <table style={{ color: "black", borderCollapse: "collapse", width: "100%" }}>
                      <tr>
                        <th style={{ border: "1px solid #dddddd", textAlign: "left", padding: "8px", }}>Cancellation Requested</th>
                        <th style={{ border: "1px solid #dddddd", textAlign: "left", padding: "8px", }}>Cancellation Fee</th>
                      </tr>
                      <tr>
                        <td style={{ border: "1px solid #dddddd", textAlign: "left", padding: "8px", }}>Cancel before 30<sup>th</sup> April 2021</td>
                        <td style={{ border: "1px solid #dddddd", textAlign: "left", padding: "8px", }}>NIL</td>
                      </tr>
                      <tr>
                        <td style={{ border: "1px solid #dddddd", textAlign: "left", padding: "8px", }}>For 1<sup>st</sup> May 2021 Onwards</td>
                        <td style={{ border: "1px solid #dddddd", textAlign: "left", padding: "8px", }}>5k per cabin</td>
                      </tr>
                      <tr>
                        <td style={{ border: "1px solid #dddddd", textAlign: "left", padding: "8px", }}>For No-Show</td>
                        <td style={{ border: "1px solid #dddddd", textAlign: "left", padding: "8px", }}>100% of total fare</td>
                      </tr>
                    </table>
                    <br></br>
                    <h3 style={{ color: "#500E4B", fontWeight: "600" }}>
                      <u>Cancellation for sailings from Jun 2021 onwards</u>
                    </h3>
                    <table style={{ color: "black", borderCollapse: "collapse", width: "100%" }}>
                      <tr>
                        <th style={{ border: "1px solid #dddddd", textAlign: "left", padding: "8px", }}>Days to departure</th>
                        <th style={{ border: "1px solid #dddddd", textAlign: "left", padding: "8px", }}>Cancellation Fee</th>
                      </tr>
                      <tr>
                        <td style={{ border: "1px solid #dddddd", textAlign: "left", padding: "8px", }}>More than 31 days</td>
                        <td style={{ border: "1px solid #dddddd", textAlign: "left", padding: "8px", }}>25% of cabin fare</td>
                      </tr>
                      <tr>
                        <td style={{ border: "1px solid #dddddd", textAlign: "left", padding: "8px", }}>0 to 30 days</td>
                        <td style={{ border: "1px solid #dddddd", textAlign: "left", padding: "8px", }}>50% of cabin fare</td>
                      </tr>
                      <tr>
                        <td style={{ border: "1px solid #dddddd", textAlign: "left", padding: "8px", }}>For No-Show</td>
                        <td style={{ border: "1px solid #dddddd", textAlign: "left", padding: "8px", }}>100% of cabin fare</td>
                      </tr>
                    </table>
                    <br></br>
                    <ul style={{ color: "black", listStyle: "disc", marginLeft: "1rem", }}>
                      <li>Cancellation fee will be applicable only on cabin fare,</li>
                      <li>
                        Port Charges &amp; gratuity will be refunded completely.
                </li>
                      <li>GST for the refunded amount will also be returned.</li>
                      <li>
                        GST will be applicable on rescheduling fee and on fare difference if any.
                </li>
                      <li>
                        GST + TCS will also be applicable for international sailing.
                  <br></br>
                  (TCS will applicable only for International sailings with bundle package.)
                </li>
                    </ul>
                  </CancelView>
                </div>
              )}
              </div>
            </Tile>
            </span>
            </div>
            </>
        </BookingFormView>
      </>
    )
  }

  const OrderDetail = ({ title, children, big }) => (
    <div className="mb-6">
      <h1 className="uppercase pb-0 text-j-gray text-tiny leading-none ">
        {title}
      </h1>
      <h4
        className={
          big ? "text-j-magenta text-3xl font-bold pt-4" : "text-j-magenta pt-2 text-lg font-semibold"
        }
      >
        {children}
      </h4>
    </div>
  );

  const PaymentDetails = ({ content, details, big }) => (
    <>
      <div className="grid grid-cols-1 grid-cols-2" style={{ marginBottom: '7px' }}>
        {big && <>
          <div style={{ color: "black", fontSize: 16, fontWeight: 'bold' }} className="float-left bold">
            {content}
          </div>
          <div style={{ color: "black", fontSize: 16, textAlign: 'right', fontWeight: 'bold' }} className="float-right font-weight-bold text-capitalize">
            &#x20B9;{details}
          </div>
        </>
        }
        {!big && <>
          <div style={{ color: "grey", fontSize: 14 }} className="float-left bold">{content}</div>
          <div style={{ color: "grey", fontSize: 14, textAlign: 'right' }} className="float-right font-weight-bold text-capitalize">
            &#x20B9;{details}
          </div>
        </>
        }
      </div>
    </>
  );
  
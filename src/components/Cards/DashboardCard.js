import React from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import Link from '@material-ui/core/Link';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import { useHistory } from "react-router-dom";
import Image1 from "../../assets/images/calendar.png"
import Image2 from "../../assets/images/lead.png"

function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {'Copyright © '}
      <Link color="inherit" href="https://material-ui.com/">
        Your Website
      </Link>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}

const useStyles = makeStyles((theme) => ({
  '@global': {
    ul: {
      margin: 0,
      padding: 0,
      listStyle: 'none',
    },
  },
  appBar: {
    borderBottom: `1px solid ${theme.palette.divider}`,
  },
  toolbar: {
    flexWrap: 'wrap',
  },
  toolbarTitle: {
    flexGrow: 1,
  },
  link: {
    margin: theme.spacing(1, 1.5),
  },
  heroContent: {
    padding: theme.spacing(8, 0, 6),
  },
  cardHeader: {
    backgroundColor:
      theme.palette.type === 'light' ? "#500E4B" : "#500E4B",
    color: 'white'
  },
  cardPricing: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'baseline',
    marginBottom: theme.spacing(2),
  },
  buttonStyle: {
      borderColor:'#EA725B',
      color:'#EA725B',
      marginBottom: '10px',
      '&:hover': {
        backgroundColor: '#EA725B',
        color: '#FFF',
        borderColor: '#fff'
      }
  }
}));

const tiers = [
  {
    title: 'Booking',
    description: 'All Bookings',
    buttonText: 'Find Bookings',
    image: Image1,
    redirection: '/admin/allbookinglist'
  },
  {
    title: 'Lead',
    description: 'Add Lead',
    buttonText: 'Add Now',
    image: Image2,
    redirection: '/admin/sharelead'
  },
];


export default function DashboardCard() {
    let history = useHistory();
    const classes = useStyles();

    const redirectionfunc = (redirection) => {
        history.push(redirection)
    }

  return (
    <React.Fragment>
      <CssBaseline />
      <Container maxWidth="lg" component="main" className="mat-6">
          <div className="grid mt-8 gap-8 grid-cols-2 md:grid-cols-2 xl:grid-cols-2">
            {tiers.map((tier) => (
            // Enterprise card is full width at sm breakpoint
              <div className="flex flex-col ">
                <div className="bg-white shadow-md  rounded-3xl p-4 pb-0">
                  <div className="flex-none lg:flex">
                    <div className="h-full w-24 lg:h-10 lg:w-24 lg:mb-0 mb-3">
                        <img src={tier.image}
                          className="w-24 object-scale-down lg:object-cover lg:h-24 rounded-2xl" />
                    </div>
                    <div className="flex-auto ml-3 justify-evenly py-2">
                        <div className="flex flex-wrap ">
                            <div className="w-full flex-none text-xs text-j-magenta font-medium ">
                                {tier.title}
                            </div>
                            <h2 className="flex-auto text-j-magent text-lg font-bold">{tier.description}</h2>
                        </div>
                        <p className="mt-1"></p>
                        <div className="flex border-t-2 border-gray-200 border-dotted"></div>
                        <div className="flex text-sm font-medium">
                          <div className="w-11/12"></div>
                          <div className="w-full">
                            <button
                                className="mb-2 md:mb-0 bg-magenta px-5 py-2 mt-2 shadow-sm tracking-wider text-white rounded-full hover:bg-j-orange justify-end w-full focus:outline-none"
                                type="button" aria-label="like"
                                onClick={()=> redirectionfunc(tier.redirection)}>{tier.buttonText}</button>
                            </div>
                        </div>
                    </div>
                  </div>
                </div>
              </div>
               ))}
            </div>
      </Container>
      {/* Footer */}
      {/* End footer */}
    </React.Fragment>
  );
}
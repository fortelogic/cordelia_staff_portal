import React, { Component, useRef, useEffect } from "react";
import { IoArrowForwardOutline } from "react-icons/io5";
import { Link, useHistory } from "react-router-dom";

class Cards extends Component {
  state = {};

  render() {
    return (
      <>
        <CardsComponent />
      </>
    );
  }
}

const CardsComponent = () => {
  let history = useHistory();

  const redirect = () => {
    history.push("/bookingList");
  };

  const redirectBooking = () => {
    history.push("/admin/routes");
  };

  const redirectBookingList = () => {
    history.push("/admin/bookingList");
  }

  return (
    <div class="p-10 grid grid-cols-1 sm:grid-cols-1 md:grid-cols-2 lg:grid-cols-2 xl:grid-cols-2 gap-20 ml-8 mr-8">
      <div
        class="overflow-hidden shadow-lg text-center rounded-2xl bg-gradient-to-r from-j-orange to-j-red-light ml-6 mr-6"
        onClick={redirectBooking}
        style={{cursor: "pointer"}}
      >
        <div class="px-6 py-4 mt-10">
          <div class="font-bold mb-2 text-white">
            Book a Cabin
          </div>
        </div>
        <div class="px-6 pt-4 pb-2 mb-10">
          <span class="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">
            <IoArrowForwardOutline />
          </span>
        </div>
      </div>
      <div 
        class="rounded-2xl overflow-hidden shadow-lg text-center bg-gradient-to-r from-j-orange to-j-red-light ml-6 mr-6" 
        style={{cursor: "pointer"}}
        onClick={redirectBookingList}
      >
        <div class="px-6 py-4 mt-10">
          <div class="font-bold mb-2 text-white">
            My Bookings
          </div>
        </div>
        <div class="px-6 pt-4 pb-2 mb-10">
          <span class="inline-block bg-gray-200 rounded-full px-3 py-1 text-sm font-semibold text-gray-700 mr-2 mb-2">
            <IoArrowForwardOutline />
          </span>
        </div>
      </div>
    </div>
  );
};
export default Cards;

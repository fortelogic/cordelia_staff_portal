import React, { useEffect } from "react";
import cx from "classnames";


export const Overlay = ({ children, className }) => {
  useEffect(() => {
    if (typeof document !== "undefined")
      document.body.style.overflow = "hidden";
    return () => {
      if (typeof document !== "undefined")
        document.body.style.overflow = "auto";
    };
  });
  return (
    <div
      className={cx(
        "fixed top-0 left-0 bottom-0 right-0 overflow-y-scroll disable-scrollbars filter-modal",
        className
      )}
    >
      <div className="mx-auto w-full">{children}</div>
    </div>
  );
};

export default Overlay;

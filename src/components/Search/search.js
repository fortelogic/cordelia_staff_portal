import React, { Component, useRef, useMemo, useState, useEffect } from "react";

export default function Search({onChangeSearch, handleSearch, defaultValue, placeHolder}) {
    return(
        <>
            <div className="p-8">
                <div className="bg-white flex items-center rounded-full shadow-xl border">
                    <input className="rounded-l-full w-full py-2 px-6 text-gray-700 leading-tight focus:outline-none" id="search" type="text" placeholder={placeHolder} onChange={(e)=>onChangeSearch(e)} defaultValue={defaultValue}/>
                    <div className="p-4">
                        <button className="bg-magenta text-white rounded-full p-2 hover:bg-blue-400 focus:outline-none w-12 h-8 flex items-center justify-center" onClick={()=>handleSearch()}>
                            <i className="fas fa-search"></i>
                        </button>
                    </div>
                </div>
            </div>
        </>
    )
}
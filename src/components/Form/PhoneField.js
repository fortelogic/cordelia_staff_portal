import React from "react";
import Input from "./Input";
import Field from "./Field";
import Select from "./Select";

const defaultPrefix = "+91";

const PhoneField = React.forwardRef(
  (
    {
      className,
      error,
      name,
      onChange,
      lessSpace,
      inverseError,
      defaultValue,
      noIcon,
      countryPhoneCode,
      ...rest
    },
    ref
  ) => {
    const [prefix, number] = (defaultValue || "").split(" ");
    const [value, setValue] = React.useState(defaultValue);
    const phoneInput = React.useRef(null);
    const countryPrefix = (number && prefix) || defaultPrefix;
    const updateValue = () => {
      const val = `${countryPhoneCode.length?`${countryPhoneCode} `:""}${phoneInput.current.value}`;
      setValue(val);
      onChange && onChange({ target: { value: val } });
    };

    return (
      <Field
        icon={!noIcon && "fal fa-mobile flex-grow-0"}
        error={error}
        inverseError={inverseError}
        className={className}
        lessSpace={lessSpace}
      >
        {/* <Select
          disabled
          tiny
          className="w-16"
          maxLength="5"
        >
            <option value={countryPhoneCode}>
              {countryPhoneCode}
            </option>
        </Select> */}
        {/* <div className="border-l border-j-gray -my-2" /> */}
        <Input
          ref={phoneInput}
          {...rest}
          type="number"
          onChange={updateValue}
          grow
          withIcon
          defaultValue={number || prefix}
          maxLength="10"
        />
        <input ref={ref} name={name} value={value} type="hidden" />
      </Field>
    );
  }
);

export default PhoneField;

import cx from "classnames";
import React from "react";

const CheckBox = ({ children, value, onChange, ...rest }) => (
  <div
    className="flex mb-6 cursor-pointer"
    onClick={() => onChange && onChange({ target: { value: !value } })}
  >
    <i
      className={cx(
        "far pr-2 pt-2 text-lg self-center leading-none",
        value ? "fa-check-square" : "fa-square"
      )}
    />
    <input type="hidden" value={value} {...rest} />
    <p className="leading-0 pt-1">{children}</p>
  </div>
);

export default CheckBox;

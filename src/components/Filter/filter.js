import React, { Component, useRef, useMemo, useState, useEffect } from "react";
import DateRangePicker from "react-daterange-picker";
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';

const useStyles = makeStyles((theme) => ({
    container: {
      display: 'flex',
      flexWrap: 'wrap',
    },
    textField: {
      marginLeft: theme.spacing(1),
      marginRight: theme.spacing(1),
      width: 200,
    },
}));

export default function Filter({
    onChangeFilterbyDate,
    days, 
    handleFilterByDate, 
    onChangeFilterbyCruiceDate,
    handleFilterByCruiceDate,
    onChangeFilterbySource,
    handleFilterBySource,
    handleClearByDate,
}) {
    const [date, changeDate] = useState(new Date());
    const classes = useStyles();
    
    return(
        <>
            <div className="p-2 grid grid-cols-3 gap-6 ml-2 mr-2">
                <div className="bg-white rounded-lg shadow-xl border">
                    <h1 className="text-xl font-bold p-4">Select by Date</h1>
                        <DateRangePicker
                            value={days}
                            onSelect={onChangeFilterbyDate}
                            singleDateRange={true}
                        />
                        <p className="pl-3 text-xs text-gray-700 pb-2 underline text-center cursor-pointer" onClick={()=> handleClearByDate()}>Clear</p>
                    <button className="bg-opacity-500 py-2 px-4 mb-2 rounded text-sm font-semibold hover:bg-opacity-75 text-gray-600 border ml-3 w-11/12 hover:bg-gray-300" onClick={()=>handleFilterByDate()}>Filter By Date</button>
                </div>
                <div>
                    <div className="bg-white rounded-lg shadow-xl border">
                        <h1 className="text-xl font-bold p-4">Select by Cruice Date</h1>
                        <div className="mt-4 mb-6 pl-4 pr-10">
                                <input
                                    id="datetime-local"
                                    type="date"
                                    defaultValue=""
                                    className= "h-10 rounded-lg pl-2 pr-2 pt-2 pb-4 ml-2 bg-gray-200 w-full"
                                    onChange={(e) => onChangeFilterbyCruiceDate(e)}
                                />
                                <p className="pl-3 text-xs text-gray-500 pt-2">*Please select cruice date for filter</p>
                        </div>
                        <button className="bg-opacity-500 py-2 px-4 mb-2 rounded text-sm font-semibold hover:bg-opacity-75 text-gray-600 border ml-3 w-11/12 hover:bg-gray-300" onClick={()=>handleFilterByCruiceDate()}>Filter By Cruice Date</button>
                    </div>
                </div>
                <div>
                    <div className="bg-white rounded-lg shadow-xl border">
                        <h1 className="text-xl font-bold p-4">Filter By Source</h1>
                        <div className="mt-4 mb-6 pl-4 pr-4">
                            <label className="text-xs">Booking Source</label>
                            <select
                                name="booking_source"
                                id="booking_source"
                                style={{
                                width: "100%",
                                padding: "0.625rem",
                                borderRadius: "0.625rem",
                                boxShadow:
                                    "0 1px 3px 0 rgba(0, 0, 0, 0.1), 0 1px 2px 0 rgba(0, 0, 0, 0.06)",
                                }}
                                placeholder="Booking Source"
                                onChange={(e) => onChangeFilterbySource(e)}

                            >
                                <option value={[]} selected disabled>Choose Here</option>
                                <option value="API">API</option>
                                <option value="B2C">B2C</option>
                                <option value="B2B">B2B</option>
                            </select>
                        </div>
                        <button className="bg-opacity-500 py-2 px-4 mb-2 rounded text-sm font-semibold hover:bg-opacity-75 text-gray-600 border ml-3 w-11/12 hover:bg-gray-300" onClick={()=>handleFilterBySource()}>Filter By Booking Source</button>
                    </div>
                </div>
            </div>
        </>
    )
}
export const GeneralCard = ({children}) => {
    return(
        <div class="lg:m-4 shadow-md hover:shadow-lg hover:bg-gray-100 rounded-lg bg-white my-12 mx-8">
             {children}
        </div>
    )
}

export default GeneralCard;
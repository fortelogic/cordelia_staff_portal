import React from "react";
import cx from "classnames";
import Overlay from "../../Overlay/Overlay";
import Button from "../../Button/Button";

const BookingOverlay = ({ children, onCancel, open, style = "back" }) => {
  const button =
    style === "back" ? (
      <i
        className="fas fa-arrow-left cursor-pointer text-3xl"
        onClick={onCancel}
      />
    ) : style === "close" ? (
      <i
        className="fas fa-times cursor-pointer text-4xl float-right"
        onClick={onCancel}
      />
    ) : null;

  const menu =
    button && onCancel ? (
      <div className={cx("flex", { "justify-end": style === "close" })}>
        {button}
      </div>
    ) : null;

  return (
    <Overlay open={open} className="bg-magenta text-white px-5 pt-8 pb-16 itinerarymodal">
      {menu}
      <div className="pt-6">{children}</div>
    </Overlay>
  );
};

export default BookingOverlay;

export const SkipButton = ({ className, ...props }) => (
  <Button
    className={cx("w-full border border-j-white bg-j-magenta text-j-white", className)}
    {...props}
  >
    Skip
  </Button>
);

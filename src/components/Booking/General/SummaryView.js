import React from "react";
import moment from "moment";
import Tile from "../../Tile/Tile";

const SummaryView = ({ booking }) =>
  booking.giftCard ? (
    <GiftCardSummaryView booking={booking} />
  ) : (
    <ItinerarySummaryView booking={booking} />
  );

const GiftCardSummaryView = ({ booking }) => (
  <>
    <h1>Order summary</h1>
    <Tile tiny theme="white" className="mb-5 border border-j-gray-lighter">
      <Tile.Inner className="pb-0">
        <OrderDetail title="Item">Gift card</OrderDetail>
        <OrderDetail title="Total amount payable" big>
          &#x20B9; {booking.giftCard.value.toLocaleString("hi-IN")}
        </OrderDetail>
      </Tile.Inner>
    </Tile>
  </>
);

const ItinerarySummaryView = ({ booking }) => {
  const { rooms } = booking;
  let departure = booking.departure;
  let departureTime = booking.itinerary.startTime;
  let arrival = booking.destination;
  let arrivalTime = booking.itinerary.endTime;
  let bookigStore = JSON.parse(window.localStorage.getItem('booking'));
  const guests = rooms
    .map((r) => r.adults + r.children + r.infants)
    .reduce((t, i) => t + i, 0);

  const selected = {};
  rooms.forEach((r) => {
    if (!selected[r.selected.name]) selected[r.selected.name] = 0;
    selected[r.selected.name]++;
  });
  // const totalPrice = rooms.reduce((x, r) => x + (r.total_price || r.selected.price.total), 0);
  const totalPrice = bookigStore.total;

  let bookingArr = booking.rooms;
  // let x = booking.route.name;
  // x = x.split(" - ")
  return (
    <>
      
      <Tile tiny className="mb-5 border border-j-gray-lighter rounded-lg">
        <Tile.Inner className="pb-0">
          
          {booking.number ? (<OrderDetail title="Booking Reference no">
              {booking.number}
            </OrderDetail>) : null}

          {booking.invoice_url ? (<OrderDetail title="Invoice">
              <a href={booking.invoice_url}><i class="fa fa-download" aria-hidden="true"></i>
 Download</a>
            </OrderDetail>) : null}

          {booking.itinerary ? (
            <OrderDetail title="Ship">
              {booking.itinerary.ship.name}
            </OrderDetail>
          ) : (
            <OrderDetail title="Item">
              {booking.route
                ? "Flexi Date Certificate"
                : "Flexi Cruise Certificate"}
            </OrderDetail>
          )}

          <div className="grid grid-cols-3 gap-4">
                    <div>
                      <OrderDetail title="Departure">
                      {departure}
                      <br></br>
                      {moment(departureTime).format("ddd, D MMM YYYY, h:mm a")}
                      </OrderDetail>
                    </div>
                    <div>
                      <OrderDetail title="Arrival">
                        {arrival}
                        <br></br>
                        {moment(arrivalTime).format("ddd, D MMM YYYY, h:mm a")}
                      </OrderDetail>
                    </div>
                  </div>
          {Object.keys(bookingArr)
                  .map((k, index) => 
                  
                    <OrderDetail title={`Cabin ${index+1}`}>
                        {bookingArr[index].selected.name}<br/>
                        Deck No: {((bookingArr[index].room_number).toString()).charAt(0)}<br/>
                        Room No: {bookingArr[index].room_number}<br/>
                        {bookingArr[index].adults+bookingArr[index].children+bookingArr[index].infants}  Guest{(bookingArr[index].adults+bookingArr[index].children+bookingArr[index].infants) > 1 ? "s" : null}
                    </OrderDetail>
                  )}
          {/* <OrderDetail title="Itinerary">
            {Object.keys(selected)
              .map((k) => `${selected[k]} x ${k}`)
              .join(", ")}
            , {guests} Guest{guests > 1 ? "s" : null}
          </OrderDetail> */}
          {/* <OrderDetail title="Total amount payable" big>
            &#x20B9; {Math.round(totalPrice.toLocaleString("hi-IN"))}
          </OrderDetail> */}
        </Tile.Inner>
      </Tile>
    </>
  );
};

export default SummaryView;

const OrderDetail = ({ title, children, big }) => (
  <div className="mb-6">
    <h1 className="uppercase pb-0 text-j-gray text-tiny leading-none">
      {title}
    </h1>
    <h4
      className={
        big ? "text-j-magenta text-3xl font-bold pt-4" : "text-j-magenta pt-2 text-lg font-semibold"
      }
    >
      {children}
    </h4>
  </div>
);

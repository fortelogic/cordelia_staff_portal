import Button from "../../Button/Button";
import cx from "classnames";


export const BookingFormView = ({
  children,
  title,
  buttonText = "Proceed to select room category",
  buttonClassName = "bg-j-red-light",
  disabled,
  id,
  lessSpace,
  ...rest
}) => (
  <div className={lessSpace ? "my-2" : "my-2"} id={id} >
    <h2 className="text-xl pb-7 font-medium">{title}</h2>
    {children}
    <div className="grid grid-cols-2 gap-4">
    <div />
    <Button
      disabled={disabled}
      className={cx(
        "w-full mt-9 text-white mb-7 pt-1",
        disabled ? "bg-j-gray-lighter" : buttonClassName
      )}
      {...rest}
      // style={{textAlign:'center', color: 'white' , paddingTop: '5x' , paddingBottom: '5px'}}
    >
      {buttonText}
    </Button>
    </div>
    
  </div>
);

export default BookingFormView;

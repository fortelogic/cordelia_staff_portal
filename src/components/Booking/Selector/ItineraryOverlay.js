import React, { useState } from "react";
import cx from "classnames";
import Overlay from "../../Overlay/Overlay";
import LoadingIcon from "../../Loading/LoadingIcon";
import CDN from "../../../utils/cdn";
import View from "../../View/View";
import Button from "../../Button/Button";

const ItineraryCard = ({ line }) => {
  const [buttonFlag, setButtonFlag] = useState(true);
  return (
    <div
      className="mt-4 bg-j-white"
      style={{
        position: "relative",
        textAlign: "center",
        borderRadius: "0.625rem",
        margin: "0 auto",
        marginBottom: "2rem",
        boxShadow: "5px 5px 30px -9px rgba(0,0,0,0.75)",
        color: "white",
      }}
    >
      <img
        src={CDN(line.image_url)}
        alt="Itinerary Image"
        style={{ height: "320px", borderRadius: "0.625rem" }}
      ></img>
      <div
        style={{
          position: "absolute",
          top: "50%",
          left: "50%",
          transform: "translate(-50%, -50%)",
        }}
      >
        {buttonFlag && line.title && <h4>{line.title}</h4>}
        {buttonFlag && line.sub_title && <h4>{line.sub_title}</h4>}
        {buttonFlag && line.ship_timings && <h4>{line.sub_title}</h4>}
        {buttonFlag && (
          <Button
            className="mb-3"
            style={{ background: "rgba(0, 0, 0, 0.6)" }}
            onClick={() => setButtonFlag(false)}
          >
            Learn More
          </Button>
        )}
      </div>
      {!buttonFlag && (
        <div
          style={{ color: "black", textAlign: "left", marginLeft: "0.625rem" }}
        >
          <br></br>
          <i
            className="fal fa-times cursor-pointer text-4xl float-right text-j-black"
            style={{ position: "relative", right: "2%" }}
            onClick={() => setButtonFlag(true)}
          />
          <br></br>
          {line.title && <h4>{line.title}</h4>}
          {line.sub_title && <h4>{line.sub_title}</h4>}
          <p style={{ margin: "1rem auto" }}>{line.content}</p>
          <br></br>
        </div>
      )}
    </div>
  );
};

const ItineraryOverlay = ({ onCancel, itineraryId, style = "back" }) => {
  const [itineraryContent, setItineraryContent] = React.useState([]);
  const [loading, setLoading] = useState(true);
  const [buttonFlag, setButtonFlag] = useState(true);
  const button =
    style === "back" ? (
      <i
        className="fal fa-arrow-left cursor-pointer text-3xl text-j-white"
        style={{ position: "relative", right: "-6%", marginTop: "-12%" }}
        onClick={onCancel}
      />
    ) : style === "close" ? (
      <i
        className="fal fa-times cursor-pointer text-4xl float-right text-j-white"
        style={{ position: "relative", right: "-6%", marginTop: "-12%" }}
        onClick={onCancel}
      />
    ) : null;

  const closeButtons =
    button && onCancel ? (
      <div className={cx("flex", { "justify-end": style === "close" })}>
        {button}
      </div>
    ) : null;
    React.useEffect(() => {
      if(itineraryId) {
        fetch(`${process.env.NEXT_PUBLIC_BACKEND_URL_API}/routes/${itineraryId}`)
        .then((res) => res.json())
        .then((content) => {
          setItineraryContent(content);
          setLoading(false);
        })
        .catch((error) => {
          setLoading(false);
        });
    }
  }, [itineraryId, loading]);
  const menu = loading ? (
    <View>
      {closeButtons}
      <div
        style={{
          margin: 0,
          position: "absolute",
          top: "50%",
          left: "50%",
          transform: "translateY(-50%)",
        }}
      >
        <LoadingIcon className="py-20 text-j-white" />
      </div>
    </View>
  ) : (
    <View>
      {closeButtons}
      <div
        className="mt-4 bg-j-white"
        style={{
          padding: "1rem",
          borderRadius: "10px",
          margin: "0 auto",
          marginBottom: "2rem",
          boxShadow: "5px 5px 30px -9px rgba(0,0,0,0.75)",
        }}
      >
        <h2>{itineraryContent.obj.title}</h2>
        <div
          className="mt-4"
          style={{
            background: "rgb(240, 244, 249)",
            padding: "1rem",
            borderRadius: "10px",
            margin: "0 auto",
            marginBottom: "2rem",
          }}
        >
          {itineraryContent.obj.ite.map((p) => (
            <h4
              className={cx("flex uppercase leading-tiny", "text-j-magenta")}
              key={p}
              style={{ paddingTop: "1rem", paddingBottom: "0px" }}
            >
              <div className={cx("text-center w-12")}>
                <i className="far fa-anchor"></i>
              </div>
              <span>{p}</span>
            </h4>
          ))}
        </div>
      </div>
      {itineraryContent.contents.map((line, index) => (
        <ItineraryCard line={line} />
      ))}
    </View>
  );

  return (
    <Overlay open className="bg-j-magenta px-5 pt-8 pb-16 itinerarymodal">
      {menu}
    </Overlay>
  );
};

export default ItineraryOverlay;

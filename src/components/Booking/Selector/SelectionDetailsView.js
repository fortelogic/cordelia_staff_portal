import React, { useState } from "react";
import cx from "classnames";
// import Gallery from "react-photo-gallery";
import Button from "../../Button/Button";
import Slider from "../../Slider/Slider";
import CDN from "../../../utils/cdn";
import BookingOverlay from "../General/BookingOverlay";
import { CancelView } from "../../View/FAQView";
import DetailCard from "./DetailCard";

export const SelectionDetailsView = ({
  rooms,
  selections,
  roomIndex: defaultRoomIndex,
  selectionIndex: defaultSelectionIndex,
  onClose,
  onSelect,
  deckRoom = null,
  noSwitch
}) => {
  const [roomIndex, setRoomIndex] = useState(defaultRoomIndex);
  const [selectionIndex, setSelectionIndex] = useState(defaultSelectionIndex);
  const getIndex = (currentIndex, length, delta) =>
    delta > 0
      ? delta + currentIndex === length
        ? 0
        : delta + currentIndex
      : delta + currentIndex < 0
        ? length - 1
        : delta + currentIndex;

  const room = rooms[roomIndex];
  for (let i = 0; i < selections.length; i++) {
    selections[i].images.map((ele) => {
      ele.source = `${process.env.NEXT_PUBLIC_IMAGE_HOSTNAME}${ele.source}`;
      return ele;
    })
  }
  const selection = selections[selectionIndex];
  const selectButton = deckRoom ? (
  <div>
      <Button
        className="w-full bg-j-red-light text-white my-8 pt-1"
        onClick={() => {
          onSelect(roomIndex, deckRoom.selection, deckRoom);
          let switchedToNext = false;
          if (!switchedToNext) onClose();
        }}
      >
        Proceed
      </Button>
  </div>
  ) : (
    <div>
      <Button
        className="w-full bg-j-red-light text-white my-8 pt-1"
        onClick={() => onSelect()}
      >
        Select
      </Button>
    </div>
  );

  return (
    <BookingOverlay
      style={noSwitch ? "back" : "close"}
      onCancel={onClose}
    >
      <Slider
        onStep={(delta) => {
          const newRoomIndex = getIndex(roomIndex, rooms.length, delta);
          setRoomIndex(newRoomIndex);
          setSelectionIndex(
            rooms[newRoomIndex].selected
              ? selections
                .map((s) => s.name)
                .indexOf(rooms[newRoomIndex].selected.name)
              : 0
          );
        }}
        className="mb-14"
        inverse
        uppercase
        showFirst={!noSwitch && roomIndex > 0}
        showLast={!noSwitch && roomIndex < rooms.length - 1}
      >
        Cabin {room.id + 1}
      </Slider>
      <Slider
        onStep={(delta) =>
          setSelectionIndex(
            getIndex(selectionIndex, selections.length, delta)
          )
        }
        showFirst={!noSwitch && selectionIndex > 0}
        showLast={!noSwitch && selectionIndex < selections.length - 1}
        className="mb-8"
      >
        {selection.name}
      </Slider>
      <div className="overflow-hidden w-full">
      {/* <Gallery
          photos={selection.images.map((img) =>
            Object.assign({}, img, { thumbnail: CDN(img.src), src: CDN(img.src)})
          )}
          targetRowHeight={120}
      /> */}
      </div>

      {/* {selectButton} */}

      <div className="pt-2">
        <DetailCard
          title="Room features"
          description={[selection.features[0]]}
          id="features"
        >
            {
              selection.features.map(feature => <p className="p-2 text-sm">{feature}</p>)
            }
          </DetailCard>
        <DetailCard
          title="Inclusions"
          description={[selection.inclusions[0]]}
          id="features"
        >
            {
              selection.inclusions.map(feature => <p className="p-2 text-sm">{feature}</p>)
            }
          </DetailCard>
        <DetailCard
          title="Price details"
          description={[
            <span key="total">Total</span>,
            <span key="price">
              &#x20B9; {(selection.price.total || 0).toLocaleString("hi-IN")}
            </span>,
          ]}
          id="features"
        >
          <PriceDetails {...selection.price} />
          <h4 className="flex justify-between font-bold text-j-magenta">
          </h4>
        </DetailCard>
        <DetailCard title="Cancellation and Reschedule Policy">
          <div>
            <CancelView>
              <h3 style={{ color: "#500E4B", fontWeight: "600" }}>
                <u>
                  Rescheduling Fee for sailings May, Jun &amp; July 31, 2021
                </u>
              </h3>
              <table style={{color: "black", borderCollapse: "collapse", width: "100%"}}>
                <tr>
                  <th style={{border: "1px solid #dddddd", textAlign: "left", padding: "8px",}}>Days to Departure</th>
                  <th style={{border: "1px solid #dddddd", textAlign: "left", padding: "8px",}}>Rescheduing Fee</th>
                </tr>
                <tr>
                  <td style={{border: "1px solid #dddddd", textAlign: "left", padding: "8px",}}>More than 8 days</td>
                  <td style={{border: "1px solid #dddddd", textAlign: "left", padding: "8px",}}>NIL Fee + Fare Difference</td>
                </tr>
                <tr>
                  <td style={{border: "1px solid #dddddd", textAlign: "left", padding: "8px",}}> 0 to 7 days</td>
                  <td style={{border: "1px solid #dddddd", textAlign: "left", padding: "8px",}}>INR 5000 per cabin + Fare difference</td>
                </tr>
              </table>
              <br></br>
              <ul style={{color: "black", listStyle: "disc", marginLeft: "1rem",}}>
                <li>
                  Free rescheduling is limited to one time<sup>*</sup>
                </li>
                <li>
                  Rescheduled itinerary must commence on or before 31 st March
                  2022
                </li>
              </ul>
              <br></br>
            </CancelView>
            <CancelView title="" theme="blue">
              <h3 style={{ color: "#500E4B", fontWeight: "600" }}>
                <u>
                  Rescheduling Fee for sailings from 1st August 2021 onwards
                </u>
              </h3>
              <table style={{color: "black", borderCollapse: "collapse", width: "100%"}}>
                <tr>
                  <th style={{border: "1px solid #dddddd", textAlign: "left", padding: "8px",}}>Days to sailing date</th>
                  <th style={{border: "1px solid #dddddd", textAlign: "left", padding: "8px",}}>Rescheduing Fee</th>
                </tr>
                <tr>
                  <td style={{border: "1px solid #dddddd", textAlign: "left", padding: "8px",}}>More than 31 days</td>
                  <td style={{border: "1px solid #dddddd", textAlign: "left", padding: "8px",}}>NIL Fee + Fare difference</td>
                </tr>
                <tr>
                  <td style={{border: "1px solid #dddddd", textAlign: "left", padding: "8px",}}>0 to 30 days</td>
                  <td style={{border: "1px solid #dddddd", textAlign: "left", padding: "8px",}}>INR 5000 per cabin + Fare difference</td>
                </tr>
              </table>
              <br></br>
              <ul style={{color: "black", listStyle: "disc", marginLeft: "1rem",}}>
                <li>
                  Free rescheduling is limited to one time<sup>*</sup>
                </li>
              </ul>
              <br></br>
            </CancelView>
            <CancelView title="">
              <h3 style={{ color: "#500E4B", fontWeight: "600" }}>
                CANCELLATION POLICY
              </h3>
              <table style={{color: "black", borderCollapse: "collapse", width: "100%"}}>
                <tr>
                  <th style={{border: "1px solid #dddddd", textAlign: "left", padding: "8px",}}>Days to departure</th>
                  <th style={{border: "1px solid #dddddd", textAlign: "left", padding: "8px",}}>Cancellation Fees</th>
                </tr>
                <tr>
                  <td style={{border: "1px solid #dddddd", textAlign: "left", padding: "8px",}}>More than 31 days</td>
                  <td style={{border: "1px solid #dddddd", textAlign: "left", padding: "8px",}}>25% of cabin fare</td>
                </tr>
                <tr>
                  <td style={{border: "1px solid #dddddd", textAlign: "left", padding: "8px",}}>0 to 30 days</td>
                  <td style={{border: "1px solid #dddddd", textAlign: "left", padding: "8px",}}>50% of cabin fare</td>
                </tr>
                <tr>
                  <td style={{border: "1px solid #dddddd", textAlign: "left", padding: "8px",}}>For No-Show</td>
                  <td style={{border: "1px solid #dddddd", textAlign: "left", padding: "8px",}}>100% of cabin fare</td>
                </tr>
              </table>
              <br></br>
              <ul style={{color: "black", listStyle: "disc", marginLeft: "1rem",}}>
                <li>Cancellation fee will be applicable only on cabin fare,</li>
                <li>
                  Port Charges &amp; gratuity will be refunded completely.
                </li>
                <li>GST for the refunded amount will also be returned.</li>
                <li>
                  GST will be applicable on rescheduling fee + Fare difference.
                </li>
                <li>
                  GST will be applicable for international sailing on
                  rescheduling fee + Fare difference.
                </li>
              </ul>
            </CancelView>
          </div>
        </DetailCard>
      </div>
      
      {selectButton}
    </BookingOverlay>
  );
};

export default SelectionDetailsView;

const PriceDetails = ({ individual, discounts, taxes, total }) => {
  const subTotal =
    individual.reduce((sum, i) => sum + i.total, 0) +
    (discounts ? discounts.reduce((sum, i) => sum + i.total, 0) : 0);
  const totalTaxes = taxes.reduce((sum, i) => sum + i.value, 0);

  return (
    <div>
      {individual.map((x, i) => (
        <PriceBlock
          key={i}
          title={x.type || `Person ${i + 1}`}
          value={x.total}
          positions={[
            { description: "Fare", value: x.fare },
            { description: "Discount", value: x.discount },
            { description: "Service Charges & Levies", value: x.portCharges+x.gratuity },
          ]}
        />
      ))}
      {discounts && (
        <PriceBlock
          title={"Group discount"}
          value={discounts.total}
          positions={discounts.details}
        />
      )}
      <PriceBlock
        title={"Sub-total"}
        value={subTotal}
        positions={[{ description: "Taxable sub-total", value: subTotal }]}
        bold
      />
      <h2 className="flex justify-between font-bold">
        <span className="text-2xl">Total</span>
        <span className="text-2xl">&#x20B9; {(total || 0).toLocaleString("hi-IN")}</span>
      </h2>
    </div>
  );
};

const PriceBlock = ({ title, value, bold, positions }) => (
  <div className="mb-8">
    <h4
      className={cx(
        "flex justify-between leading-loose pb-0",
        bold ? "font-bold" : "font-medium"
      )}
    >
      <span>{title}</span>
      <span>&#x20B9; {(value || 0).toLocaleString("hi-IN")}</span>
    </h4>
    {positions.map((p, i) => (
      <p className="flex justify-between leading-loose">
        <span>{p.description}</span>
        <span>&#x20B9; {(p.value || 0).toLocaleString("hi-IN")}</span>
      </p>
    ))}
  </div>
);

import React, { Component, useEffect, useState, useContext } from 'react';
import BookingFormView from "./General/BookingFormView";
import Tile from "../Tile/Tile";
import Button from "../Button/Button";
import cx from "classnames";
import LoadingIcon from "../Loading/LoadingIcon";
import SelectorOverlay from "./Selector/SelectorOverlay";
import SelectionDetailsView from "./Selector/SelectionDetailsView";
import UserContext from "../../store/UserContext";
import { getToken } from "../../utils/common";

var roomselectedValue;
const RoomSelectionView = ({nextStep, prevStep}) => {
    const [user, setUser] = useContext(UserContext);
    const booking = user.booking || {};
    const [selection, setSelection] = useState(null);
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState(false);
    const [roomIndex, setroomIndex] = useState(null);
    const [open, setOpen] = useState(null);
    const [preview, setPreview] = useState(null);
    const [itinerary, setItinerary] = useState(booking.itinerary)
    // const { itinerary_data, itinerary_error, itinerary_loading } = useQuery(GET_ITINERARIES, {
    //     variables: {},
    //     skip: !!selection || itinerary,
    //   });
    //   useEffect(() => {
    //     if (itinerary_data && itinerary_data.itineraries) {
    //       setItinerary(itinerary_data.itineraries[0]);
    //     }
    //   }, [itinerary_data, itinerary_error, itinerary_loading]);

    useEffect(() => {
        var itinerary = booking.itinerary
        fetch(process.env.REACT_APP_API_ENDPOINT+'/agent_portal/itineraries/'+itinerary.id+'/check_availability', {
            method: 'POST',
            headers: {
              Accept: "application/json",
              "Content-Type": "application/json",
              Authorization:
              `Bearer ${getToken()}`,
            },
            body: JSON.stringify({
                    visitorID: booking.visitorID,
                    itineraryID: itinerary && itinerary.id,
                    promoCode: "", // TODO: connect landing page promo codes here
                    rooms: booking.rooms.map((room) => {
                        const { adults, infants, children } = room;
                        return {
                            adults,
                            infants,
                            children
                        };
                    }),
            })
        }).then((res) => {
            if (res.ok) {
                return res.json();
            } else {
              throw new Error('Something went wrong');
            }
        }).then((response) => {
          setSelection(response.data.checkAvailability)
          setLoading(false)
        }).catch((error) => {
          setError(true)
          setLoading(false)
        });
    }, [loading]);

    if (loading || error || !selection) {
        return (
          <h1 className="text-4xl">
            <LoadingIcon className="py-20 text-j-magenta" />
          </h1>
        );
    }

    const onSelect = (index, selected, fromSelector, selected_room) => {
        const rooms = [...booking.rooms];
        if(selected_room){
          rooms[index] = Object.assign({}, rooms[index], { selected }, {"room_id": selected_room.itinerary_room_id, "total_price": selected_room.total_price, "room_number": selected_room.number});
        }else{
          rooms[index] = Object.assign({}, rooms[index], { selected }, {"room_id": null , "total_price": null, "room_number": null});
        }
        roomselectedValue = index == rooms.length - 1 && !rooms.filter((room) => !room.selected).length;
        if(roomselectedValue) {
          setUser(
            Object.assign({}, user, {
              booking: Object.assign({}, booking, {
                rooms,
              }),
            })
          );
        } else {
          setUser(
            Object.assign({}, user, {
              booking: Object.assign({}, booking, {
                rooms,
                roomsSelected: roomselectedValue,
              }),
            })
          );
        }
      };

    const conformingPaymentDetails = (requestData) => {
        let bookingInput = {}
        let visitorsID = window.localStorage.getItem('store');
        let bookingID = JSON.parse(visitorsID);
        let visitorsIDNew = bookingID.visitorID;
        let itineryID = bookingID.booking.itinerary.id;
            bookingInput.rooms = requestData.rooms.map((room) => ({
            priceKey: room.selected.priceKey,
            roomId: room.room_id,
        }));
        fetch(`${process.env.REACT_APP_API_ENDPOINT}/agent_portal/bookings`, {
            method: 'POST',
            headers: {
              Accept: "application/json",
              "Content-Type": "application/json",
              Authorization:
              `Bearer ${getToken()}`,
          },
            body: JSON.stringify({
            variables: { input: bookingInput ,visitorID: visitorsIDNew, itineraryID: itineryID },
        })
        }).then(response => response.json())
        .then(response => {
            if(response.booking){
              window.localStorage.setItem('booking', JSON.stringify(response.booking));
              if(booking.invoice_url){ delete booking.invoice_url}
                setUser(
                   Object.assign({}, user, {
                      booking: Object.assign({}, booking, {
                          number: response.booking.number
                        }),
                    })
                )
            //   setPriceDeclaration(true)
            }else{
            //   setPriceDeclaration(false)
            //   setErrorMsg(response.data)
            }
        }).catch(err => err);

        nextStep();
       
    }

    const backbuttonpressed= () => {
        prevStep()
    }

    const isComplete = !booking.rooms.filter((room) => !room.selected).length;

    return(
            <>
                <BookingFormView
                    title="Find your perfect room"
                    buttonText="Proceed to passenger information"
                    lessSpace={true}
                    onClick={() => {
                        conformingPaymentDetails(booking);
                        window.scrollTo({ top: 0, behavior: "smooth" })
                      }
                      }
                      id="otp-form"
                      disabled={!isComplete}
                >
                     {open && <div style={{zIndex:999999}}>
                        <div style={{position: "fixed", backgroundColor: "rgba(0,0,0, 0.2)", zIndex:999, top: "0px", left: "0px", right: "0px", bottom: "0px"}} onClick={() => setOpen(false)}></div>
                            <SelectorOverlay className="disable-scrollbars itinerarymodal" onClose={() => setOpen(null)} rooms={booking.rooms} selections={selection} itinerary={booking.itinerary} onSelect={onSelect} selectedCategory={open} roomIndex={roomIndex}/>
                        </div>
                    }
                    <div className="grid grid-cols-1 md:grid-cols-2 gap-4">
                            {booking.rooms.map((room, i) => (
                                <RoomView
                                    room={room}
                                    changePop={(selectedCategory, index) => {setroomIndex(index);setOpen(selectedCategory) }}
                                    selections={selection[i].categories}
                                    roomIndex={i}
                                    onSelect={(selection) => onSelect(i, selection)}
                                    onDetails={(room, selection, selectionIndex) => {
                                    setPreview(Object.assign([room.id, selectionIndex]));
                                    }}
                                />
                            ))}
                            {preview && (
                                <div style={{zIndex:999999}}>
                                    <div style={{position: "fixed", backgroundColor: "rgba(0,0,0, 0.2)", zIndex:999, top: "0px", left: "0px", right: "0px", bottom: "0px"}} onClick={() => setPreview(null)}></div>
                                    <SelectionDetailsView
                                        rooms={booking.rooms}
                                        selections={selection[preview[0]].categories}
                                        selectionIndex={preview[1]}
                                        roomIndex={preview[0]}
                                        onClose={() => setPreview(null)}
                                        onSelect={() => {
                                            setroomIndex(preview[1])
                                            setOpen(selection[preview[0]].categories[preview[1]]);
                                            setPreview(null);
                                        }}
                                    />
                                </div>
                            )}
                    </div>
                </BookingFormView>
            </>
    )
}

export default RoomSelectionView;

const RoomView = ({ room, onSelect, selections, onDetails, changePop, roomIndex }) => {
    const selection = room.selected ? (
        <RoomCard
          room={room.selected}
          roomId={room.id}
          selected
          changePop = {changePop}
          onChange={() => onSelect(null)}
          onDetails={(selection) =>
            onDetails(
              room,
              selection,
              selections.map((s) => s.name).indexOf(room.selected.name)
            )
          }
          roomIndex={roomIndex}
          roomNumber={room.room_number}
        />
      ) : (
          selections.map((r, i) => (
            <RoomCard
              room={r}
              key={i}
              roomId={room.id}    
              changePop = {changePop}
              onChange={(selection) => onSelect(selection)}
              onDetails={(selection) => onDetails(room, selection, i)}
              roomIndex={roomIndex}
            />
          ))
        );
    
    return (
        <Tile theme="magenta" shadow className="my-2 bg-bgcolor rounded-lg" id={`room-${room.id}`}>
            <Tile.Inner>
                <h5 className="uppercase">Cabin {room.id + 1} - {room.adults + room.children + room.infants} Guests</h5><br/>
                {selection}
            </Tile.Inner>
        </Tile>
    );
}

const RoomCard = ({ room, roomId, onChange, selected, onDetails, changePop, roomIndex, roomNumber }) => {
    return (
        <Tile className="mb-8 relative rounded-lg">
            <Tile.Inner
                tiny
                className={cx(
                "rounded-big",
                room.popular ? "bg-j-orange" : "bg-j-white"
                )}
                theme={room.popular ? "orange" : "white"}
            >
                <div className="flex justify-between">
                    <h4
                        className={cx(
                        "font-bold pb-0 self-center",
                        room.popular ? "text-white" : "text-j-magenta"
                        )}
                    >
                        {room.name}
                    </h4>
                    <Button
                        className={cx(
                        "border self-center pt-1",
                        room.popular
                            ? "border-white bg-j-orange text-j-white"
                            : "border-j-gray bg-j-white text-j-gray"
                        )}
                        onClick={() => {selected ? onChange(room) : changePop(room, roomIndex)}}
                    >
                        {selected ? "Change" : "Select"}
                    </Button>
                   
                </div>
                { selected &&
                        <div className="pt-3">
                            <div className="text-xs text-j-green">Room No: {roomNumber}</div>
                        </div>
                        }
                        <div className="flex justify-between pt-5">
                        <a
                            className={cx(
                            "text-xs uppercase cursor-pointer self-center",
                            room.popular ? "text-j-white" : "text-j-orange"
                            )}
                            onClick={() => {
                            onDetails(room)
                            }}
                        >
                            Details
                        </a>
                        <h2
                            className={cx(
                            "leading-none self-center pb-0 pr-6 text-2xl font-normal",
                            room.popular ? "text-j-white" : "text-j-black"
                            )}
                        >
                            &#x20B9; {(room.price.total || 0).toLocaleString("hi-IN")}
                        </h2>
                        </div>
            </Tile.Inner>
        </Tile>
    )
}
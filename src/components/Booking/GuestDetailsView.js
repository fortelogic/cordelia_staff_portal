import React, { Component, useContext, useState, useEffect } from 'react';
import BookingFormView from "./General/BookingFormView";
import Tile from "../Tile/Tile";
import Button from "../Button/Button";
import cx from "classnames";
import InputField from "../Form/InputField";
import SelectField from "../Form/SelectField";
import CountryStates from "../../public/country-states.json";
import CountryCodes from "../../public/country-phone";
import PhoneField from "../Form/PhoneField";
import Joi from "joi";
import { joiResolver } from "@hookform/resolvers/joi";
import { useForm } from "react-hook-form";
import { name, phone, email, date, gender } from "../../utils/validations";
import UserContext from "../../store/UserContext";
import moment from "moment";
import { getToken } from "../../utils/common";

class GuestDetailsView extends Component {
    documentData;
    constructor(props) {
        super(props);
        this.state = {
            booking:[],
            selection: []
        }
    }

    render() {
        return(
            <>
                {/* <div className="grid grid-cols-3 border-b p-6">
                    <div>
                        <button onClick={this.backbuttonpressed}>
                            Back
                        </button>
                    </div>
                    <div className="text-center text-basecolor font-bold">Title of cruise trip</div>
                </div> */}
                <GuestDetailsSubView nextStep={this.props}/>
            </>
        )
    }
}

export default GuestDetailsView;

const GuestDetailsSubView = (nextStep) => {
  const [user, setUser] = useContext(UserContext);
  const [showUpsell, setShowUpsell] = useState(false);
  const [submitted, setSubmitted] = useState(false);
  const [errorMsg, setErrorMsg] = useState(false);
  const booking = user.booking || {};
  const { rooms } = booking;
  const joiObject = {};

  const referenceDate = booking.itinerary
    ? new Date(booking.itinerary.startTime)
    : new Date();

  const adultMaxDate = new Date(
    new Date(referenceDate).setMonth(referenceDate.getMonth() - 12 * 12)
  );
  const childMaxDate = new Date(
    new Date(referenceDate).setMonth(referenceDate.getMonth() - 2 * 12)
  );
  const infantMaxDate = new Date(
    new Date(referenceDate).setMonth(referenceDate.getMonth() - 6)
  );

  rooms.forEach((room, roomIndex) => {
    const count = room.adults + room.children + room.infants;
    for (let guestIndex = 0; guestIndex < count; guestIndex++) {
      const guestType =
        guestIndex < room.adults
          ? "adult"
          : guestIndex < room.adults + room.children
            ? "child"
            : "infant";
      const prefix = `${roomIndex}_${guestIndex}`;
      joiObject[`${prefix}_name`] = name;
      joiObject[`${prefix}_meal`] = name;
      joiObject[`${prefix}_gender`] = gender;
      joiObject[`${prefix}_dob`] =
        guestType === "adult"
          ? date.max(adultMaxDate)
          : guestType === "child"
            ? date.max(childMaxDate).min(adultMaxDate)
            : date.max(infantMaxDate).min(childMaxDate);
      joiObject[`${prefix}_country`] = name;
      joiObject[`${prefix}_state`] = name;
      if (guestType === "adult") {
        joiObject[`${prefix}_phone`] = phone;
        joiObject[`${prefix}_email`] = email;
      }
    }
  });

  const { register, errors, handleSubmit, setError } = useForm({
    resolver: joiResolver(Joi.object(joiObject)),
  });

  function createBookingApi(requestData)
  {
      let bookingID = window.localStorage.getItem('booking');
      bookingID = JSON.parse(bookingID);
      bookingID = bookingID.id;
      return fetch(`${process.env.REACT_APP_API_ENDPOINT}/agent_portal/bookings/${bookingID}`, {
          method: 'PUT',
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization:
            `Bearer ${getToken()}`,
        },
          body: JSON.stringify(requestData)
      }).then(response => response.json())
      .then(response => {
        if(response.booking){
          booking.id = response.booking.id
          booking.guestsFilled = true
          setUser(Object.assign({}, user, {booking: booking}));
          localStorage.setItem('partialPayment', response.partial_payable_amount);
        }else{
          setSubmitted(false);
          setErrorMsg(response.data);
        }
    }).catch(err => err);
  }

  const onSubmit = (data) => {
    setSubmitted(true);
    const newRooms = [...rooms].map((room) =>
      Object.assign({}, room, {
        guests: [...(room.guests || [])].map((guest) =>
          Object.assign({}, guest)
        ),
      })
    );
    Object.keys(data).forEach((key) => {
      const [roomIndex, guestIndex, field] = key.split("_");
      const guest =
        newRooms[parseInt(roomIndex)].guests[parseInt(guestIndex)] || {};
      newRooms[parseInt(roomIndex)].guests[
        parseInt(guestIndex)
      ] = Object.assign({}, guest, { [field]: data[key] });
    });
    setUser(
      Object.assign({}, user, {
        booking: Object.assign({}, booking, {
          rooms: newRooms,
        }),
      })
    );

    let bookingInput = {}
    bookingInput.rooms = newRooms.map((room) => ({
      priceKey: room.selected.priceKey,
      roomId: room.room_id,
      guests: room.guests.map((guest, i) => ({
        name: guest.name,
        type:
          i < room.adults
            ? "ADULT"
            : i < room.adults + room.children
              ? "CHILD"
              : "INFANT",
        discountCategory: null,
          //i < room.adults ? getDiscountCategory(room.discounts[i]) : null, // TODO: what values? It can be pulled from room.discounts
        email: guest.email,
        phoneNumber: guest.phone,
        meal: guest.meal,
        idType: guest.doc,
        idNumber: guest.passNumber,
        gender: guest.gender.toUpperCase(),
        dateOfBirth: moment(guest.dob).format("YYYY-MM-DD"),
        country: guest.country,
        state: guest.state,
        citizenship: guest.country,
      })),
    }));

    createBookingApi({
      variables: { input: bookingInput },
    })
    setShowUpsell(false);
    nextStep.nextStep.nextStep()
    // setUser(
    //   Object.assign({}, user, {
    //     booking: Object.assign({}, booking, {
    //       guestsFilled: true,
    //     }),
    //   })
    // );
  };

  //  const handleSubmit = (e) => {
  //    e.preventDefault()
  //   nextStep.nextStep.nextStep()
  //  }

  const backbuttonpressed = (e) => {
    e.preventDefault()
    nextStep.nextStep.prevStep()
  }

  return (
      <>
        <BookingFormView
          title="Now let's add the details of the guests."
          buttonText="Proceed to checkout"
          onClick={handleSubmit(onSubmit)}
          lessSpace={true}
          id="guest-info-form"
          disabled={submitted}
        >
          {/* <p className="text-j-gray">
            To save your reservation, we need the details of each traveler per
            stateroom. Remember, first and last names must match what appears on
            government-issued photo IDs.
          </p> */}
          <form onSubmit={handleSubmit(onSubmit)}>
              {rooms.map((r, i) => (
              <RoomCard
                  key={i}
                  room={r}
                  roomIndex={i}
                  errors={errors}
                  register={register}
              />
              ))}
          </form>
          {!!Object.keys(errors).length && (
            <p className="text-j-red pt-4 flex">
              <i className="fas fa-exclamation-triangle text-2xl px-4 self-center" />
              <span className="self-center">
                Please enter all guests' details correctly before continuing.
              </span>
            </p>
          )}
        </BookingFormView>
      </>
      )
}

const RoomCard = ({ room, roomIndex, errors, register }) => {

    const count = room.adults + room.children + room.infants;
    const guests = [...(room.guests || Array(count).fill({}))];
    // const [open, setOpen] = useState(roomIndex === 0);

    if (guests.length < count)
      guests.push(...Array(count - guests.length).fill({}));
      guests.splice(count);

    // if (roomIndex == 0 && !guests[0].name) {
    //   guests[0].name = user.name || user.booking.name
    //   guests[0].phone = user.phone || user.booking.phone
    // }

    useEffect(() => {
      if (
        !!Object.keys(errors).filter((key) => key.startsWith(`${roomIndex}_`))
          .length
      ) {
        // setOpen(true);
      }
    }, [errors]);
    

    const guestForms = guests.map((guest, i) => (
        <GuestForm
          // className={!open && "hidden"}
          guestType={
            i < room.adults
              ? "Adult"
              : i < room.adults + room.children
                ? "Child"
                : "Infant"
          }
          key={i}
          index={i}
          roomIndex={roomIndex}
          guest={guest}
          errors={errors}
          register={register}
        />
      ));
      
    return (
        <Tile theme="magenta" shadow className="my-5 rounded-lg" id={`room-${room.id}`}>
          <Tile.Inner>
          {roomIndex == 0 && <p className="text-j-white pb-4">
            To save your reservation, we need the details of each traveler per
            stateroom. Remember, first and last names must match what appears on
            government-issued photo IDs.
          </p>}
            <h4
              className="uppercase pb-4 flex justify-between cursor-pointer"
            //   onClick={() => setOpen(!open)}
            >
              <span className="text-white">Cabin {room.id + 1}</span>
              {/* <i
                className={cx("fal text-j-orange text-4xl leading-4", {
                  "fa-angle-down": !open,
                  "fa-angle-up": open,
                })}
              /> */}
            </h4>
            <p className="text-white">
              {room.selected.name}, {count} Guests
            </p>
            <div className="grid grid-cols-1 sm:grid-cols-1 md:grid-cols-1 lg:grid-cols-1 gap-4">
              {guestForms}
            </div>
          </Tile.Inner>
        </Tile>
      );
}

const GuestForm = ({
    index,
    roomIndex,
    guest,
    guestType,
    errors,
    register,
    className,
  }) => {
    // defaultValue={moment(guest.dob).format("YYYY-MM-DD")}
    const [type, setType] = useState("text");
    const [countryStatesArray, setCountryState] = useState(["Maharashtra", "Tamil Nadu", "Karnataka", "Kerala", "Andaman and Nicobar Islands", "Andhra Pradesh", "Arunachal Pradesh", "Assam", "Bihar", "Chandigarh", "Chhattisgarh", "Dadra and Nagar Haveli", "Daman and Diu", "Delhi", "Goa", "Gujarat", "Haryana", "Himachal Pradesh", "Jammu and Kashmir", "Jharkhand", "Lakshadweep", "Madhya Pradesh", "Manipur", "Meghalaya", "Mizoram", "Nagaland", "Orissa", "Pondicherry", "Punjab", "Rajasthan", "Sikkim", "Tripura", "Uttaranchal", "Uttar Pradesh", "West Bengal"]);
    const [country, setCountry] = useState("India");
    const [phoneCode, setCountryCode] = useState("+91");
    const prefix = `${roomIndex}_${index}`;
  
    return (
      <div className={cx("pt-12", className)}>
        <h4 className="text-white">
          Guest {index + 1}: {guestType}
        </h4>
        <div className="grid grid-cols-2">
            <div className="mt-4">
              <label className="pl-4">Full name:</label>
              <InputField
                  icon="fal fa-user"
                  // placeholder="Full name"
                  name={`${prefix}_name`}
                  withIcon
                  lessSpace
                  defaultValue={guest.name}
                  ref={register({ required: true })}
                  inverseError
                  error={
                      errors && errors[`${prefix}_name`] && "Please enter a valid name"
                  }
              />
            </div>
            <div className="grid grid-cols-2">
              <div className="mt-4">
                <label className="pl-4">Date of Birth:</label>
                <InputField
                    icon="fal fa-calendar-alt"
                    type="date"
                    onFocus={() => setType("date")}
                    onBlur={() => setType("date")}
                    // placeholder="DOB: "
                    name={`${prefix}_dob`}
                    withIcon
                    lessSpace
                    ref={register({ required: true })}
                    inverseError
                    error={
                        errors &&
                        errors[`${prefix}_dob`] &&
                        `Please enter a valid date for ${guestType}`
                    }
                />
              </div>
              <div className="mt-4">
                <label className="pl-4">Gender:</label>
                <SelectField
                    icon="fal fa-utensils"
                    name={`${prefix}_gender`}
                    withIcon
                    lessSpace
                    placeholder="Gender"
                    ref={register({ required: true })}
                    inverseError
                    error={errors && errors[`${prefix}_gender`] && "Please select a gender"}
                    >
                    <option selected value="" disabled>Choose gender Type</option>
                    <option value="male">Male</option>
                    <option value="female">Female</option>
                </SelectField> 
              </div>
            </div>
            <div>
                <label className="pl-4">Country:</label>
                <SelectField
                    icon="fal fa-globe"
                    name={`${prefix}_country`}
                    withIcon
                    lessSpace
                    ref={register({ required: true })}
                    value={country}
                    onChange={(event) =>  {
                        let x = event.target.value;
                        setCountry(x);
                        setCountryState(CountryStates.countries.filter((country) => country.country === x)[0].states);
                        setCountryCode(`+${CountryCodes.filter((country) => country.Name === x)[0].Dial}`);
                    }}
                    inverseError
                    error={errors && errors[`${prefix}_country`] && "Please choose"}
                    >
                    {
                        CountryStates.countries.map((country) => {
                        return  <option value={country.country} selected>{country.country}</option>
                        })
                    }
                  </SelectField>
            </div>
            <div>
                <label className="pl-4">State:</label>
                <SelectField
                    icon="fal fa-building"
                    name={`${prefix}_state`}
                    withIcon
                    lessSpace
                    defaultValue={guest.state || ""}
                    ref={register({ required: true })}
                    inverseError
                    error={errors && errors[`${prefix}_state`] && "Please choose"}
                    >
                    {
                        countryStatesArray.map((states) => {
                        return  <option value={states} selected>{states}</option>
                        })
                    }
                </SelectField>
            </div>
            {guestType === "Adult" && (
                <div>
                  <label className="pl-4">Mobile Number:</label>
                  <PhoneField
                    name={`${prefix}_phone`}
                    placeholder="A 10-digit mobile number"
                    ref={register({ required: true })}
                    defaultValue={guest.phone}
                    countryPhoneCode={phoneCode}
                    lessSpace
                    inverseError
                    error={
                      errors &&
                      errors[`${prefix}_phone`] &&
                      "Please enter a correct phone number"
                    }
                  />
                </div>
            )}
             {guestType === "Adult" && (
                  <div>
                    <label className="pl-4">Email:</label>
                    <InputField
                          icon="fal fa-envelope"
                          name={`${prefix}_email`}
                          placeholder="Email Address"
                          ref={register({ required: true })}
                          defaultValue={guest.email}
                          lessSpace
                          inverseError
                          error={
                              errors &&
                              errors[`${prefix}_email`] &&
                              "Please enter a valid email"
                          }
                      />
                  </div>
            )}
            <div className="grid grid-cols-2">
              <div>
                <label className="pl-4">Meal:</label>
                <SelectField
                  icon="fal fa-utensils"
                  name={`${prefix}_meal`}
                  withIcon
                  lessSpace
                  placeholder="Choose Meal Type"
                  ref={register({ required: true })}
                  inverseError
                  error={
                    errors &&
                    errors[`${prefix}_meal`] &&
                    `Please Pick your Meal Preference`
                  }
                >
                  <option selected value="" disabled>Choose Meal Type</option>
                  <option value="Veg">Vegetarian</option>
                  <option value="Jain">Jain</option>
                  <option value="Non. Veg">Non - Vegetarian</option>
                </SelectField> 
              </div>
            </div>
        </div>
      </div>
    );
  };
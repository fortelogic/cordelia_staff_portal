import React, { Component, useContext, useState, useEffect, useRef } from 'react';
import BookingFormView from "./General/BookingFormView";
import Tile from "../Tile/Tile";
import Button from "../Button/Button";
import cx from "classnames";
import { Link, useHistory } from "react-router-dom";
import UserContext from "../../store/UserContext";
import { getToken } from "../../utils/common";
import SummaryView from "./General/SummaryView";
import LoadingIcon from "../Loading/LoadingIcon";
import InputField from "../Form/InputField";
import PhoneField from "../Form/PhoneFieldAlt";
import Joi from "joi";
import { joiResolver } from "@hookform/resolvers/joi";
import { useForm } from "react-hook-form";
import { name, phone, email, date, phone_pattern } from "../../utils/validations";
import CheckBox from "../Form/CheckBox";
import RadioField from "../Form/RadioField";
import CardPaymentForm from "./payment-forms/CardPaymentForm";
import BankPaymentForm from "./payment-forms/BankPaymentForm";
import UPIPaymentForm from "./payment-forms/UPIPaymentForm";
import WalletPayment from "./payment-forms/WalletPayment";
import scrollTo from "../../utils/scrollTo";

class ConformBookingView extends Component {
    documentData;
    constructor(props) {
        super(props);
        this.state = {
            booking:[],
            selection: []
        }
    }

    componentWillMount() {
        this.documentData = JSON.parse(localStorage.getItem('store'));
        if (localStorage.getItem('store')) {
            this.setState({booking: this.documentData})
            this.setState({selection: JSON.parse(localStorage.getItem('checkAvailability'))})
        }
    }

    backbuttonpressed = (e) => {
        e.preventDefault()
        this.props.prevStep()
    }

    handleSubmit = () => {

    }

    render() {
        return(
            <>
                <SummaryViewUI />
            </>
        )
    }
}

export default ConformBookingView;

const SummaryViewUI = ({ }) => {
    const [user, setUser] = React.useContext(UserContext);
    const booking = user.booking || {};
    let departure = booking.departure;
    let departureTime = booking.departure_time;
    let bookigStore = JSON.parse(window.localStorage.getItem('booking'));
    const [amountStatus, setAmount] = useState(true);
    const totalPrice = bookigStore.total;
    const twentyPercentage = window.localStorage.getItem('partialPayment');
    // const twentyPercentage = totalPrice*0.25;
    const remaingAmount = totalPrice-twentyPercentage;
    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_ENDPOINT}/agent_portal/wallet/balance`, {
        method: "GET",
        headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization:
            `Bearer ${getToken()}`,
        },
        })
        .then((response) => response.json())
        .then((response) => {
            if(response.amount>totalPrice) {
                setAmount(false)
            }
            
        })
    })
    // x = x.split(" - ");
  
    let bookingArr = booking.rooms;
    return (
        <>
            <div className="grid lg:grid-cols-2 md:grid-cols-2 sm:grid-cols-1 gap-2 pt-0 mt-4">
                <SummaryView booking={booking} />
                <div>
                        <ConformBooking amountStatus={amountStatus} booking={booking} totalPrice={totalPrice} twentyPercentage={twentyPercentage} remaingAmount={remaingAmount}/>
                </div>
            </div>
        </>
    )
}

const ConformBooking = ({amountStatus, booking, totalPrice, twentyPercentage, remaingAmount}) => {
    let history = useHistory();
    const [submitted, setSubmitted] = useState(false);
    const [user, setUser] = React.useContext(UserContext);
    let bookingID = window.localStorage.getItem('booking');
    var storageProfile= JSON.parse(localStorage.getItem('UserProfile'));
    const [billingData, setBillingData] = useState(null);
    bookingID = JSON.parse(bookingID);
    bookingID = bookingID.id;
    // const formRef = useRef(null)
    const isGiftCard = booking.giftCard;
    const [showTax, setShowTax] = useState(!isGiftCard && user.gstin);
    const [loading, setLoading] = useState(false);
    const [cardOpen, cardDetails] = useState(false);
    const [partialStatus, setPartialStatus] = useState(false);
    const [fullPay, setFullPay ] = useState(true);
    const [displayAmount, setDisplayAmount] = useState("Full")
    const submitRef = useRef(null);
    const [isFailed, setFailed] = useState(
        new URLSearchParams(window.location.search).get("fail")
    );

    const bookingInput = {
        contact: {
          name: "",
          email: "",
          phoneNumber: "",
        },
        paymentInfo: {
          plan: "",
          voucherNumber: "",
          promoCode: "",
          name: "",
          email: "",
        },
    };

    bookingInput.paymentInfo.type = "wallet"
    bookingInput.paymentInfo.amount = totalPrice

    // const handleSubmit = (requestData) => {
    //     setLoading(true);
    //     fetch(`https://cordelia.fortelogic.in/agent_portal/payments`, {
    //         method: 'POST',
    //         headers: {
    //             Accept: "application/json",
    //             "Content-Type": "application/json",
    //             Authorization:
    //             `Bearer ${getToken()}`,
    //         },
    //         body: JSON.stringify(requestData)
    //     }).then(response => response.json())
    //     .then(response => {
    //         if(response.status == 'Success') {
    //             setLoading(false);
    //             history.push("/admin/success");
    //         }
    //     }).catch(err => err);
    //     // history.push("/admin/success");
    // }

    const schema = Joi.object({
        name,
        phone,
        email,
        haveGstin: Joi.string().empty(""),
        gstin: Joi.string().empty(""),
        gstin_name: Joi.string().min(2).empty(""),
        gstin_phone: Joi.string().pattern(phone_pattern).empty(""),
      })
        .with("haveGstin", "gstin")
        .with("haveGstin", "gstin_name")
        .with("haveGstin", "gstin_phone");

    const { register, errors, handleSubmit, setError } = useForm({
        resolver: joiResolver(schema),
    });

    const submitHandler = handleSubmit((data) => {
        const { haveGstin, gstin, gstin_name, gstin_phone, ...rest } = data;
        const cleanData = Object.assign(
          {},
          rest,
          haveGstin && gstin && gstin_name && gstin_phone
            ? { gstin, gstin_name, gstin_phone }
            : {}
        );
        setBillingData(cleanData);
        cardDetails(true)
        setUser(
          Object.assign(
            {},
            user,
            { email: data.email },
            showTax
              ? {
                gstin: data.gstin,
                name: data.gstin_name,
                phone: data.gstin_phone,
              }
              : {}
          )
        );
    });

    const setFailedError = (error) => {
        setFailed(error);
        if (error) scrollTo("payment");
    };

    const onChangeValue = (status ,e) => {
        setFullPay(!fullPay);
        if(status==false) {
            setPartialStatus(true);
        } else {
            setPartialStatus(false);
        }
        // setPartialStatus(status);
        if(displayAmount == 'Full') {
            setDisplayAmount('Partial')
        } else {
            setDisplayAmount('Full');
        }
    }

    return(
        <>
            {/* <h1 className="alt">Billing details</h1> */}
            <Tile tiny className="mb-5 border border-j-gray-lighter rounded-lg bg-magenta" >
                <Tile.Inner className="pb-0">
                    {!cardOpen && <>
                    <h1 className="text-grey-600 text-sm">Billing details</h1> 
                    <div className="pt-10">
                        <form onSubmit={submitHandler}>
                            <InputField
                                placeholder="Full name"
                                name="name"
                                lessSpace
                                defaultValue={storageProfile.name}
                                ref={register({ required: true })}
                                error={errors && errors.name && "Please enter your full name"}
                            />
                            <PhoneField
                                name="phone"
                                placeholder="Phone number"
                                ref={register({ required: true })}
                                lessSpace
                                payementpage={true}
                                defaultValue={storageProfile.phone_number}
                                noIcon
                                error={
                                errors && errors.phone && "Please enter a correct phone number"
                                }
                            />
                            <InputField
                                placeholder="Email address"
                                type="email"
                                name="email"
                                lessSpace
                                defaultValue={storageProfile.email}
                                ref={register({ required: true })}
                                error={errors && errors.email && "Please enter a valid email address"}
                            />
                            <input
                                name="haveGstin"
                                type="hidden"
                                value={showTax ? "1" : ""}
                                ref={register()}

                            />
                            {/* {!isGiftCard && (
                                <CheckBox onChange={() => setShowTax(!showTax)} value={showTax}>
                                    I have GSTIN
                                </CheckBox>
                            )} */}
                            {/* <div className={showTax ? "" : "hidden"}>
                                <InputField
                                    placeholder="GSTIN"
                                    name="gstin"
                                    lessSpace
                                    defaultValue={(user.gstin && user.gstin.gstin) || ""}
                                    ref={register({ required: true })}
                                    error={errors && errors.gstin && "Please enter GSTIN"}
                                />
                                <InputField
                                    placeholder="Name"
                                    name="gstin_name"
                                    lessSpace
                                    defaultValue={
                                    (user.gstin && user.gstin.name) || user.name || booking.name
                                    }
                                    ref={register({ required: true })}
                                    error={errors && errors.gstin_name && "Please enter a name"}
                                />
                                <PhoneField
                                    name="gstin_phone"
                                    placeholder="Mobile number"
                                    ref={register({ required: true })}
                                    lessSpace
                                    defaultValue={
                                    (user.gstin && user.gstin.phone) || user.phone || booking.phone
                                    }
                                    noIcon
                                    error={
                                    errors &&
                                    errors.gstin_phone &&
                                    "Please enter a correct phone number"
                                    }
                                />
                            </div> */}
                            {/* uncomment below line once api done */}
                            <div>
                                {/* <label>
                                    <span className="text-j-magenta text-sm">Got an offer/coupon code?</span><br/>
                                    <div className="flex">
                                        <input type="text" style={{flex:1}} className="border border-grey-200 rounded-l pl-1" placeholder="Enter coupen code"/>
                                        <button type="submit" className="bg-j-orange text-white pl-2 pr-2">Apply</button>
                                    </div>
                                </label> */}
                                <h3 className="text-sm text-j-magenta font-semibold mt-4">GRAND TOTAL</h3>
                                <h1 className="text-2xl text-j-magenta font-bold">&#x20B9; {Math.round(totalPrice)}</h1> 
                                <div className="flex mt-2">
                                    <div className="w-9/12">
                                        {twentyPercentage>0 && <div className="radio">
                                            <label>
                                                <input
                                                type="radio"
                                                value="Partial"
                                                checked={displayAmount === "Partial"}
                                                onChange={(e)=>onChangeValue(partialStatus, e)}
                                                // className="pt-4"
                                                />
                                                <span className="text-xs pl-2 font-bold">Reserve now by paying 25%</span><br />
                                                {/* {!fullPay && <span className="pl-5" style={{fontSize:"8px"}}>DUE:{Math.round(remaingAmount)}</span>} */}
                                            </label>
                                        </div>
                                        }
                                        <div className="radio w-full mt-2 mb-4">
                                            <label>
                                                <input
                                                type="radio"
                                                value="Male"
                                                checked={displayAmount === "Full"}
                                                onChange={(e)=>onChangeValue(partialStatus, e)}
                                                />
                                                 <span className="text-xs pl-2 font-bold">Book Now by paying 100% and save on Foreign Exchange Fluctuation</span>
                                            </label>
                                        </div>
                                    </div>
                                    <div  className="w-3/12">
                                        <p className="text-xs" style={{fontSize:"8px"}}>AMOUNT PAYABLE</p>
                                        <h1 className="text-xl text-j-magenta font-bold">&#x20B9; {fullPay?Math.round(totalPrice) : Math.round(twentyPercentage)}</h1> 
                                        {!fullPay && <p className="text-xs text-j-magenta" style={{fontSize:"8px"}}>DUE AMOUNT : &#x20B9; {Math.round(remaingAmount)}</p>}
                                    </div>
                                </div>
                            </div>
                            <input type="submit" ref={submitRef} className="bg-j-red-light text-white w-full mb-12 p-3 rounded-small"/>
                        </form>
                    </div>
                    </>
                }
                {cardOpen &&
                    <div>
                        <div className="grid lg:grid-cols-2 md:grid-cols-2 sm:grid-cols-2 gap-2">
                            <h1 className="alt">Payment options</h1>
                            <button className="bg-grey-light focus:outline-none text-j-magenta font-bold py-2 px-4 rounded inline-flex items-center justify-end" onClick={()=>cardDetails(false)}>
                            <svg className="w-4 h-4 mr-2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"><path d="M11.739,13.962c-0.087,0.086-0.199,0.131-0.312,0.131c-0.112,0-0.226-0.045-0.312-0.131l-3.738-3.736c-0.173-0.173-0.173-0.454,0-0.626l3.559-3.562c0.173-0.175,0.454-0.173,0.626,0c0.173,0.172,0.173,0.451,0,0.624l-3.248,3.25l3.425,3.426C11.911,13.511,11.911,13.789,11.739,13.962 M18.406,10c0,4.644-3.763,8.406-8.406,8.406S1.594,14.644,1.594,10S5.356,1.594,10,1.594S18.406,5.356,18.406,10 M17.521,10c0-4.148-3.373-7.521-7.521-7.521c-4.148,0-7.521,3.374-7.521,7.521c0,4.148,3.374,7.521,7.521,7.521,7.521.7.52z"/></svg>
                            <span>Back</span>
                            </button>
                        </div>
                        <span className="text-j-gray">
                            <RadioField
                                name="paymentOption"
                                defaultValue="bank"
                                paymentStatus= {true}
                                options={[
                                    // {
                                    //     value: "card",
                                    //     label: "Credit Card / Debit Card",
                                    //     view: (
                                    //       <CardPaymentForm
                                    //         booking={booking}
                                    //         paymentPath="booking"
                                    //         billingData={billingData}
                                    //         partialStatus={partialStatus}
                                    //         submitRef={submitRef}
                                    //         setFailed={setFailedError}
                                    //         className="bg-black"
                                    //       />
                                    //     ),
                                    // },
                                    // {
                                    //     value: "bank",
                                    //     label: "Net Banking",
                                    //     view: (
                                    //       <BankPaymentForm
                                    //         booking={booking}
                                    //         paymentPath="booking"
                                    //         billingData={billingData}
                                    //         partialStatus={partialStatus}
                                    //         submitRef={submitRef}
                                    //         setFailed={setFailedError}
                                    //       />
                                    //     ),
                                    //   },
                                    //   {
                                    //     value: "upi",
                                    //     label: "UPI",
                                    //     view: (
                                    //       <UPIPaymentForm
                                    //         booking={booking}
                                    //         paymentPath="booking"
                                    //         billingData={billingData}
                                    //         partialStatus={partialStatus}
                                    //         submitRef={submitRef}
                                    //         setFailed={setFailedError}
                                    //       />
                                    //     ),
                                    //   },
                                      {
                                        value: "wallet",
                                        label: "Cordelia Wallet",
                                        view: (
                                          <WalletPayment
                                            booking={booking}
                                            paymentPath="booking"
                                            billingData={billingData}
                                            partialStatus={partialStatus}
                                            submitRef={submitRef}
                                            setFailed={setFailedError}
                                            amountStatus={amountStatus}
                                          />
                                        ),
                                      },
                                ]}
                            />
                        </span>
                    </div>
                }
                    
                </Tile.Inner>
            </Tile>
           
            {/* {loading &&  <h1 className="text-4xl">
            <LoadingIcon className="py-20 text-j-magenta" />
            </h1>}
            {!loading && <Button
                className="w-full border text-white border-j-magenta bg-j-red-light pt-1"
                onClick={()=>handleSubmit({
                    variables: { input: bookingInput },booking_id: bookingID
                })}
            >
            Confirm Booking
            </Button>
            } */}
        </>
        // <button
        //     className="bg-blue-500 text-white active:bg-blue-600 font-bold uppercase text-xs px-4 py-2 rounded shadow hover:shadow-md outline-none focus:outline-none mr-1 ease-linear transition-all duration-150"
        //     type="button"
        //     onClick={()=>handleSubmit({
        //         variables: { input: bookingInput },booking_id: bookingID
        //       })}
        //     disabled={amountStatus.amount}
        // >
        //     Confirm Booking
        // </button>
    )
}

const OrderDetail = ({ title, children, big }) => (
    <>
      <h4
        className={
          big ? "text-white text-3xl font-bold pt-4" : "text-white pt-2"
        }
      >
        {children}
      </h4>
    </>
);
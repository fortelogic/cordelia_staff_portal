import Tile from "../../Tile/Tile";
import React from "react";
import Button from "../../Button/Button";

export const SelectionCard = ({ booking, handleModify }) => {
  const { rooms } = booking;

  if (!rooms) return null;
  const roomTypes = {};
  rooms
    .filter((r) => r.selected)
    .forEach(({ selected: { name } }) => {
      roomTypes[name] = roomTypes[name] ? roomTypes[name] + 1 : 1;
    });

  return (
    <Tile shadow className="mb-2">
      <Tile.Inner
        className="rounded-lg flex justify-between bg-magenta text-white"
        tiny
      >
        <p className="text-xs leading-none uppercase self-center flex flex-col leading-relaxed">
          {Object.keys(roomTypes).map((k) => (
            <span key={k}>
              {roomTypes[k]} x {k}
            </span>
          ))}
        </p>
        <Button
          className="border border-j-white bg-j-orange text-j-white"
          onClick={() => handleModify(2)}
        >
          Change
        </Button>
      </Tile.Inner>
    </Tile>
  );
};

export default SelectionCard;

import React, { useState, useEffect } from "react";
import Button from "../../Button/Button";
import RouteCard from "../../RouteCard/RouteCard";

export const Card = (cardsData) => {
  let itinerary = localStorage.getItem("itinerary");
  itinerary = JSON.parse(itinerary);
  let array = cardsData.cardData;
  let groupedDays = null;
  groupedDays = array.reduce(function (r, a) {
    r[a.no_days] = r[a.no_days] || [];
    r[a.no_days].push(a);
    return r;
  }, Object.create(null));
  for (const key in groupedDays) {
    groupedDays[key] = groupedDays[key].reduce(function (r, a) {
      r[a.cruice_name] = r[a.cruice_name] || [];
      r[a.cruice_name].push(a);
      return r;
    }, Object.create(null));
  }
  return (
    <>
      {Object.keys(groupedDays).map((key) => (
        <RoutesView key={key} nightCount={key} mainObj={groupedDays} />
      ))}
    </>
  );
};

export default Card;

export const RoutesView = ({ nightCount, mainObj }) => {
  const [open, setOpen] = useState(false);
  const [dataArr, setDataArr] = useState([]);
  const id = `${nightCount}-nights`;
  let itinerary = localStorage.getItem("itinerary");
  itinerary = JSON.parse(itinerary);
  let groupedDays = mainObj;
  const onlyUnique = (value, index, self) => {
    return self.indexOf(value) === index;
  };
  const getNightCountIti = () => {
    let temp = null;
    temp = [];
    let count = nightCount || 2;
    let routesData = groupedDays[count];
    let arr = Object.values(routesData);
    for (let i in arr) {
      if (arr[i]) {
        let x = null;
        x = arr[i][0].itiId;
        for (let j in itinerary) {
          if (itinerary[j].id === x) {
            temp.push(itinerary[j]);
          }
        }
      }
    }
    let unique = [];
    unique = temp.filter(onlyUnique);
    setDataArr(unique);
  };
  return (
    <div className="mt-4" style={{ maxWidth: "90%", margin: "1rem auto" }}>
      <h2 className="flex justify-between cursor-pointer select-none" id={id}>
        <span className="pt-1">{nightCount}-Night Cruises</span>
        {!open ? (
          <Button
            className="bg-j-white text-j-orange border border-j-orange"
            style={{ whiteSpace: "nowrap" }}
            onClick={() => {
              setOpen(!open);
              getNightCountIti();
            }}
          >
            View Cruises
          </Button>
        ) : (
          <Button
            className="bg-j-white text-j-orange border border-j-orange"
            style={{ whiteSpace: "nowrap" }}
            onClick={() => {
              setOpen(!open);
              getNightCountIti();
            }}
          >
            Hide Cruises
          </Button>
        )}
      </h2>
      <br></br>
      <div className="grid grid-cols-1 lg:grid-cols-3 md:grid-cols-2 gap-4">
        {open &&
          dataArr.map((route) => (
            <RouteCard
              route={route}
              key={route.id}
              nightCount={nightCount}
              light
            />
          ))}
      </div>
    </div>
  );
};

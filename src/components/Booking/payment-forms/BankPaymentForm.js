import React, { useState, useEffect } from "react";
import cx from "classnames";
import PaymentForm from "./PaymentForm";
import CheckoutForm from "./CheckoutForm";
import SelectField from "../../Form/SelectField";

const banks = { hdfc: "HDFB", icici: "ICIB", axis: "AXIB", kotak: "162B" };

const BankPaymentForm = ({ booking, billingData,partialStatus, submitRef, setFailed, paymentPath, amount }) => {
  const [bank, setBank] = useState(banks[0]);
  const [submitted, setSubmitted] = useState(false);
  const [clicked, setClicked] = useState(false);
  const [showCheckout, setShowCheckout] = useState(false);
  const [moreBankFlag, setMoreBankFlag] = useState(false);

  const onSubmit = (e) => {
    console.log('test')
    e.preventDefault();
    setClicked(true);
    console.log('test',clicked)
    if (submitRef && submitRef.current) submitRef.current.click();
  };

  useEffect(() => {
    if(paymentPath == "wallet")
    {
      if (clicked) {
        setSubmitted(true);
        setClicked(false);
        setShowCheckout(true);
        console.log('test',showCheckout)
      }
    } else if( paymentPath == "booking") {
      if (clicked && billingData) {
        setSubmitted(true);
        setClicked(false);
        setShowCheckout(true);
        console.log('test',showCheckout)
      }
    }
  }, [billingData, clicked]);

  return (
    <>
      <PaymentForm onSubmit={onSubmit} submitted={submitted}>
        <div className="flex justify-around">
          {Object.keys(banks).map((name) => (
            <BankOption
              name={name}
              key={name}
              selected={bank === name}
              onClick={() => {
                setBank(name);
              }}
            />
          ))}
        </div>
        <a onClick={() => setMoreBankFlag(!moreBankFlag)} className="block mt-4 text-sm text-j-orange uppercase cursor-pointer">
          More banks
        </a>
        {
          moreBankFlag && (<SelectField name="BankExt" withIcon lessSpace placeholder="More Banks" value={bank}
          onChange={(event) =>  {
            let x = event.target.value;
            setBank(x);
          }}>
          <option disabled value="" selected>
            Select a Bank
          </option>
          <option value="BBRB">Bank of Baroda</option>
          <option value="BOIB">Bank of India</option>
          <option value="CABB">Canara Bank</option>
          <option value="DSHB">Deutsche Bank</option>
          <option value="IDFCNB">IDFC Netbanking</option>
          <option value="INIB">IndusInd Bank</option>
          <option value="JAKB">Jammu and Kashmir Bank</option>
          <option value="KRKB">Karnataka Bank</option>
          <option value="KRVB">Karur Vysya </option>
          <option value="CPNB">Punjab National Bank - Corporate Banking</option>
          <option value="PNBB">Punjab National Bank - Retail Banking</option>
          <option value="SOIB">South Indian Bank</option>
          <option value="SBBJB">State Bank of Bikaner and Jaipur</option>
          <option value="SBIB">State Bank of India</option>
          <option value="SBTB">State Bank of Travancore</option>
          <option value="UBIB">Union Bank of India</option>
          <option value="UNIB">United Bank Of India</option>
          <option value="VJYB">Vijaya Bank</option>
        </SelectField>)}
      </PaymentForm>
      {showCheckout ? (
        <CheckoutForm
          paymentPath = {paymentPath}
          booking={booking}
          partialStatus={partialStatus}
          billingData={billingData}
          amount={amount}
          {...{ bank, code: banks[bank] || bank}}
          cancel={(error) => {
            setShowCheckout(false);
            setSubmitted(false);
            if (error) setFailed(error);
          }}
        />
      ) : null}
    </>
  );
};

export default BankPaymentForm;

const BankOption = ({ name, selected, onClick }) => (
  <div
    style={{ width: 65 }}
    className="text-center text-j-gray cursor-pointer select-none"
    onClick={onClick}
  >
    <div
      style={{
        width: 65,
        height: 65,
      }}
      className={cx(
        "rounded-full border-2 flex justify-center",
        selected ? "border-j-orange" : "border-transparent"
      )}
    >
      <img
        src={`/images/banks/${name}.png`}
        style={{
          width: 57,
          height: 57,
        }}
        className="rounded-full bg-j-white self-center"
      />
    </div>
    <h1 className="alt">{name}</h1>
  </div>
);

import React, { Component, useState, useEffect } from "react";
import PaymentForm from "./PaymentForm";
import CheckoutForm from "./CheckoutForm";



const WalletPayment = ({ booking, billingData, partialStatus ,submitRef, setFailed, amountStatus, paymentPath }) => {
    const [submitted, setSubmitted] = useState(false);
    const [clicked, setClicked] = useState(false);
    const [showCheckout, setShowCheckout] = useState(false);

    const onSubmit = (e) => {
        e.preventDefault();
        setClicked(true);
        if (submitRef && submitRef.current) submitRef.current.click();
    };
    
    useEffect(() => {
        if (clicked && billingData) {
          setSubmitted(true);
          setClicked(false);
          setShowCheckout(true);
        }
    }, [billingData, clicked]);

    return(
        <>
            <PaymentForm
                buttonText="Pay Amount"
                onSubmit={onSubmit} 
                submitted={submitted}
            ></PaymentForm>
            {showCheckout ? (
                <CheckoutForm
                    booking={booking}
                    paymentPath={paymentPath}
                    billingData={billingData}
                    partialStatus={partialStatus}
                    wallet = {true}
                    cancel={(error) => {
                        setShowCheckout(false);
                        setSubmitted(false);
                        if (error) setFailed(error);
                    }}
                />
            ) : null}
        </>
      )
  }


export default WalletPayment;
import React, {useState, Component, setUser } from 'react';
import BookingFormView from "./General/BookingFormView";
import Tile from "../Tile/Tile";
import Button from "../Button/Button";
import cx from "classnames";
import scrollTo from "../../utils/scrollTo";
import UserContext from "../../store/UserContext";
import { CancelView } from "../View/FAQView";
import moment from "moment";


class PaymentSummaryView extends Component {
    
    render(){
        const { values } = this.props;
        return (
            <>
              <PaymentSummaryViewAPI nextStep={this.props}/>
            </>
        )
    }
}
export default PaymentSummaryView;



const PaymentSummaryViewAPI = (nextStep) => {
    const [user, setUser] = React.useContext(UserContext);
    const bookingMain = user.booking;
    const booking = user.booking.rooms || {};

    const backbuttonpressed = (e) => {
        e.preventDefault()
        nextStep.nextStep.prevStep()
    }

    const conformingPaymentDetails = (e) => {
        e.preventDefault()
        nextStep.nextStep.nextStep()
    }

    return(
        <>
            <BookingFormView
                buttonText="Continue"
                onClick={conformingPaymentDetails}
                lessSpace={true}
                id="otp-form"
                // disabled={!isComplete}
            >
                {PriceDetails(booking || [], bookingMain)}
            </BookingFormView>
        </>
    )
}

const PriceDetails = (bookingArr, bookingMainArr) => {
    const [open, setOpen] = useState(null);
    const [fareOpen, setFareOpen] = useState(false);
    let bookingStore = window.localStorage.getItem('booking');
    bookingStore = JSON.parse(bookingStore);
    const container = {
      margin: "15px",
      backgroundColor: "white",
      borderRadius: "10px"
    }
    const { rooms } = bookingMainArr;
    let agentPrice = 0;
    let grandTotalValue = 0;
    let grandTotalValueIncTax = 0;

    const guests = rooms
        .map((r) => r.adults + r.children + r.infants)
        .reduce((t, i) => t + i, 0);


    const selected = {};
    rooms.forEach((r) => {
        if (!selected[r.selected.name]) selected[r.selected.name] = 0;
        selected[r.selected.name]++;
    });

    bookingArr.forEach((booking) => {
        booking.selected.price.individual.forEach((r) => {
            agentPrice = r.fare + agentPrice;
        })
    })

    bookingArr.reduce((grandTotal, booking ) => {
        grandTotalValue = grandTotalValue+booking.selected.price.total;
    } , 0)

    grandTotalValueIncTax = grandTotalValue+(bookingStore.gst);
    
    return (
        <>
        <div className="grid lg:grid-cols-2 md:grid-cols-2 sm:grid-cols-1 gap-2">
        <span>
          <Tile tiny className="mb-5 border border-j-gray-lighter rounded-lg" style={{marginRight: '5px'}}>
              <Tile.Inner className="pb-0">
                  <OrderDetail title="SHIP">
                    {bookingMainArr.itinerary.ship.name}
                  </OrderDetail>
                  <div style={{display: "flex", justifyContent: "space-between"}}>
                    <div className="mr-2">
                      <OrderDetail title="Departure">
                        {bookingMainArr.departure}
                        <br></br>
                        {moment(bookingMainArr.itinerary.startTime).format("ddd, D MMM YYYY, h:mm a")}
                      </OrderDetail>
                    </div>
                    <div className="ml-2">
                      <OrderDetail title="Arrival">
                        {bookingMainArr.destination}
                        <br></br>
                        {moment(bookingMainArr.itinerary.endTime).format("ddd, D MMM YYYY, h:mm a")}
                      </OrderDetail>
                    </div>
                  </div>
                  {Object.keys(bookingArr)
                  .map((k, index) => 
                  
                    <OrderDetail title={`Cabin ${index+1}`}>
                        {bookingArr[index].selected.name}<br/>
                        Deck No: {((bookingArr[index].room_number).toString()).charAt(0)}<br/>
                        Room No: {bookingArr[index].room_number}<br/>
                        {bookingArr[index].adults+bookingArr[index].children+bookingArr[index].infants}  Guest{(bookingArr[index].adults+bookingArr[index].children+bookingArr[index].infants) > 1 ? "s" : null}
                    </OrderDetail>
                  )}
                  {/* <OrderDetail title="ROOM AND GUEST">
                    {Object.keys(selected)
                    .map((k) => `${selected[k]} x ${k}`)
                    .join(", ")}
                  , {guests} Guest{guests > 1 ? "s" : null} ,

                  </OrderDetail> */}
              </Tile.Inner>
            </Tile>
        </span>
        <span>
        <Tile tiny theme="white" className="mb-5 col-span-2 bg-transparent" style={{marginLeft: '5px', backgroundColor:'transparent'}}>
              <div className="border border-j-gray-lighter" style={{ backgroundColor: "white", borderRadius: "10px"}}>
                <div
                  style={{margin:'25px', color:'#500E4B', fontWeight:'bold'}}
                  className="grid grid-cols-1 grid-cols-2"
                  onClick={() => {
                      setOpen(!open);
                  }}
                >
                  <span className="pt-1"> PRICE DETAILS</span>
                  <i
                    style={{color:"grey", fontSize: 28, textAlign:'right'}}
                    className={cx("fas cursor-pointer text-j-gray-light text-4xl", {
                      "fa-angle-down": !open,
                      "fa-angle-up": open,
                    })}
                  ></i>
                </div>
                { open && bookingArr.map((booking, index) =>
                  <div key={index}>
                    <div style={{color:"black"}}>
                      {booking.selected.price.individual.map((price, index) =>
                        <div key={index} style={{margin:"25px"}}>
                          <div style={{width:'100%'}}>
                              <PaymentDetails content={price.type} details={Math.round((price.total)*100/100)} big={true} />
                              <PaymentDetails content="Cabin Fare:" details={Math.round((price.fare)*100/100)} big={false} />
                              {price.discount != '0' && <div className="grid grid-cols-1 grid-cols-2" style={{marginBottom:'7px'}}>
                                      <div style={{color:"grey", fontSize: 14}} className="float-left bold">Individual Discount</div>
                                      <div style={{color:"grey", fontSize: 14, textAlign:'right'}} className="float-right font-weight-bold text-capitalize">
                                          - &#x20B9;{price.discount}
                                      </div>
                                </div>}
                              <PaymentDetails content="Service Charges & Levies" details={price.portCharges+price.portCharges} big={false} />
                            </div>
                          </div>
                      )}
                    </div>
                  </div>
                )}

                {open && <div style={{margin:'25px' }}>
                  <PaymentDetails content="Promo Code:" details="0" big={true} />
                  <PaymentDetails content="New Year Promo:" details="0" big={false} />
                </div>
                }

                {open && <div style={{margin:'25px' }}>
                  <PaymentDetails content="Sub-Total:" details={bookingArr.reduce((grandTotal, booking ) => {
                          return (Math.round(grandTotal + booking.selected.price.total))} , 0)} big={true} />
                  <PaymentDetails content="Taxable Sub Total:" details={bookingArr.reduce((grandTotal, booking ) => {return (Math.round(grandTotal + booking.selected.price.total))} , 0)} big={false} />
                </div>
                }
                {open && <div style={{margin:'25px' }}>
                  <PaymentDetails content="Taxes:" details={Math.round(bookingStore.gst)} big={true} />
                  <PaymentDetails content="GST:" details={Math.round(bookingStore.gst)} big={false} />
                </div>
                }

                <div style={{margin:'25px' }}>
                  <PaymentDetails content="Gross Fare:" details={Math.round(grandTotalValueIncTax)} big={true} />
                </div>

                <div style={{margin:'25px'}}>
                    <PaymentDetails content="Agent Commission:" details={bookingStore.agent_commission} big={true} />
                </div>

                <div style={{margin:'25px'}}>
                    <PaymentDetails content="Net Payable to Cordelia:" details={bookingStore.total} big={true} />
                </div>
                <div
                  style={{
                    margin: "25px",
                    marginBottom: "25px",
                    color: "#500E4B",
                    fontWeight: "bold",
                  }}
                  className="grid grid-cols-1 grid-cols-2"
                  onClick={() => {
                    setFareOpen(!fareOpen);
                  }}
                >
                  <span className="pt-1">Cancellation and Reschedule policy</span>
                  <i
                    style={{ color: "grey", fontSize: 28, textAlign: "right" }}
                    className={cx(
                      "fas cursor-pointer text-j-gray-light text-4xl",
                      {
                        "fa-angle-down": !fareOpen,
                        "fa-angle-up": fareOpen,
                      }
                    )}
                  ></i>
                </div>
                {fareOpen && (
                  <div style={{padding: "1rem"}}>
                    <CancelView>
                      <h3 style={{ color: "#500E4B", fontWeight: "600" }}>
                        <u>
                          Rescheduling Fee for sailings May, Jun &amp; July 31,
                          2021
                        </u>
                      </h3>
                      <table style={{color: "black", borderCollapse: "collapse", width: "100%"}}>
                        <tr>
                          <th style={{border: "1px solid #dddddd", textAlign: "left", padding: "8px",}}>Days to Departure</th>
                          <th style={{border: "1px solid #dddddd", textAlign: "left", padding: "8px",}}>Rescheduing Fee</th>
                        </tr>
                        <tr>
                          <td style={{border: "1px solid #dddddd", textAlign: "left", padding: "8px",}}>More than 8 days</td>
                          <td style={{border: "1px solid #dddddd", textAlign: "left", padding: "8px",}}>NIL Fee + Fare Difference</td>
                        </tr>
                        <tr>
                          <td style={{border: "1px solid #dddddd", textAlign: "left", padding: "8px",}}> 0 to 7 days</td>
                          <td style={{border: "1px solid #dddddd", textAlign: "left", padding: "8px",}}>INR 5000 per cabin + Fare difference</td>
                        </tr>
                      </table>
                      <br></br>
                      <ul style={{color: "black", listStyle: "disc", marginLeft: "1rem",}}>
                        <li>
                          Free rescheduling is limited to one time<sup>*</sup>
                        </li>
                        <li>
                          Rescheduled itinerary must commence on or before 31 st
                          March 2022
                        </li>
                      </ul>
                      <br></br>
                    </CancelView>
                    <CancelView title="" theme="blue">
                      <h3 style={{ color: "#500E4B", fontWeight: "600" }}>
                        <u>
                          Rescheduling Fee for sailings from 1st August 2021
                          onwards
                        </u>
                      </h3>
                      <table style={{color: "black", borderCollapse: "collapse", width: "100%"}}>
                        <tr>
                          <th style={{border: "1px solid #dddddd", textAlign: "left", padding: "8px",}}>Days to sailing date</th>
                          <th style={{border: "1px solid #dddddd", textAlign: "left", padding: "8px",}}>Rescheduing Fee</th>
                        </tr>
                        <tr>
                          <td style={{border: "1px solid #dddddd", textAlign: "left", padding: "8px",}}>More than 31 days</td>
                          <td style={{border: "1px solid #dddddd", textAlign: "left", padding: "8px",}}>NIL Fee + Fare difference</td>
                        </tr>
                        <tr>
                          <td style={{border: "1px solid #dddddd", textAlign: "left", padding: "8px",}}>0 to 30 days</td>
                          <td style={{border: "1px solid #dddddd", textAlign: "left", padding: "8px",}}>INR 5000 per cabin + Fare difference</td>
                        </tr>
                      </table>
                      <br></br>
                      <ul style={{color: "black", listStyle: "disc", marginLeft: "1rem",}}>
                        <li>
                          Free rescheduling is limited to one time<sup>*</sup>
                        </li>
                      </ul>
                      <br></br>
                    </CancelView>
                    <CancelView title="">
                      <h3 style={{ color: "#500E4B", fontWeight: "600" }}>
                        CANCELLATION POLICY
                      </h3>
                      <table style={{color: "black", borderCollapse: "collapse", width: "100%"}}>
                        <tr>
                          <th style={{border: "1px solid #dddddd", textAlign: "left", padding: "8px",}}>Days to departure</th>
                          <th style={{border: "1px solid #dddddd", textAlign: "left", padding: "8px",}}>Cancellation Fees</th>
                        </tr>
                        <tr>
                          <td style={{border: "1px solid #dddddd", textAlign: "left", padding: "8px",}}>More than 31 days</td>
                          <td style={{border: "1px solid #dddddd", textAlign: "left", padding: "8px",}}>25% of cabin fare</td>
                        </tr>
                        <tr>
                          <td style={{border: "1px solid #dddddd", textAlign: "left", padding: "8px",}}>0 to 30 days</td>
                          <td style={{border: "1px solid #dddddd", textAlign: "left", padding: "8px",}}>50% of cabin fare</td>
                        </tr>
                        <tr>
                          <td style={{border: "1px solid #dddddd", textAlign: "left", padding: "8px",}}>For No-Show</td>
                          <td style={{border: "1px solid #dddddd", textAlign: "left", padding: "8px",}}>100% of cabin fare</td>
                        </tr>
                      </table>
                      <br></br>
                      <ul style={{color: "black", listStyle: "disc", marginLeft: "1rem",}}>
                        <li>
                          Cancellation fee will be applicable only on cabin
                          fare,
                        </li>
                        <li>
                          Port Charges &amp; gratuity will be refunded
                          completely.
                        </li>
                        <li>
                          GST for the refunded amount will also be returned.
                        </li>
                        <li>
                          GST will be applicable on rescheduling fee + Fare
                          difference.
                        </li>
                        <li>
                          GST will be applicable for international sailing on
                          rescheduling fee + Fare difference.
                        </li>
                      </ul>
                    </CancelView>
                  </div>
                )}
              </div>
          </Tile>
          </span>
        </div>
        </>
    )
}

const OrderDetail = ({ title, children, big }) => (
    <div className="mb-6">
      <h1 className="uppercase pb-0 text-j-gray text-tiny leading-none ">
        {title}
      </h1>
      <h4
        className={
          big ? "text-j-magenta text-3xl font-bold pt-4" : "text-j-magenta pt-2 text-lg font-semibold"
        }
      >
        {children}
      </h4>
    </div>
  );

  const PaymentDetails = ({ content, details, big }) => (
    <>
      <div className="grid grid-cols-1 grid-cols-2" style={{marginBottom:'7px'}}>
        { big && <>
          <div style={{color:"black", fontSize: 16, fontWeight:'bold'}} className="float-left bold">
            {content}
          </div>
          <div style={{color:"black", fontSize: 16, textAlign:'right', fontWeight:'bold'}} className="float-right font-weight-bold text-capitalize">
            &#x20B9;{details}
          </div>
          </>
        }
        { !big && <>
            <div style={{color:"grey", fontSize: 14}} className="float-left bold">{content}</div>
            <div style={{color:"grey", fontSize: 14, textAlign:'right'}} className="float-right font-weight-bold text-capitalize">
                &#x20B9;{details}
            </div>
          </>
        }
      </div>
    </>
  );
import React, { useContext, useState } from "react";
import UserContext from "../../store/UserContext";
import cx from "classnames";
import moment from "moment";
import Button from "../Button/Button";
import DayWiseOverlay from "../Booking/Selector/DayWiseOverlay";
import styles from "./RouteCard.module.css";
import { useHistory } from "react-router-dom";
// NOTE: The route should already have a list of itineraries included!
export const RouteCard = ({ route, light, nightCount }) => {
  const [user, setUser] = useContext(UserContext);
  const [open, setOpen] = useState(false);
  const [openModal, setOpenModal] = useState(false);
  const [openItiModal, setOpenItiModal] = useState(false);
  const [showMore, setShowMore] = useState(false);
  const [selectedMonth, setSelectedMonth] = useState("");
  let history = useHistory();

  const clickShow = () => {
    setShowMore((oldValue) => !oldValue);
  };

  const goToBooking = (route, itinerary, nightCount) => {
    setUser(
      Object.assign({}, user, {
        name: null,
        phone: null,
        booking: Object.assign({}, user.booking, {
          itinerary,
          route: null,
          nightCount: null,
          roomsSelected: false,
          guestsFilled: false,
          giftCard: null,
          jurisdiction: null,
          packages: null,
          hotels: null,
          returnHotels: null,
          returnFlight: null,
          flight: null,
          visitorID: 123456,
          rooms: null,
          departure: itinerary.ports[0].port.name,
          destination: itinerary.ports[itinerary.ports.length - 1].port.name,
          departure_time: `${new Date(itinerary.startTime)}`,
          destination_time: `${new Date(itinerary.startTime)}`,
        }),
      })
    );
    history.push("/admin/booking");
  };

  route.itineraries.sort(
    (a, b) => new Date(a.startTime) - new Date(b.startTime)
  );

  let groupMonthly = route.itineraries.reduce((result, ite) => {
    let dateObj = new Date(ite.startTime);
    let monthyear = dateObj.toLocaleString("en-us", {
      month: "long",
      year: "numeric",
    });
    if (!result[monthyear]) result[monthyear] = [ite];
    else result[monthyear].push(ite);
    return result;
  }, {});

  const handleDateClick = (month) => {
    setSelectedMonth(month);
  };

  return (
    <div className={cx(open ? "bg-magenta text-j-white mb-12 rounded-lg p-5" : "bg-white mb-12 rounded-lg p-5")}>
      <div className="px-5 pb-0 pt-8">
        {route.ports.map((p) => (
          <h4
            className={cx(
              "flex uppercase leading-tiny",
              open ? "text-white" : "text-j-magenta"
            )}
            key={p.id}
          >
            <div
              className={cx("text-center w-12", !open && "text-j-magenta")}
            >
              <i className="fas fa-anchor"></i>
            </div>
            <span>{p.port.name}</span>
          </h4>
        ))}
        <br></br>
      </div>
      {open ? (
        <>
          <h4 className="text-lg text-white" id={route.id}>
            AVAILABLE CRUISE DATES
          </h4>
          <br></br>
          {Object.keys(groupMonthly).map((month, index) => (
            <div style={{ textAlign: "center" }}>
              <h5 className="text-lg mb-3">
                <button
                  className="h-10 pt-1 items-center rounded transition duration-300 ease-in-out px-8 justify-center font-bold uppercase text-sm bg-j-orange text-white"
                  style={{ width: "64%" }}
                  onClick={() => handleDateClick(month)}
                >
                  {month}
                </button>
              </h5>
              {selectedMonth === month || (index === 0 && !selectedMonth)
                ? groupMonthly[
                    selectedMonth || Object.keys(groupMonthly)[0]
                  ].map((itinerary) => (
                    <div
                      className="border rounded-lg border-j-gray-lighter p-5 bg-white mb-10"
                      key={itinerary.id}
                    >
                      <div className="">
                        <div className="">
                          <small className="uppercase text-sm text-j-gray-lighter">
                            Cruise starts
                          </small>
                          <span className="text-lg uppercase block text-lg text-j-black">
                            {moment(itinerary.startTime)
                              .locale("en")
                              .format("ddd, D MMM YYYY")}
                          </span>
                        </div>
                        <br />
                        <div className="">
                          <small className="uppercase text-sm text-j-gray-lighter">
                            Cruise ends
                          </small>
                          <span className="text-lg uppercase block text-lg text-j-black">
                            {moment(itinerary.endTime)
                              .locale("en")
                              .format("ddd, D MMM YYYY")}
                          </span>
                        </div>
                      </div>
                      {openItiModal && (
                        <div style={{ zIndex: 999999 }}>
                          <div
                            style={{
                              position: "fixed",
                              backgroundColor: "rgba(0,0,0, 0.2)",
                              zIndex: 999,
                              top: "0px",
                              left: "0px",
                              right: "0px",
                              bottom: "0px",
                            }}
                            onClick={() => setOpenItiModal(false)}
                          ></div>
                          <DayWiseOverlay
                            itineraryId={itinerary.id}
                            style="close"
                            onCancel={() => setOpenItiModal(false)}
                          ></DayWiseOverlay>
                        </div>
                      )}
                      <br />
                      <Button
                        className="bg-j-orange w-full text-white"
                        onClick={() =>
                          goToBooking(route, itinerary, nightCount)
                        }
                      >
                        Get price
                      </Button>
                      <br />
                      <Button
                        className="bg-j-orange w-full text-white"
                        onClick={() => {
                          setOpenItiModal(true);
                        }}
                      >
                        More Info
                      </Button>
                      <br />
                      <Button
                        className="text-j-orange w-full mb-4 border border-j-orange cr-bttn"
                        onClick={() => {
                          setOpen(false);
                        }}
                      >
                        Hide cruise dates
                      </Button>
                    </div>
                  ))
                : null}
            </div>
          ))}
        </>
      ) : ((
          <Button
            className="bg-white text-j-orange w-full mb-4 border border-j-orange"
            onClick={() => {
              setOpen(true);
            }}
          >
            See cruise dates
          </Button>
        )
      )}
    </div>
  );
};

export default RouteCard;

import React, { Component, useRef, useMemo, useState, useEffect } from "react";
import moment from "moment";
import Button from '@material-ui/core/Button';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';
import Dialog from '@material-ui/core/Dialog';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useTheme, makeStyles } from '@material-ui/core/styles';
import Avatar from '@material-ui/core/Avatar';
import { getToken } from "../../utils/common";

const useStyles = makeStyles((theme) => ({
    orange: {
        color: 'white',
        backgroundColor: '#EA725B',
        marginLeft:'8%',
        marginTop:'8%'
    }
}))

export default function NoteModal({notesOpen, handleNotesClose, itineryid}) {

    const theme = useTheme();
    const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));
    const classes = useStyles();
    const [itiID, setItID] = useState();
    const [notes, setNotes] = useState();
    const [comments, setComments] = useState([])

    useEffect(()=> {
        commentsAPI()
    },[])

    const commentsAPI = () => {
        fetch(process.env.REACT_APP_API_ENDPOINT+'/staff_portal/enquiries/'+itineryid+'/comments', {
            method: 'GET',
            headers: {
              Accept: "application/json",
              "Content-Type": "application/json",
              Authorization:
              `Bearer ${getToken()}`,
            },
          })
          .then((response) => { 
              if(response.ok){
                 return response.json()
              }
              throw Error("Verification failed");
             }
            )
          .then((response) => {
            setComments(response.comments)
            console.log(response.comments)
          })
    }

    const onChangeNote = (e) => {
        setNotes(e.target.value)
    }

    const handleNotes = () => {
        fetch(process.env.REACT_APP_API_ENDPOINT+'/staff_portal/enquiries/'+itineryid+'/comment', {
            method: 'post',
            headers: {
              Accept: "application/json",
              "Content-Type": "application/json",
              Authorization:
              `Bearer ${getToken()}`,
            },
            body: JSON.stringify({
                note: notes
            })
          })
          .then((response) => { 
              if(response.ok){
                 return response.json()
              }
              throw Error("Verification failed");
             }
            )
          .then((response) => {
            commentsAPI()
          })
    }

    return(
        <>
            <Dialog
                disableBackdropClick
                disableEscapeKeyDown
                fullScreen={fullScreen}
                width="xl"
                aria-labelledby="responsive-dialog-title"
                open={notesOpen}
                onClose={handleNotesClose}
            >
                <DialogTitle id="responsive-dialog-title">Previous Note history!</DialogTitle>
                <DialogContent>
                    <div className="grid mt-4  gap-8 grid-cols-1">
                        <div className="flex flex-col ">
                            <div className="bg-white p-5">
                                <div className="flex flex-col sm:flex-row items-center">
                                    <div className="w-full sm:w-auto sm:ml-auto mt-3 sm:mt-0"></div>
                                    <div class="mt-2">
				                        <div class="form">
                                            <div class="md:space-y-2 mb-3">
                                                {comments.map((data) => {
                                                    console.log(data)
                                                     return (<div class="flex items-center py-2">
                                                        <div class="w-12 h-12 mr-4 mt-4 flex-none rounded-full border overflow-hidden">
                                                            <Avatar className={classes.orange}>{(data.owner).charAt(0)}</Avatar>
                                                        </div>
                                                        <div>
                                                            <p className="text-xs text-gray-600 pb-3 pl-2">{data.owner}, {moment(data.created_at).format("DD-MMMM-YYYY, HH:mm")}</p>
                                                            <label class="cursor-pointer bg-magenta  hover:shadow-lg rounded-full p-2">
                                                                <span class="focus:outline-none text-white text-sm py-2 px-4 rounded-full ">{data.note}</span>
                                                                <input type="file" class="hidden" />
                                                            </label>
                                                        </div>
                                                 </div>)
                                                })}
						                    </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </DialogContent>
                <DialogActions>
                    <div className="w-full pl-2 pr-2">
                        <div class="flex-auto w-full mb-1 text-xs space-y-2">
                            <label class="font-semibold text-gray-600 py-2">Notes</label>
                            <textarea required="" name="message" id="" className="w-full min-[100px] max-h-[150px] h-20 appearance-none block w-full bg-grey-lighter text-grey-darker border border-grey-lighter rounded-lg  py-4 px-4 " placeholder="Enter your notes" spellCheck="false" onChange={(e)=>onChangeNote(e)}></textarea>
                        </div>
                        <div class="mt-5 text-right md:space-x-3 md:block flex flex-col-reverse">
                            <button class="mb-2 md:mb-0 bg-white px-5 py-2 text-sm shadow-sm font-medium tracking-wider border text-gray-600 rounded-full hover:shadow-lg hover:bg-gray-100" onClick={handleNotesClose}> Cancel </button>
                            <button class="mb-2 md:mb-0 bg-magenta px-5 py-2 text-sm shadow-sm font-medium tracking-wider text-white rounded-full hover:shadow-lg hover:bg-j-orange" onClick={handleNotes}>Save</button>
                        </div>
                    </div>
                </DialogActions>
            </Dialog>
        </>
    )
}
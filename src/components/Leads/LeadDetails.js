import React, { Component, useRef, useMemo, useState, useEffect } from "react";
import ReactTableComponent from "../Table/ReactTablecomponent";
import {Link, useLocation, useHistory} from "react-router-dom";
import { getToken } from "../../utils/common";
import moment from "moment";
import NoteModal from "./NoteModal";



export default function LeadDetails({handlePageChange}) {

    let location = useLocation();
    const [tableData, setDataTable] = useState([])
    const [notesOpen, setnotesOpen] = useState(false);
    const [itineryid, setItineryID] = useState();
    const [totalPages, setTotalPages] = useState(0);
    const [loading, setLoading] = useState(false);
    let history = useHistory();

    useEffect(() => {
        customerEnquiryDetails();
    },[])

    const customerEnquiryDetails = () => {
        setLoading(true);
        fetch(process.env.REACT_APP_API_ENDPOINT+'/staff_portal/customer_enquiries?id='+location.state.id, {
            method: 'GET',
            headers: {
              Accept: "application/json",
              "Content-Type": "application/json",
              Authorization:
              `Bearer ${getToken()}`,
            },
        }).then((response) => { 
            if(response.ok){
                return response.json()
            }
            throw Error("Verification failed");
            }
        ).then((response) => {
            var dummySetdata = [];
            var responseData= response.enquiries;
            responseData.map((data) => {
             dummySetdata.push(
               {
                "number": data.number,
                "date": moment(data.callback_time).format("DD-MM-YYYY"),
                "name": data.name,
                "department": data.department_id,
                "id": data.id,
                "email": data.email,
                "contact_phone": data.phone_number,
                "priority": data.priority,
                "quotations": data.quotations[0],
                "details":data.number,
                "last_commented_user": data.last_commented_user
               })
            })
            setTotalPages(response.pagination.total_pages);
            setDataTable(dummySetdata);
            setLoading(false)
        })
    }

    const columns = React.useMemo(
        () => [
        {
            Header: () => null,
            id: 'expander',
            Cell: ({ row }) => (
                <span {...row.getToggleRowExpandedProps()}>
                    {row.isExpanded ?  <i className="fas fa-chevron-circle-up text-j-magenta"></i> : <i className="fas fa-chevron-circle-down text-j-magenta"></i>}
                </span>
            ),
        },
        {
            Header: 'Inquiry Number',
            accessor: 'number',
            maxWidth: 150,
            Cell: (props) => {
              return <p style={{width:'80px'}}>{props.value}</p>
            }
        },
        {
            Header: 'Date',
            accessor: 'date',
            Cell: (props) => {
              return <p style={{width:'70px'}}>{props.value}</p>
            }
        },
        {
            Header: 'Name',
            accessor: 'name',
        },
        {
            Header: 'Contact Number',
            accessor: 'contact_phone',
        },
        {
            Header: 'Notes',
            accessor: 'id', 
            width: 60,
            Cell: (props) => {
                console.log(props)
              return (
                <div>
                    <p className="text-sm pt-2 underline text-j-magenta cursor-pointer" onClick={() => handleOpenNotes(props.value)}>Add Notes</p>
                    {props.data[props.row.index].last_commented_user !=null && <p><span>(Last commented: </span>{props.data[props.row.index].last_commented_user})</p>}
                </div>
              )
            }
        },
        {
            Header: "",
            accessor: "details",
            Cell: (props) => {
               
              return (
                <div>
                  <button className="bg-j-orange hover:bg-magenta text-white p-2 rounded-xl text-xs" onClick={()=>{
                      handleDetails(props.value)
                  }}>Booking<span className="pl-1"><i className="fas fa-arrow-circle-right"></i></span></button>
                </div>
              );
            },
        },
    ],
    []
    )

    const handleNotesClose = () => {
        setnotesOpen(false)
    }


    const handleOpenNotes = (id) => {
        setItineryID(id)
        setnotesOpen(true)
    }

    const handleDetails = (number) => {
        //history.push('/admin/lead-details')
        history.push({
            pathname: '/admin/lead-search',
            state: { number: number }
        })
    }

    const renderRowSubComponent = ({ i }) => {
        return (
            <>
                <div
                    style={{
                    fontSize: '14px',
                    }}
                >
                    {/* <code>{JSON.stringify({ values: tableData[i] }, null, 2)}</code> */}
                    <div className="grid grid-cols-3 gap-4 pl-20 pt-4 pr-14 pb-4">
                        <p className="text-grey-700 border-b pb-4 "><span className="font-bold">Email:</span> <span className="pl-2">{tableData[i].email}</span></p>
                        <p className="text-grey-700 border-b pb-4 "><span className="font-bold">Department:</span> <span className="pl-2 uppercase">{tableData[i].department}</span></p>
                        <p className="text-grey-700 border-b pb-4 "><span className="font-bold">Priority:</span> <span className="pl-2 uppercase">{tableData[i].priority}</span></p>
                        <p className="text-grey-700 border-b pb-4 "><span className="font-bold">Itinerary:</span> <span className="pl-2 uppercase">{tableData[i].quotations.itinerary}</span></p>
                        <p className="text-grey-700 border-b pb-4 "><span className="font-bold">Nights:</span> <span className="pl-2 uppercase">{tableData[i].quotations.nights}</span></p>
                        <p className="text-grey-700 border-b pb-4 "><span className="font-bold">Guests:</span> <span className="pl-2 uppercase">{tableData[i].quotations.guests}</span></p>
                        <p className="text-grey-700 border-b pb-4 "><span className="font-bold">Cabins:</span> <span className="pl-2 uppercase">{tableData[i].quotations.cabins}</span></p>
                        {/* {tableData[i].last_commented_user != null && <p className="text-grey-700 border-b pb-4 "><span className="font-bold">Last commented:</span><span className="pl-2 uppercase"> {tableData[i].last_commented_user}</span></p>} */}
                    </div>
                </div>
            </>
        )
    }
    


    const data= tableData;
    return (
        <>
            <div className="ml-8 mr-8">
                <div className="overflow-x-auto">
                    <div className="min-w-screen bg-gray-100 flex items-center justify-center bg-gray-100 font-sans overflow-hidden">
                        <ReactTableComponent 
                            columns={columns}
                            data={data}
                            loading={loading}
                            renderRowSubComponent={renderRowSubComponent}
                            count={totalPages}
                            page={1}
                            handlePageChange={handlePageChange}
                        />
                        {notesOpen && <NoteModal
                            notesOpen={notesOpen} 
                            handleNotesClose={handleNotesClose}
                            itineryid={itineryid}
                        />}
                    </div>
                </div>
            </div>
        </>
    )
}
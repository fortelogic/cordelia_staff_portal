import React, { Component, useRef, useMemo, useState, useEffect } from "react";
import { useTable, useExpanded } from "react-table";
import { getToken } from "../../utils/common";
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import { makeStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import moment from "moment";
import NoteModal from "./NoteModal"
import { useHistory } from "react-router-dom";
import ReactTableComponent from "../Table/ReactTablecomponent";

function TabPanel(props) {
    const { children, value, index, ...other } = props;
  
    return (
      <div
        role="tabpanel"
        hidden={value !== index}
        id={`vertical-tabpanel-${index}`}
        aria-labelledby={`vertical-tab-${index}`}
        {...other}
      >
        {value === index && (
          <Box p={3}>
            <Typography>{children}</Typography>
          </Box>
        )}
      </div>
    );
  }
  
  TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
  };
  
  function a11yProps(index) {
    return {
      id: `vertical-tab-${index}`,
      'aria-controls': `vertical-tabpanel-${index}`,
    };
  }
  
  const useStyles = makeStyles((theme) => ({
    root: {
      backgroundColor: theme.palette.background.paper,
      display: 'flex',
    },
    tabs: {
      borderRight: `1px solid ${theme.palette.divider}`,
    },
    tabPanel: {
      width:'80%'
    },
    textField: {
      width: '90%'
    },
    tab: {
       
    },
    ul: {
        "& .MuiPaginationItem-root": {
          backgroundColor: "#500E4B",
          color:'#fff'
        }
    }
  }));

export default function Leads({}) {
    const [paginationstatus, setPaginationStatus] = useState(false);
    const [dataTable, setDataTable] = useState([]);
    const [loading, setLoading] = useState(true);
    const [count, setCount] = useState(1);
    const [page, setPage] = useState(1);
    const [totalPages, setTotalPages] = useState(0)
    const classes = useStyles();
    const [value, setValue] = useState(0);
    const [notesOpen,setnotesOpen] = useState(false);
    const [mystaffID, setMyStaffID] = useState()
    const [staffList, setStaffList] = useState([])
    const [searchValue, setSearchValue] = useState()
    const [itineryid, setItineryID] = useState()
    let history = useHistory();

    useEffect(() => {
        ownersList()
    }, [paginationstatus]);

    const ownersList = () => {
        fetch(process.env.REACT_APP_API_ENDPOINT+'/staff_portal/staff', {
            method: 'GET',
            headers: {
              Accept: "application/json",
              "Content-Type": "application/json",
              Authorization:
              `Bearer ${getToken()}`,
            },
        }).then((response) => { 
            if(response.ok){
                return response.json()
            }
            throw Error("Verification failed");
            }
        ).then((response) => {
            console.log(response)
            setMyStaffID(response.current_staff_id)
            setStaffList(response.staff)
            enquiryListApi(response.current_staff_id)

        })
        // bookingListApi(count)
    }
    const handleChange = (event, newValue) => {
        setValue(newValue);
    };

    const enquiryListApi = (count, search) => {
        if(search == undefined) {
            var searchValue = '';
        } else {
            var searchValue = search;
        }
        if(count == "all") {
            var url = 'enquiries?';
        } else {
            var url = 'enquiries?staff_id='+count
        }
        fetch(process.env.REACT_APP_API_ENDPOINT+'/staff_portal/'+url+'&search='+searchValue, {
            method: 'GET',
            headers: {
              Accept: "application/json",
              "Content-Type": "application/json",
              Authorization:
              `Bearer ${getToken()}`,
            },
        }).then((response) => { 
            if(response.ok){
                return response.json()
            }
            throw Error("Verification failed");
            }
        ).then((response) => {
            var dummySetdata = [];
            console.log(response);
            var responseData= response.enquiries; 
            responseData.map((data) => {
            dummySetdata.push(
                {
                  "id": data.id,
                  "name": data.name,
                  "email": data.email,
                  "contact_phone": data.phone_number,
                  "total_quotations": data.enquiry_count,
                  "details": data.enquiries,
                }
              )
            })
            setTotalPages(1);
            setDataTable(dummySetdata);
            setLoading(false);
            // document.getElementsByClassName('scroll-to-top')[0].scrollTop = 0;
        }).catch((err) =>  console.log(err));
    }

    const columns = React.useMemo(
        () => [
        {
            Header: () => null,
            id: 'expander',
            Cell: ({ row }) => (
                <span {...row.getToggleRowExpandedProps()}>
                    {row.isExpanded ?  <i className="fas fa-chevron-circle-up text-j-magenta"></i> : <i className="fas fa-chevron-circle-down text-j-magenta"></i>}
                </span>
            ),
        },
        {
            Header: 'Name',
            accessor: 'name',
            maxWidth: 150,
            Cell: (props) => {
              return <p style={{width:'120px'}}>{props.value}</p>
            }
        },
        {
            Header: 'Email ID',
            accessor: 'email',
            Cell: (props) => {
              return <p style={{width:'150px', wordBreak:'break-word'}}>{props.value}</p>
            }
        },
        {
            Header: 'Contact Number',
            accessor: 'contact_phone',
        },
        {
            Header: 'Total Quotation',
            accessor: 'total_quotations', 
            width: 60,
            Cell: (props) => {
              return (<>
                  <div className="flex justify-center">{props.value}</div>
              </>)
            },
        },
        {
            Header: "",
            accessor: "id",
            Cell: (props) => {
              return (
                <div>
                  <button className="bg-j-orange hover:bg-magenta text-white p-2 rounded-xl text-xs" onClick={()=>{
                      handleDetails(props.value)
                  }}>Details<span className="pl-1"><i className="fas fa-arrow-circle-right"></i></span></button>
                </div>
              );
            },
        },
    ],
    []
    )

    const handleOpenNotes = (id) => {
        setItineryID(id)
        setnotesOpen(true)
    }

    const handleNotesClose = () => {
        setnotesOpen(false)
    }

    const handleDetails = (id) => {
        // history.push('/admin/lead-details')
        history.push({
          pathname: '/admin/lead-details',
          state: { id: id }
        })
    }

    const renderRowSubComponent = ({ i }) => {
        console.log(i)
        return(
            <>
                <div className={classes.root}>
                    <Tabs
                        orientation="vertical"
                        variant="scrollable"
                        value={value}
                        onChange={handleChange}
                        aria-label="Vertical tabs example"
                        className={classes.tabs}
                    >
                    {dataTable[i].details.map((value,i) => {
                        return(<Tab label={value.number} {...a11yProps(i)}  className={classes.tab}/>)
                    })}
                    </Tabs>
                    {dataTable[i].details.map((data,i) => {
                        return(
                            <TabPanel value={value} index={i} className={classes.tabPanel}>
                                <div className="grid grid-cols-4 gap-2 pb-0 w-full">
                                    <div className="border-black text-center mb-2 border-b border-gray-400 pb-2">
                                        <p className="text-sm">Inqury No</p>
                                        <p className="pt-1 text-gray-600 text-sm">{data.number}</p>
                                    </div>
                                    <div className="border-black text-center mb-2 border-b border-gray-400 pb-2">
                                        <p className="text-sm">Department</p>
                                        <p className="pt-1 text-gray-600 text-sm">{data.department_id}</p>
                                    </div>
                                    <div className="border-black text-center mb-2 border-b border-gray-400 pb-2 w-full">
                                        <p className="text-sm">Priority</p>
                                        <p className="pt-1 text-gray-600 text-sm">{data.priority}</p>
                                    </div>
                                    <div className="border-black text-center mb-2 border-b border-gray-400 pb-2">
                                        <p className="text-sm">Itinerary Details</p>
                                        <p className="pt-1 text-gray-600 text-sm">{data.quotations[0].itinerary}</p>
                                    </div>
                                </div>
                                <div className="grid grid-cols-4 gap-2 pb-0">
                                    <div className="border-black text-center mb-2 border-b border-gray-400 pb-4">
                                        <p className="text-sm">Nights</p>
                                        <p className="pt-1 text-gray-600 text-sm">{data.quotations[0].nights}</p>
                                    </div>
                                    <div className="border-black text-center mb-2 border-b border-gray-400 pb-4">
                                        <p className="text-sm">Guset Count</p>
                                        <p className="pt-1 text-gray-600 text-sm">{data.quotations[0].guests}</p>
                                    </div>
                                    <div className="border-black text-center mb-2 border-b border-gray-400 pb-4">
                                        <p className="text-sm">Cabins</p>
                                        <p className="pt-1 text-gray-600 text-sm">{data.quotations[0].cabins}</p>
                                    </div>
                                    <div className="border-black text-center mb-2 border-b border-gray-400 pb-4">
                                        <p className="text-sm pt-2 underline text-j-magenta cursor-pointer" onClick={() => handleOpenNotes(data.id)}>Add Notes</p>
                                        {data.last_commented_user != null && <p className="text-sm">(Last commented: {data.last_commented_user})</p>}
                                    </div>
                                </div>
                            </TabPanel>)
                         })}
                        {notesOpen && <NoteModal
                            notesOpen={notesOpen} 
                            handleNotesClose={handleNotesClose}
                            itineryid={itineryid}
                        />}
                        
                </div>
            </>
        )
    }

    const handlePageChange = (event, value) => {
        console.log(value)
    };

    const onChangeStaff = (e) => {
        console.log(e.target.value)
        setMyStaffID(e.target.value)
        enquiryListApi(e.target.value,'');
    }

    const onChangeSearch = (e) => {
        setSearchValue(e.target.value)
    }

    const handleSearch = () => {
        enquiryListApi(mystaffID,searchValue);
    }

    let data = dataTable;
    const tableInstance = useTable({ columns, data },  useExpanded)
    const {
        getTableProps,
        getTableBodyProps,
        headerGroups,
        rows,
        prepareRow,
        visibleColumns,
        state: { expanded },
      } = tableInstance

    return(
        <>
            <div className="pl-8 pt-2 pb-2">
                <span><i className="fas fa-project-diagram text-gray-500 pr-2"></i></span> <span className="text-md font-medium text-gray-500">Leads</span>
            </div>
            <div className="grid grid-cols-2 mr-4">
                <div className="pl-8 pt-4 grid grid-cols-2 gap-4">
                    <div className="relative inline-flex">
                    <svg class="w-2 h-2 absolute top-0 right-0 m-4 pointer-events-none" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 412 232"><path d="M206 171.144L42.678 7.822c-9.763-9.763-25.592-9.763-35.355 0-9.763 9.764-9.763 25.592 0 35.355l181 181c4.88 4.882 11.279 7.323 17.677 7.323s12.796-2.441 17.678-7.322l181-181c9.763-9.764 9.763-25.592 0-35.355-9.763-9.763-25.592-9.763-35.355 0L206 171.144z" fill="#648299" fill-rule="nonzero"/></svg>
                        <select class="border w-full border-gray-300 rounded-full text-gray-600 h-10 pl-5 pr-10 bg-white hover:border-gray-400 focus:outline-none shadow-lg appearance-none"
                        onChange={(e)=>onChangeStaff(e)}>
                            <option>Filter By Staff</option>
                            <option value="all">All</option>
                            {staffList.map((data) => (
                                <option value={data.id}>{data.name}</option>
                            ))}
                        </select>
                    </div>
                </div>
                <div className="bg-white flex items-center rounded-full shadow-lg m-4 border h-10">
                    <input className="rounded-l-full w-full pl-6 text-gray-700 focus:outline-none" id="search" type="text" placeholder="Search" onChange={(e) => onChangeSearch(e)}/>
                    <div className="p-4">
                        <button className="bg-magenta hover:bg-j-orange text-white rounded-full p-2 focus:outline-none w-6 h-6 flex items-center justify-center" onClick={()=>handleSearch()}>
                            <i className="fas fa-search"></i>
                        </button>
                    </div>
                </div>
            </div>
            <div className="ml-8 mr-8">
                <div className="overflow-x-auto">
                    <div className="min-w-screen bg-gray-100 flex items-center justify-center bg-gray-100 font-sans overflow-hidden">
                        <ReactTableComponent 
                            columns={columns}
                            data={data}
                            loading={loading}
                            renderRowSubComponent={renderRowSubComponent}
                            count={count}
                            page={page}
                            handlePageChange={handlePageChange}
                        />
                    </div>
                </div>
            </div>
        </>
    )
}
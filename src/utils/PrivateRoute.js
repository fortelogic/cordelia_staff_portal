import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { getToken } from './common';

// handle the private routes
function PrivateRoute({ children, ...rest }) {
  return (
    <Route
      {...rest}
      render={(props) => getToken() ? children : <Redirect to={{ pathname: '/login', state: { from: props.location } }} />}
    />
  )
}

export default PrivateRoute;
export const CDN = (path) =>
  path
    ? `https://images-staging.jalesh.com ${
        path.startsWith("/") ? "" : "/"
      }${path}`
     : ""
    // : `https://placeimg.com/640/480/any?time=${Math.random()}`;

export default CDN;

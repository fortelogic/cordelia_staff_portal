import React, { Component, useContext } from "react";
import Tile from "../../components/Tile/Tile";

class Failed extends Component {
    constructor(props) {
      super(props);
      this.state = {};
    }
  
    render() {
      if (typeof window !== "undefined") {
        window.location.href = `${window.location.origin}/admin/booking?fail=1`;
      }
      return (
        <>
            <div className="text-white px-6 py-4 border-0 rounded relative mb-4 bg-magenta m-10 h-full">
                <span className="inline-block align-middle mr-8">
                    <b className="capitalize">Booking</b> Failed
                    <h4 className="pb-5">
                    <i className="fas fa-times-circle text-j-green text-3xl pr-2 pt-2" />
                    Please Try Again.
                    </h4>
                </span>
            </div>
        </>
      )
    }
}

export default Failed;
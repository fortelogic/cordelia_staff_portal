import React, { Component, useRef, useMemo, useState, useEffect } from "react";
import Button from '@material-ui/core/Button';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';
import Dialog from '@material-ui/core/Dialog';
import RadioGroup from '@material-ui/core/RadioGroup';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useTheme } from '@material-ui/core/styles';
import moment from "moment";
import { getToken } from "../../../utils/common";
import Alert from '@material-ui/lab/Alert';
import DateRangePicker from "react-daterange-picker";
import "react-daterange-picker/dist/css/react-calendar.css";
import { extendMoment } from "moment-range";
import ReactLoading from 'react-loading';
import cx from "classnames"
import styles from '../sharelead.module.css'

export default function ProductDetails({
    handleDateOfTravel,
    selectNight,
    handleNights,
    onSelect,
    DateRangeApiCall,
    days,
    nightLoad,
    showNights,
    nights,
    showItinerary,
    itineraries,
    getPriceDetails,
    handleSelectDate,
    showGuest,
    itineraryindex,
    selectedDate,
    selectGuestAdult,
    selectGuestChild,
    selectGuestInfant,
    limitstatus,
    adultCount,
    childCount,
    infantCount,
    checkAvailability,
    cabins,
    cabinsLoad,
    showCabin,
    selectCabins,
    openDetailedItinery,
    itinerariesDetails,
    open,
    handleClose,
    handleOk,
    todayVal,
    initialDay,
    departureDate,
    routeSelected,
    handleRoute,
    cabinStatus,
    handleSelectPromoCode,
    promoCode
}) {

    const [totalGuestAllowed, setTotalGuestAllowed] = useState(4);
    const [totalGuest, setTotalGuest] = useState();
    const [adultMax, setAdultMax] = useState()
    const [childMax, setChildMax] = useState()
    const [infantMax, setInfantMax] = useState()
    const [dateOpen, setDateOpen] = useState(false)
    // const [showNight, setshowNight] = useState(false);
    // const [routeSelected, setRouteSelected] = useState();

    // const [showNight, setshowNight] = useState(false);
    // const [showItinerary, setshowItinerary] = useState(false);

    console.log(cabinStatus)
    const theme = useTheme();
    const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));

    useEffect(() => {
        initialFunction()
    },[])

    const initialFunction = () => {

        var adultDate = new Date();;
        var adultMax = adultDate.getFullYear() - 12;
        adultDate.setFullYear(adultMax);
        var dateFormatAdult = moment(adultDate).format("YYYY-MM-DD");
        setAdultMax(dateFormatAdult);

        var childDate = new Date();;
        var childMax = childDate.getFullYear() - 6;
        console.log(childDate)
        childDate.setFullYear(childMax);
        var dateFormatChild = moment(childDate).format("YYYY-MM-DD");
        setChildMax(dateFormatChild);

        var infantDate = new Date();;
        var infantMax = infantDate.getFullYear() - 2;
        infantDate.setFullYear(infantMax);
        var dateFormatInfant = moment(infantDate).format("YYYY-MM-DD");
        setInfantMax(dateFormatInfant);
    }

    const handleCalenderClose = () => {
        DateRangeApiCall()
        setDateOpen(false)
    }

    // const handleRoute = (e) => {
    //     setRouteSelected(e.target.value)
    // }

    return(<>
        <div className="w-8/12 border rounded-lg p-4 shadow-md">
            <div className="flex w-full">
                <div className="flex w-5/12">
                    <label className="text-sm text-gray-700 self-center w-5/12">
                        Date of Travel
                    </label>
                    <span onClick={()=>setDateOpen(true)} className="pt-2 text-sm pl-2 text-red-500 cursor-pointer text-center"><span>Pick date Range</span><br />
                    {!initialDay && <span className="text-gray-600 no-underline">({days.start.format("DD-MM-YYYY")} to {days.end.format("DD-MM-YYYY")})</span>}
                    </span>
                     <Dialog
                        disableBackdropClick
                        disableEscapeKeyDown
                        fullScreen={fullScreen}
                        width="lg"
                        aria-labelledby="responsive-dialog-title"
                        open={dateOpen}
                        onClose={handleCalenderClose}
                    >
                        <DialogTitle id="responsive-dialog-title">Which Date You Want To Travel?</DialogTitle>
                        <DialogContent>
                            <div className="bg-white w-full">
                                <DateRangePicker
                                    value={days}
                                    onSelect={onSelect}
                                    singleDateRange={true}
                                    minimumDate={todayVal}
                                />
                            </div>
                        </DialogContent>
                        <DialogActions>
                            <Button color="primary" onClick={handleCalenderClose}>
                                Ok
                            </Button>
                        </DialogActions>
                    </Dialog>
                </div>
                <div className="flex w-9/12 pl-2">
                    {showNights && <div className="flex pt-4">
                        <p className="text-sm">Nights:</p>
                        {Object.keys(nights).map((key) => (
                            <label className="pl-4">
                                <input 
                                    type="radio" 
                                    value={key} 
                                    checked={selectNight==key}
                                    onChange={(e) => handleNights(e,nights)} 
                                />
                                <span className="pl-1 text-sm">{key}N</span>
                            </label>
                        ))}
                    </div>}
                    {!showNights && <div className="w-full bg-gray-200">
                        {!nightLoad && <p className="text-center text-gray-400 text-sm pt-2 pb-2">Number of Days not available for selected Date</p>}
                        {nightLoad && <div className="opacity-60 flex justify-center">
                            <ReactLoading type={'bubbles'} color={'blue'} />
                        </div>
                        }
                    </div>}
                </div>
            </div>
            <div className="flex mt-4">
                <div className="w-8/12">
                    <div className="flex">
                        <p className="text-sm">Itineraries:</p>
                        {showItinerary && <div>
                            {Object.keys(itineraries).map((key) => (
                            <div className="flex block">
                                <label className="pl-4 w-full">
                                    <input 
                                        type="radio" 
                                        value={key} 
                                        checked={routeSelected==key}
                                        onChange={(e) => handleRoute(e, itineraries[key])} 
                                        onClick={(e) => handleRoute(e, itineraries[key])} 
                                    />
                                    <span className="pl-1 text-sm">{key}</span>
                                    <span className={routeSelected==key ? "text-j-orange underline pl-4 pt-1 text-xs font-bold cursor-pointer" : "text-j-gray pl-4 pt-1 text-xs font-bold cursor-pointer"}>Details</span>
                                </label>
                                {/* <span>
                                    <button 
                                        onClick={()=>openDetailedItinery(itineraries[key])} 
                                        disabled={routeSelected==key ? false : true}
                                        className="font-bold">
                                       <span className={routeSelected==key ? "text-j-orange underline pl-4 pt-1 text-xs" : "text-j-gray pl-4 pt-1 text-xs"}>Details</span>
                                    </button>
                                </span> */}
                            </div>
                            ))}
                             {departureDate != null && <span className="text-sm font-normal text-gray-700 text-center pl-4">(selected date: {moment(departureDate).format("MM-DD-YYYY")})</span> }
                        </div>
                    }
                    {!showItinerary && 
                        <div className="w-full bg-gray-200 ml-2">
                            <p className="text-center text-gray-400 text-sm pt-8 pb-8">No Itineraries to display on selected Date</p>
                        </div>
                    }
                    </div>
                </div>
                <div className="w-4/12 pl-4">
                    <div className="flex w-full justify-end">
                        <p className="justify-end text-sm">Cabins:</p>
                        <div className="pl-2 w-full justify-end">
                            {showCabin && !limitstatus && <>
                                {cabinStatus.map((data,i) => {
                                    console.log(data)
                                    return(
                                    <label className="flex items-center">
                                        <input 
                                            type="checkbox" 
                                            value={i}
                                            onChange={(e) => selectCabins(e)}
                                            checked={data.isChecked}
                                        />
                                        <span className="ml-2 text-sm">{data.name}</span>
                                        {data.offer && <span className={cx(styles.tooltip,"text-xs ml-2 text-j-orange")}><i className="fas fa-tag"></i><span className={cx(styles.tooltiptext)}>{data.dataOfferText}</span></span>}
                                        {(data.onBoardOffer !=null) && <span className={cx(styles.tooltip,"text-xs ml-2 text-j-orange")}><i className="fas fa-tag"></i><span className={cx(styles.tooltiptext)}>{data.onBoardOffer}</span></span>}
                                    </label>)
                                })}
                            </>}
                            {!showCabin && 
                                <div className="w-full bg-gray-200">
                                    {!cabinsLoad && <p className="text-center text-gray-400 text-sm pt-8 pb-8">No Cabins available/Please select itineraries</p>}
                                    {cabinsLoad && <div className="opacity-60 flex justify-center">
                                        <ReactLoading type={'bubbles'} color={'blue'} />
                                    </div>}
                                </div>
                            }
                        </div>
                    </div>
                    {showCabin && <a className="text-sm cursor-pointer text-j-orange font-medium" href="https://cordelia-images-prod.s3.ap-south-1.amazonaws.com/static/August+Offers+T+%26+C.pdf" target="_blank">*Offers policy</a>}
                </div>
            </div>
            <div className="flex mt-4">
                <div className="w-full">
                    <div className="flex w-full">
                        <p className="pt-3 w-1/12 text-sm">Guests:</p>
                        <div className="w-11/12 flex"> 
                            {showGuest && <>
                                <div className="w-4/12 ml-2">
                                    <div className="flex">
                                        <label className="text-sm text-gray-700 self-center w-4/12">
                                            Adults
                                        </label>
                                        <select
                                            id="lead"
                                            name="lead"
                                            autoComplete="lead"
                                            className="mt-1 block w-7/12 py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm  text-gray-600 text-sm"
                                            onChange={(e)=> selectGuestAdult(e)}
                                            value={adultCount}
                                        >
                                            {Array(totalGuestAllowed).fill(null).map((data, i) => {
                                                return <option value={i+1}>{i+1}</option>
                                            })}
                                        </select>
                                    </div>
                                    {Array(adultCount).fill(null).map((data, i) => {
                                        return(
                                            <div className="flex mt-2">
                                                <span className="pt-2 text-sm">{i+1}</span>
                                                <input type="date" className="rounded-md p-1 ml-2 border w-10/12 text-sm" max={adultMax} />
                                            </div>
                                        )
                                    })}
                                </div>
                                <div className="w-4/12">
                                    <div className="flex">
                                        <label className="text-sm text-gray-700 self-center w-4/12">
                                            Child
                                        </label>
                                        <select
                                            id="lead"
                                            name="lead"
                                            autoComplete="lead"
                                            className="mt-1 block w-7/12 py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm  text-gray-600 text-sm"
                                            onChange={(e)=>selectGuestChild(e)}
                                            value={childCount}
                                        >
                                            <option value="" selected disabled hidden>Choose here</option>
                                            {Array(totalGuestAllowed).fill(null).map((data, i) => {
                                                return <option value={i}>{i}</option>
                                            })}   
                                        </select>
                                    </div>
                                    {Array(childCount).fill(null).map((data, i) => {
                                        return(
                                            <div className="flex mt-2 ml-2">
                                                <span className="pt-2 text-sm">{i+1}</span>
                                                <input type="date" className="rounded-md p-1 ml-2 border w-10/12 text-sm" max={childMax}/>
                                            </div>
                                        )
                                    })}
                                </div>
                                <div className="w-4/12">
                                    <div className="flex">
                                        <label className="text-sm text-gray-700 self-center w-4/12">
                                            Infant
                                        </label>
                                        <select
                                            id="lead"
                                            name="lead"
                                            autoComplete="lead"
                                            className="mt-1 block w-7/12 py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm  text-gray-600 text-sm"
                                            onChange={(e)=>selectGuestInfant(e)}
                                            value={infantCount}
                                        >
                                            <option value="" selected disabled hidden>Choose here</option>
                                            {Array(totalGuestAllowed).fill(null).map((data, i) => {
                                                return <option value={i}>{i}</option>
                                            })}   
                                        </select>
                                    </div>
                                    {Array(infantCount).fill(null).map((data, i) => {
                                        return(
                                            <div className="flex mt-2 ml-2">
                                                <span className="pt-2 text-sm">1</span>
                                                <input type="date" className="rounded-md p-1 ml-2 border w-10/12 text-sm" max={infantMax}/>
                                            </div>
                                        )
                                    })}
                                </div>
                            </>}
                            
                            {!showGuest && 
                                <div className="w-full bg-gray-200">
                                    <p className="text-center text-gray-400 text-sm pt-8 pb-8">Please select Itinerary</p>
                                </div>
                            }
                        </div>
                    </div>
                    {limitstatus && <div className="mt-2">
                        <Alert severity="error">Maximum Limit (4) exceed. Please Select total 4 People!</Alert>
                    </div>
                    }
                    {showGuest && <div className="flex justify-end mt-2 mb-2">
                        <select className="border rounded p-2" onChange={(e)=> handleSelectPromoCode(e)} value={promoCode}>
                            <option value="">Select your promo code</option>
                            <option value="FutureDesk10">FutureDesk10</option>
                            <option value="Loyalty10">Loyalty10</option>
                            <option value="Friends10">Friends10</option>
                            <option value="Zee30">Zee30</option>
                            <option value="Recover15">Recover15</option>
                            <option value="Middayexpo10">Middayexpo10</option>
                            <option value="Cordelia5">Cordelia5</option>
                            <option value="Cordelia20">Cordelia20</option>
                            <option value="Defence10">Defence10</option>
                        </select>
                    </div>}
                    {showGuest && <button className={limitstatus ? "text-right w-full pr-2 pb-2 text-j-gray mt-4" : "text-right w-full pr-2 pb-2 text-j-orange font-medium" } onClick={checkAvailability} disabled={limitstatus || (routeSelected == null) || (routeSelected == '') ? true : false}>check availabitlity</button>}
                </div>                                    
            </div>
        </div>
        <Dialog
            disableBackdropClick
            disableEscapeKeyDown
            fullScreen={fullScreen}
            width="lg"
            aria-labelledby="responsive-dialog-title"
            open={open}
            onClose={handleClose}
        >
            <DialogTitle id="responsive-dialog-title">Which Date You Want To Travel?</DialogTitle>
            <DialogContent>
                <div className="grid grid-cols-3 gap-2">
                    {itinerariesDetails.map((option,i) => (
                        <div className="mt-2 mb-2">
                            <label className="pl-4">
                                <input 
                                    type="radio" 
                                    value={option.id} 
                                    checked={selectedDate==option.id}
                                    onChange={(e) => handleSelectDate(e, i, option.departure_date)} 
                                />
                                <span className="pl-1 text-sm">{moment(option.departure_date).format("DD MMMM YYYY")}</span>
                            </label>
                        </div>
                    ))}
                </div>
                {/* {showDetails && <div className="p-4 bg-gray-300 grid grid-cols-2">
                    <div>
                        <span>Ports:</span>{itinerariesDetails[itineraryindex].name}
                    </div>
                    <div>
                        <span>End Date:</span>{itinerariesDetails[itineraryindex].rem_obj.endTime}
                    </div>
                </div>} */}
            </DialogContent>
            <DialogActions>
                <Button autoFocus  color="primary" onClick={handleClose}>
                    Cancel
                </Button>
                <Button color="primary" onClick={handleOk}>
                Ok
                </Button>
            </DialogActions>
        </Dialog>
    </>
    )
}


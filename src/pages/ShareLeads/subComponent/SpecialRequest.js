import React, { Component, useRef, useMemo, useState, useEffect } from "react";
import ReactLoading from 'react-loading';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';
import Dialog from '@material-ui/core/Dialog';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useTheme } from '@material-ui/core/styles';

export default function SpecialRequest({handleSpecialRequest, requstStatus, requestData, addRequestSelected, handleSpecification, handleClearAll, handleSubmitSpecification, requestButtonStatus, handleChangeAdditional}) {

    const theme = useTheme();
    const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));

    return(
        <div>
            <Dialog
                disableBackdropClick
                disableEscapeKeyDown
                fullScreen={fullScreen}
                width="lg"
                aria-labelledby="responsive-dialog-title"
                open={requstStatus}
                onClose={handleSpecialRequest}
            >
                <DialogData 
                    handleSpecialRequest={handleSpecialRequest} 
                    requestData={requestData}
                    addRequestSelected={addRequestSelected}
                    handleSpecification={handleSpecification}
                    handleClearAll={handleClearAll}
                    handleSubmitSpecification={handleSubmitSpecification}
                    handleChangeAdditional={handleChangeAdditional}
                />
            </Dialog>
            <button
                className={!requestButtonStatus?"h-10 items-center rounded transition duration-300 ease-in-out bg-gray-300 text-center text-white pl-2 pr-2 mt-4":"h-10 items-center rounded transition duration-300 ease-in-out bg-j-orange text-center text-white pl-2 pr-2 mt-4"}
                onClick={handleSpecialRequest}
                disabled={!requestButtonStatus}
            >
                Special Request
            </button>
        </div>
    )
}

const DialogData = ({handleSpecialRequest, requestData, addRequestSelected, handleSpecification, handleClearAll, handleSubmitSpecification, handleChangeAdditional}) => {
    console.log(requestData)
    const [showMore, setShowMore] = useState(false)
    return(
        <div className="p-8 w-full">
            <div className="flex pb-4">
                <p className="w-9/12 font-bold">Special Request</p>
                <p className="w-3/12 flex justify-end cursor-pointer" onClick={()=>handleSpecialRequest()}>Close</p>
            </div>
            <div>
                <table>
                    <tr className="p-2">
                        <th className="w-64 text-left">Items</th>
                        <th className="w-64 text-left pl-2 pr-4">Specifications</th>
                        <th className="w-64 text-left">Price</th>
                        <th></th>
                    </tr>
                    <tbody className="mt-2">
                        {requestData.map((data, i) => {
                            console.log(requestData.selectedSpecificationValue)
                            return(
                                <>
                                    {i < 4 && <tr className="p-4">
                                        <td className="text-left pt-2">{data.name}</td>
                                        <td className="pl-2 pr-4 pt-2">
                                            <select className="p-2 border rounded pl-8 pr-8 w-full" onChange={(e)=>handleSpecification(e,i)} value={data.selectedSpecificationValue}>
                                                {Object.keys(data.specificastions).map((keys,i) => {
                                                     return(<option value={String(keys)}>{keys}</option>)
                                                })}  
                                            </select>
                                        </td>
                                        <td className="pt-2">&#x20B9;{data.selectedSpecification.total_price}</td>
                                        <td className="pt-2 text-orange underline cursor-pointer"><p onClick={()=>addRequestSelected(i)}>{data.isSelected ? 'Clear' : 'Add'}</p></td>
                                    </tr>
                                    }
                                    {i >= 4 && showMore &&<tr className="p-4">
                                        <td className="text-left pt-2">{data.name}</td>
                                        <td className="pl-2 pr-4 pt-2">
                                            <select className="p-2 border rounded pl-8 pr-8 w-full" onChange={(e)=>handleSpecification(e,i)} value={data.selectedSpecificationValue}>
                                                {Object.keys(data.specificastions).map((keys,i) => {
                                                     return(<option>{keys}</option>)
                                                })}
                                            </select>
                                        </td>
                                        <td className="pt-2">&#x20B9;{data.selectedSpecification.total_price}</td>
                                        <td className="pt-2 text-orange underline cursor-pointer"><p onClick={()=>addRequestSelected(i)}>{data.isSelected ? 'Clear' : 'Add'}</p></td>
                                    </tr>}
                                </>
                            )
                        })}
                        {showMore && <tr>
                            <td colspan="4">
                                <input 
                                    type="textarea" 
                                    name="textValue"
                                    onChange={(e)=>handleChangeAdditional(e)} 
                                    placeholder="Other Additional Requirement"
                                    className="h-20 w-full  border rounded p-4 mt-4"
                                />
                            </td>
                        </tr>}
                    </tbody>
                </table>
                <p className="flex justify-end text-orange w-full cursor-pointer" onClick={()=>setShowMore(!showMore)}>{showMore? 'View Less':'View More'}</p>
            </div>
            <div className="flex w-full justify-end mt-4">
                <button className="p-2 border rounded w-3/12 text-center text-orange" onClick={()=>handleClearAll()}>CLEAR ALL</button>
                <button className="p-2 border rounded ml-2  bg-j-orange text-center w-3/12 text-white" onClick={()=>handleSubmitSpecification()}>SUBMIT</button>
            </div>
        </div>
    )
}
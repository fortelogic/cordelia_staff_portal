import React, { Component, useRef, useMemo, useState, useEffect } from "react";
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';
import Dialog from '@material-ui/core/Dialog';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useTheme } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import ReactLoading from 'react-loading';
import PhoneIField from '../../../components/Form/PhoneFieldAlt';
import cx from 'classnames';
import styles from "../sharelead.module.css"

export default function CustomerSection({
    customer,
    handleCustomer,
    searchData,
    department,
    handleDepartment,
    country,
    state,
    handleState,
    city,
    handleCity,
    name,
    firstname,
    panNumber,
    number,
    email,
    leadStatus,
    handleLeadStatus,
    priority,
    handlePriority,
    callbackdate,
    handleCallbackdate,
    callbacktime,
    handleCallbacktime,
    handleCountry,
    countryStatesArray,
    CountryStates,
    StateCities,
    nri,
    handleNri,
    handleFirstName,
    handlePanNumber,
    handleEmail,
    handlePhone,
    inquary,
    errors,
    owner,
    searchOpen,
    handleSearchClose,
    searchapiData,
    handleCustomerChange,
    searchLoading,
    nameError,
    panNumberError,
    emailError,
    numberError,
    cityError,
    emailFormatError,
    changeCustomer,
    phoneFormatError,
    lastName,
    lastNameError,
    handleLastName
}) {
    const [search, setSearch] = useState(null);
    const [temDate, setTempDate] = useState()
    const [tempTime, setTempTime] = useState();
    const [dataSelect, setDataSelect] = useState(0);
    const [selectedIndex, setSelectedIndex] = useState(0)

    const theme = useTheme();
    const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));

    useEffect(()=> {
        setTempDataFun();
    })

    const setTempDataFun = () => {
        setTempDate(callbackdate)
        setTempTime(callbacktime)
    }

    const handleCustomerChangeIn = (e) => {
        setDataSelect(e.target.value)
        setSelectedIndex(e.target.value)
    }
    return(
        <>
             <Dialog
                disableBackdropClick
                disableEscapeKeyDown
                fullScreen={fullScreen}
                width="lg"
                aria-labelledby="responsive-dialog-title"
                open={searchOpen}
                onClose={handleSearchClose}
            >
                <DialogTitle id="responsive-dialog-title">Please Select a User from the list!</DialogTitle>
                <DialogContent>
                    {searchapiData.length == 0 && <p className="text-center">No user Found</p>}
                    {searchapiData.length > 0 && <> {searchapiData.map((data,i) => {
                    return (<div className="bg-white w-full mt-2 mb-2">
                       <label className="pl-2 pr-2 mb-4 cursor-pointer">
                            <input 
                                type="radio" 
                                value={i} 
                                checked={i==selectedIndex}
                                onChange={(e)=>handleCustomerChangeIn(e)} 
                            />
                            <span className="font-normal pl-2">Name:</span><span className="pl-1 text-sm">{data.name}</span>
                            <span className="font-normal">, Email ID:</span><span className="pl-1 text-sm">{data.email}</span>
                            <span className="font-normal">, Mob:</span><span  className="pl-1 text-sm">{data.phone_number}</span>
                        </label>
                    </div>)})}</>}
                </DialogContent>
                <DialogActions>
                    <Button variant="contained" color="primary" onClick={()=>handleCustomerChange(dataSelect)}>
                        Ok
                    </Button>
                </DialogActions>
            </Dialog>
            <div className="ml-12 mr-12 mt-12 border-b-2 pb-6 border-black">
                <div className="flex">
                    <div className="flex w-3/12 pt-2">
                        <p className="text-sm">Customer:</p>
                        <label className="pl-4">
                            <input 
                                type="radio" 
                                value={customer} 
                                checked={customer=="new"}
                                onChange={handleCustomer} 
                            />
                            <span className="pl-1 text-sm">New</span>
                        </label>
                        <label className="pl-4">
                            <input 
                                type="radio" 
                                value={customer} 
                                checked={customer=="existing"}
                                onChange={handleCustomer} 
                            />
                            <span className="pl-1 text-sm">Existing</span>
                        </label>
                    </div>
                    <div className="w-6/12">
                        <div className="shadow flex w-full ml-6">
                            <input 
                                className="w-full rounded p-2" 
                                type="text" 
                                placeholder="Search by Name, Contact number, Email ID or Lead Number" 
                                // disabled={customer=="new"?true:false}
                                onChange={(e)=>setSearch(e.target.value)} 
                                onFocus={changeCustomer}
                            />
                            <button 
                                className="bg-white w-auto flex justify-end items-center text-blue-500 p-2 hover:text-blue-400" 
                                disabled={search == null || search == ""?true:false}
                                onClick={() => searchData(search)}
                            >
                                <i className={search == null || search == ""? "fas fa-search text-gray-300":"fas fa-search"}></i>
                            </button>
                            {searchLoading && 
                            <div className="mb-4">
                                <ReactLoading type={'bubbles'} color={'blue'} className="mb-2 h-2" height={20} width={50}/>
                            </div>}
                        </div>
                    </div>
                    <div className="w-3/12 pt-2 ml-6">
                        <p className="text-right text-sm">Owner: {owner}</p>
                    </div>
                </div>
                <div className="flex mt-4">
                    <div className="flex w-3/12 pt-2">
                        <p className="text-sm">Department:</p>
                        <label className="pl-4">
                            <input 
                                type="radio" 
                                value={department} 
                                checked={department=="b2b"}
                                onChange={handleDepartment} 
                            />
                            <span className="pl-1 text-sm">B2B</span>
                        </label>
                        <label className="pl-4">
                            <input 
                                type="radio" 
                                value={department} 
                                checked={department=="b2c"}
                                onChange={handleDepartment} 
                            />
                            <span className="pl-1 text-sm">B2C</span>
                        </label>
                    </div>
                    <div className="w-9/12">
                        <div className="grid grid-cols-3 gap-4">
                            <div className="flex">
                                <label htmlFor="country" className="text-sm text-gray-700 self-center">
                                    Country
                                </label>
                                <select
                                    id="country"
                                    name="country"
                                    autoComplete="country"
                                    value={country}
                                    className="mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm ml-2 text-sm"
                                    onChange={(e)=> handleCountry(e)}
                                >
                                    {
                                        CountryStates.countries.map((country) => {
                                            return <option value={country.country} selected>{country.country}</option>
                                        })
                                    }
                                </select>
                            </div>
                            <div className="flex">
                                <label htmlFor="country" className="text-sm text-gray-700 self-center">
                                    State
                                </label>
                                <select
                                    id="state"
                                    name="state"
                                    autoComplete="state"
                                    value={state}
                                    className="mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm ml-2 text-sm"
                                    onChange={(e)=>handleState(e)}
                                >
                                    {
                                        countryStatesArray.map((states) => {
                                            return  <option value={states} selected>{states}</option>
                                        })
                                    }
                                </select>
                            </div>
                            <div>
                                <div className="flex">
                                    <label htmlFor="city" className="text-sm text-gray-700 self-center">
                                        City
                                    </label>
                                    <select
                                        id="city"
                                        name="city"
                                        autoComplete="city"
                                        value={city}
                                        className="mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm ml-2 text-sm"
                                        onChange={(e)=>handleCity(e)}
                                    >
                                        <option selected value={null} disabled>{city ? city : "Choose a City"}</option>
                                        {state&&StateCities[state]?
                                            (StateCities[state].map((city) => {
                                            return  <option value={city}>{city}</option>
                                            })):null
                                        }
                                    </select>
                                </div>
                                {cityError && <p className="text-xs text-red-500 w-full block text-center">Please enter your city</p>}
                            </div>
                            <div>
                                <div className="flex">
                                    {/* <label className="text-sm text-gray-700 self-center">
                                        Nri
                                    </label> */}
                                    <div className="ml-2">
                                        <input 
                                        id="nri"
                                        name='nri'
                                        type="checkbox"
                                        autoComplete="nri"
                                        defaultChecked={nri}
                                        onChange={(e)=>handleNri(e)}
                                        >
                                        </input><label htmlFor="nri" className="ml-2">
                                            Lead is an NRI
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="grid grid-cols-5 mt-6">
                    <div className="border-r-2 border-black text-center">
                        <p>Inquiry No</p>
                        <p className="pt-4 text-gray-600">{inquary}</p>
                    </div>
                    <div className="border-r-2 border-black text-center">
                        <p>First Name</p>
                        <input
                            id="firstname"
                            name="firstname"
                            placeholder="First Name"
                            defaultValue={firstname}
                            disabled={customer=='existing'?false:false}
                            className="mt-4 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm ml-2 text-center pt-1 pb-1 text-gray-600"
                            onChange={(e)=>handleFirstName(e)}
                        />
                        {nameError && <p className="text-xs text-red-500">Please enter your first name</p>}
                    </div>
                    <div className="border-r-2 border-black text-center">
                        <p>Last Name</p>
                        <input
                            id="lastname"
                            name="lastname"
                            placeholder="Last Name"
                            defaultValue={lastName}
                            disabled={customer=='existing'?false:false}
                            className="mt-4 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm ml-2 text-center pt-1 pb-1 text-gray-600"
                            onChange={(e)=>handleLastName(e)}
                        />
                        {lastNameError && <p className="text-xs text-red-500">Please enter your Last name</p>}
                    </div>
                    <div className="border-r-2 border-black text-center">
                        <p>Contact No</p>
                        <input
                            id="contact"
                            name="contact"
                            type="number"
                            min="0"
                            placeholder="Contact No"
                            defaultValue={number}
                            disabled={customer=='existing'?false:false}
                            className={cx(styles.inputText,"mt-4 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm ml-2 text-center pt-1 pb-1 text-gray-600")}
                            onChange={(e)=>handlePhone(e)}
                        />
                        {numberError && <p className="text-xs text-red-500">Please enter your Mobile Number</p>}
                        {phoneFormatError && <p className="text-xs text-red-500">Please enter your valid Number</p>}
                    </div>
                    <div  className="text-center">
                        <p>Email Id</p>
                        <input
                            id="email"
                            type="email"
                            name="email"
                            placeholder="Email"
                            defaultValue={email}
                            // disabled={customer=='existing'?true:false}
                            className="mt-4 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm ml-2 text-center pt-1 pb-1 text-gray-600 pl-2 pr-2"
                            onChange={(e)=>handleEmail(e)}
                        />
                        {emailError && <p className="text-xs text-red-500">Please enter your Email ID</p>}
                        {emailFormatError && <p className="text-xs text-red-500">Please enter a valid Email ID</p>}
                    </div>
                    <div className="border-r-2 border-black text-center">
                       
                    </div>
                    <div className="border-r-2 border-black text-center pt-4">
                        <p>Pan Number</p>
                        <input
                            id="panNumber"
                            name="panNumber"
                            placeholder="Pan Number"
                            defaultValue={panNumber}
                            // disabled={customer=='existing'?false:false}
                            className="mt-4 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm ml-2 text-center pt-1 pb-1 text-gray-600"
                            onChange={(e)=>handlePanNumber(e)}
                        />
                        {/* {panNumberError && <p className="text-xs text-red-500">Please enter your pan number</p>} */}
                    </div>
                </div>
                <div className="flex mt-6">
                    <div className="flex w-6/12">
                        <div className="flex w-6/12">
                            <label className="text-sm text-gray-700 self-center w-4/12">
                                Lead Status
                            </label>
                            <select
                                id="lead"
                                name="lead"
                                autoComplete="lead"
                                value={leadStatus}
                                className="mt-1 block w-7/12 py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm text-gray-600 text-sm"
                                onChange={(e)=>handleLeadStatus(e)}
                            >
                            <option value="2nd-Attempt">2nd-Attempt</option>
                            <option value="3rd-Attempt">3rd-Attempt</option>
                            <option value="Quote-sent">Quote sent</option>
                            <option value="Hot-Lead">Hot Lead</option>
                            <option value="General-Enquiry">General Enquiry</option>
                            <option value="Follow-up">Follow up</option>
                            <option value="Payment-link-sent">Payment link sent</option>
                            <option value="Quotation-Pending">Quotation Pending</option>
                            <option value="Call-not-Answering">Call not Answering</option>
                            <option value="Call-Not-Contactable">Call not Contactable</option>
                            <option value="Potential-No-Responses">Potential - No Responses</option>
                            <option value="Booked">Booked</option>
                            <option value="Price-High">Price High</option>
                            <option value="Duplicate">Duplicate</option>
                            <option value="Junk">Junk</option>
                            <option value="Lot-of-Competition">Lost of Competition</option>
                            <option value="Plan-Cancelled">Plan Cancelled</option>
                            <option value="One-way-travel">One way travel </option>
                            <option value="Future-Travel">Future Travel</option>
                            <option value="Old-Reschedule">Old Reschedule</option>

                            </select>
                        </div>
                        <div className="flex w-6/12">
                            <div className="flex w-full">
                                <label className="text-sm text-gray-700 self-center w-3/12">
                                    Priority
                                </label>
                                <select
                                    id="priority"
                                    name="priority"
                                    autoComplete="priority"
                                    value={priority}
                                    className="mt-1 block w-7/12 py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm text-gray-600 text-sm"
                                    onChange={(e)=>handlePriority(e)}
                                >
                                <option value="p0">P0</option>
                                <option value="p1">P1</option>
                                <option value="p2">P2</option>
                                <option value="p3">P3</option>
                                </select>
                            </div>
                        </div>    
                    </div>
                    <div className="flex w-6/12">
                        <div className="flex w-8/12 justify-end">
                            <label className="text-sm text-gray-700 self-center w-3/12">
                                Call back
                            </label>
                            <input
                                type="date"
                                id="calldate"
                                defaultValue={temDate}
                                className="mt-1 block w-6/12 py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm text-gray-600 text-sm"
                                onChange={(e)=>handleCallbackdate(e)}
                            />
                        </div>
                        <div className="flex w-4/12 justify-end">
                            <input
                                id="time"
                                type="time"
                                defaultValue={tempTime}
                                className="mt-1 block w-10/12 py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm ml-2 text-gray-600 text-sm"
                                onChange={(e)=>handleCallbacktime(e)}
                            />
                        </div>   
                    </div>
                </div>
            </div>
        </>
    )
}
import React, { Component, useRef, useMemo, useState, useEffect, forwardRef,useImperativeHandle } from "react";
// import Pdf from '../../../assets/pdf/cancelation_recshedule_policy.pdf';

const PriceDetails= forwardRef(({ roomSelected, requestSlectedTotal, requestSlectedTCS, requestSlectedGST },ref)=> {

    const [selectedRoom, setSelectRoom] = useState(0);
    const [totalPrice, setTotalPrice] = useState(roomSelected[0][1].price.total);
    const [gst, setGst] = useState(roomSelected[0][1].price.gst);
    const [tcs, setTcs] = useState(roomSelected[0][1].price.tcs == undefined ? null : roomSelected[0][1].price.tcs);
    const [insurance, setInsurance] = useState(roomSelected[0][1].price.insurance);
    const [portCharge, setPortCharge] = useState(roomSelected[0][1].price.port_charges);
    const [discount, setDiscount] = useState(roomSelected[0][1].price.discount);
    const [gratuvity, setGratuvity] = useState(roomSelected[0][1].price.gratuity);
    const [gratuityChecked, setGratuvityChecked] = useState(true)
    const [discountChecked, setDiscountChecked] = useState(true)

    const Pdf = "https://images.cordeliacruises.com/static/cancellation_and_reschedule_policy.pdf"
    const selectRoom = (e) => {
        console.log("nik-log",roomSelected[e.target.value][1].price.gst,roomSelected)
        setSelectRoom(e.target.value)
        setTotalPrice(roomSelected[e.target.value][1].price.total)
        setGst(roomSelected[e.target.value][1].price.gst)
        setTcs(roomSelected[e.target.value][1].price.tcs == undefined ? null : roomSelected[e.target.value][1].price.tcs)
        setInsurance(roomSelected[e.target.value][1].price.insurance)
        setPortCharge(roomSelected[e.target.value][1].price.port_charges)
        setDiscount(roomSelected[e.target.value][1].price.discount)
        setGratuvity(roomSelected[e.target.value][1].price.gratuity)
    }



    useImperativeHandle(ref, () => ({

        defaultSelection(roomSelected){
            setSelectRoom(0)
            setTotalPrice(roomSelected[0][1].price.total)
            setGst(roomSelected[0][1].price.gst)
            setTcs(roomSelected[0][1].price.tcs == undefined ? null : roomSelected[0][1].price.tcs)
            setInsurance(roomSelected[0][1].price.insurance)
            setPortCharge(roomSelected[0][1].price.port_charges)
            setDiscount(roomSelected[0][1].price.discount)
            setGratuvity(roomSelected[0][1].price.gratuity)
    }
    
      }));
    // const defaultSelection = () =>{
    //     console.log("nik-log",roomSelected)
    //     setSelectRoom(0)
    //     setTotalPrice(roomSelected[0][1].price.total)
    //     setGst(roomSelected[0][1].price.gst)
    //     setTcs(roomSelected[0][1].price.tcs == undefined ? null : roomSelected[0][1].price.tcs)
    //     setInsurance(roomSelected[0][1].price.insurance)
    //     setPortCharge(roomSelected[0][1].price.port_charges)
    //     setDiscount(roomSelected[0][1].price.discount)
    //     setGratuvity(roomSelected[0][1].price.gratuity)
    // }

    const handleGratuvity = (data) => {
        setGratuvityChecked(data)
        if(data) {
            setTotalPrice(totalPrice+gratuvity)
        } else {
            setTotalPrice(totalPrice-gratuvity)
        }

    }

    const handleDiscount = (data) => {
        setDiscountChecked(data)
        if(data) {
            setTotalPrice(totalPrice+(discount==null?0:discount))
        } else {
            setTotalPrice(totalPrice-(discount==null?0:discount))
        }

    }

    return(
        <>
           {roomSelected.map((key,i) => {
                return(
                    <div className="grid grid-cols-2">
                        <div className="flex">
                            <label className="pl-4">
                                <input 
                                    type="radio" 
                                    value={i} 
                                    checked={i==selectedRoom}
                                    onChange={(e) => selectRoom(e)} 
                                />
                                <span className="pl-1 text-sm">{key[1].name}</span>
                            </label>
                        </div>
                        <div className="flex w-full">
                            <p className="w-full text-center text-sm font-bold">&#x20B9;{Math.round(key[1].price.cabin_fare)}</p>
                        </div>
                    </div>)
            })}
            <div className="grid grid-cols-2 pt-2">
                <p className="pl-4 text-sm">Service Charges & Levies</p>
                <div className="flex w-full">
                    <p className="w-full text-center text-sm font-bold">&#x20B9;{Math.round(portCharge+gratuvity)}</p>
                </div>
            </div>
            {/* <div className="grid grid-cols-2 pt-2">
                <p className="pl-4 text-sm">Port Charges</p>
                <div className="flex w-full">
                    <p className="w-full text-center text-sm font-bold">&#x20B9;{Math.round(portCharge)}</p>
                </div>
            </div>
            <div className="grid grid-cols-2 pt-2">
                <p className="pl-4 text-sm">Gratuity</p>
                <div className="flex w-full">
                    <p className="w-full text-center text-sm font-bold">&#x20B9;{Math.round(gratuvity)}</p>
                </div>
            </div> */}
            <div className="grid grid-cols-2 pt-2">
                <p className="pl-4 text-sm">GST</p>
                <div className="flex w-full">
                    <p className="w-full text-center text-sm font-bold">&#x20B9;{Math.round(gst+requestSlectedGST)}</p>
                </div>
            </div>
            {tcs != null && <div className="grid grid-cols-2 pt-2">
                <p className="pl-4 text-sm">TCS</p>
                <div className="flex w-full">
                    <p className="w-full text-center text-sm font-bold">&#x20B9;{Math.round(tcs+requestSlectedTCS)}</p>
                </div>
            </div>
            }
            {requestSlectedTotal > 0 && <div className="grid grid-cols-2 pt-2">
                <p className="pl-4 text-sm">Special Request</p>
                <div className="flex w-full">
                    <p className="w-full text-center text-sm font-bold">&#x20B9;{Math.round(requestSlectedTotal-requestSlectedGST-requestSlectedTCS)}</p>
                </div>
            </div>
            }
            {/* <div className="grid grid-cols-2 pt-2">
                <p className="pl-4 text-sm">Insurance</p>
                <div className="flex w-full">
                    <p className="w-full text-center text-sm font-bold">&#x20B9;{Math.round(insurance)}</p>
                </div>
            </div> */}
            <div className="grid grid-cols-2 pt-2">
                <p class="ml-4 text-sm">Discount</p>
                <div className="flex w-full">
                    <p className="w-full text-center text-sm font-bold">&#x20B9;{Math.round(discount==null?0:discount)}</p>
                </div>
            </div>
            <div className="grid grid-cols-2 pt-4">
                <p className="pl-4 font-bold text-lg">Grand Total</p>
                <div className="flex w-full">
                    <p className="w-full text-center font-bold">&#x20B9;{Math.round(totalPrice+requestSlectedTotal)}</p>
                </div>
            </div>
            <div className="grid grid-cols-1 pt-4">
                <a className="pl-4 pr-4 text-sm cursor-pointer text-j-orange font-medium" href={Pdf} target="_blank">*Rescheduling and Cancellation policy</a>
            </div>
            <div className="grid grid-cols-1 pt-4">
                <a className="pl-4 pr-4 text-sm cursor-pointer text-j-orange font-medium" href="https://images.cordeliacruises.com/static/insurance_web_document.pdf" target="_blank">
                  *Insurance Policy Document
                  </a>
            </div>
            <div className="grid grid-cols-1 pt-4">
                <a className="pl-4 pr-4 text-sm cursor-pointer text-j-orange font-medium" href="https://images.cordeliacruises.com/static/OTHER_ADDITIONAL_CHARGES.pdf" target="_blank">
                  Other Additional Charges
                  </a>
            </div>
        </>
    )
})

export default PriceDetails;
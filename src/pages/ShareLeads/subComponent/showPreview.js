import React, { Component, useRef, useMemo, useState, useEffect } from "react";
import ReactLoading from 'react-loading';
import moment from "moment"

export default function ShowPreview({ 
    firstname,
    lastName,
    number,
    email,
    country,
    selectNight,
    roomSelected,
    itinerariesDetails,
    itineraryindexValue,
    adultCount,
    infantCount,
    childCount,
    handleModify,
    handleSend,
    inquary,
    sendLoad,
    requestSlectedTotal,
    requestSlectedTCS, 
    requestSlectedGST
 }) {

    console.log(firstname)
    console.log(roomSelected)
    return(
        <div className="m-12">
            <div className="grid grid-cols-2">
                <div>
                    <p className="text-lg font-bold text-gray-500">Preview</p>
                </div>
                <div className="w-full">
                    <p className="text-sm text-right">Inquiry No: {inquary}</p>
                </div>
            </div>
            <div className="grid grid-cols-2">
                <div className="mt-4">
                    <p className="text-sm font-bold">First Name: <span className="text-sm font-normal">{firstname}</span></p>
                    <p className="text-sm font-bold">Last Name: <span className="text-sm font-normal">{lastName}</span></p>
                    <p className="text-sm font-bold">Email ID: <span className="text-sm font-normal">{email}</span></p>
                    <p className="text-sm font-bold">Contact Number: <span className="text-sm font-normal">{number}</span></p>
                </div>
                <div className="flex justify-end mt-4">
                    <button
                        className="h-10 items-center rounded transition duration-300 ease-in-out bg-white text-center text-black border-red-700 border ml-4 mr-4 pl-4 pr-4"
                        onClick={(e)=>handleModify(e)}
                    >
                        Modify
                    </button>
                    <button
                        className={sendLoad ? "h-10 items-center rounded transition duration-300 ease-in-out bg-gray-500 text-center text-white ml-4 mr-4 pl-6 pr-6" :"h-10 items-center rounded transition duration-300 ease-in-out bg-j-orange text-center text-white ml-4 mr-4 pl-6 pr-6" }
                        onClick={(e)=>handleSend(e)}
                        disabled={sendLoad}
                    >
                        {sendLoad ? <ReactLoading type={'spinningBubbles'} color={'white'}  height={'100%'} width={'40px'} className="p-2"/> : "Send" }
                    </button>
                </div>
            </div>
            <p className="text-md mt-4 mb-4">Quotation Details</p>
            <div className="grid grid-cols-2 border-b-2 pb-6 border-gray-700">
                <div className="border-r-2 border-gray-600">
                    <div className="flex">
                        <div className="w-4/12">
                            <p className="text-sm">Nights:</p>
                        </div>
                        <div  className="w-8/12">
                            <p className="text-sm">{selectNight}</p>
                        </div>
                    </div>
                    <div className="flex">
                        <div className="w-4/12">
                            <p className="text-sm">Itinerary:</p>
                        </div>
                        <div  className="w-8/12">
                            <p className="text-sm">{itinerariesDetails[itineraryindexValue].cruice_name}</p>
                        </div>
                    </div>
                    <div className="flex">
                        <div className="w-4/12">
                            <p className="text-sm">Departure:</p>
                        </div>
                        <div  className="w-8/12">
                            <p className="text-sm">{itinerariesDetails[itineraryindexValue].Departure}, {moment(itinerariesDetails[itineraryindexValue].departure_date).format("DD-MMMM-YYYY")}</p>
                        </div>
                    </div>
                    <div className="flex">
                        <div className="w-4/12">
                            <p className="text-sm">Arrival:</p>
                        </div>
                        <div  className="w-8/12">
                            <p className="text-sm">{itinerariesDetails[itineraryindexValue].destination}, {moment(itinerariesDetails[itineraryindexValue].rem_obj.endTime).format("DD-MMMM-YYYY")}</p>
                        </div>
                    </div>
                    {/* <div className="flex">
                        <div className="w-4/12">
                            <p className="text-sm">Halt:</p>
                        </div>
                        <div  className="w-8/12">
                            {itinerariesDetails[itineraryindex].rem_obj.ports.map((data)=>{
                                return <span className="text-sm border-r border-black pr-2"></span>
                            })}
                        </div>
                    </div> */}
                </div>
                <div className="pl-4">
                    <div className="flex">
                        <div className="w-4/12">
                            <p className="text-sm">Cabins:</p>
                        </div>
                        <div  className="w-8/12">
                            <p className="text-sm">
                                {roomSelected.map((data) => {
                                    return(<span>
                                        {data[1].name}, 
                                    </span>)
                                })}
                            </p>
                        </div>
                    </div>
                    <div className="flex">
                        <div className="w-4/12">
                            <p className="text-sm">Departure:</p>
                        </div>
                        <div  className="w-8/12">
                            <p className="text-sm">{adultCount>0 ? (adultCount<2?adultCount + " Adult":adultCount + " Adults") : ""} {childCount>0 ? (childCount<2?childCount + " Child":childCount + " Children") : ""} {infantCount>0 ? (infantCount<2?infantCount + " Infant":infantCount + " Infants") : ""}</p>
                        </div>
                    </div>
                </div>
            </div>
            <div className="grid grid-cols-4 gap-4 mt-6">
                {roomSelected.map((data,i) => {
                    return(<div className="border shadow-xl rounded-xl p-4">
                        <p>Cabin {i+1}</p>
                        <p className="text-j-magenta font-bold pb-1">{data[1].name}</p>
                        <div className="flex mt-2">
                            <div className="w-6/12 text-sm">
                                Cabin Price:
                            </div>
                            <div className="w-6/12 text-sm">
                            &#x20B9;{Math.round(data[1].price.cabin_fare)}
                            </div>
                        </div>
                        <div className="flex mt-2">
                            <div className="w-6/12 text-sm">
                                Service Charges & Levies
                            </div>
                            <div className="w-6/12 text-sm">
                                &#x20B9;{Math.round(data[1].price.port_charges+data[1].price.gratuity)}
                            </div>
                        </div>
                        {/* <div className="flex mt-2">
                            <div className="w-6/12 text-sm">
                                Port Charges:
                            </div>
                            <div className="w-6/12 text-sm">
                                &#x20B9;{Math.round(data[1].price.port_charges)}
                            </div>
                        </div>
                        <div className="flex mt-2">
                            <div className="w-6/12 text-sm">
                                Gratuity:
                            </div>
                            <div className="w-6/12 text-sm">
                            &#x20B9;{Math.round(data[1].price.gratuity)}
                            </div>
                        </div> */}
                        <div className="flex mt-2">
                            <div className="w-6/12 text-sm">
                                GST:
                            </div>
                            <div className="w-6/12 text-sm">
                            &#x20B9;{Math.round(data[1].price.gst+requestSlectedGST)}
                            </div>
                        </div>
                        {data[1].price.tcs != undefined && <div className="flex mt-2">
                            <div className="w-6/12 text-sm">
                                TCS:
                            </div>
                            <div className="w-6/12 text-sm">
                            &#x20B9;{Math.round(data[1].price.tcs+requestSlectedTCS)}
                            </div>
                        </div>}
                        <div className="flex mt-2">
                            <div className="w-6/12 text-sm">
                                Special Request:
                            </div>
                            <div className="w-6/12 text-sm">
                            &#x20B9;{Math.round(requestSlectedTotal-requestSlectedGST-requestSlectedTCS)}
                            </div>
                        </div>
                        {/* <div className="flex mt-2">
                            <div className="w-6/12 text-sm">
                                Insurance:
                            </div>
                            <div className="w-6/12 text-sm">
                            &#x20B9;{Math.round(data[1].price.insurance)}
                            </div>
                        </div> */}
                        <div className="flex mt-2">
                            <div className="w-6/12 text-sm">
                                Discount:
                            </div>
                            <div className="w-6/12 text-sm">
                            &#x20B9;{Math.round(data[1].price.discount==null?0:data[1].price.discount)}
                            </div>
                        </div>
                        <div className="flex mt-2">
                            <div className="w-6/12 font-bold">
                                Total Price:
                            </div>
                            <div className="w-6/12 font-bold">
                                &#x20B9;{Math.round(data[1].price.total+requestSlectedTotal)}
                            </div>
                        </div>
                    </div>)
                })}
            </div>
        </div>
    )
}
import React, { Component, useRef, useMemo, useState, useEffect } from "react";
import CountryStates from "../../public/country-states.json";
import StateCities from "../../public/state-cities.json";
import TopNavigation from "../../components/TopNavigation/index";

import CustomerSection from "./subComponent/customerSection"
import ProductDetails from "./subComponent/productDetails"
import PriceDetails from "./subComponent/priceDetails"
import ShowPreview from "./subComponent/showPreview"
import SpecialRequest from "./subComponent/SpecialRequest"

import { getToken } from "../../utils/common";
import moment from "moment";
import Joi from "joi";
import { joiResolver } from "@hookform/resolvers/joi";
import { useForm } from "react-hook-form";
import { extendMoment } from "moment-range";

import Button from '@material-ui/core/Button';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import { makeStyles } from '@material-ui/core/styles';
import { useHistory } from "react-router-dom";

import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogActions from '@material-ui/core/DialogActions';
import Dialog from '@material-ui/core/Dialog';


var tempArray = [];
var obj = {};
var tempSecArray;
var reqestSelectedTempArray = []
var requestObj = new Object;
var special_request_gst = 0;
var special_reqst_tcs = 0;
var special_reqst_total = 0;

var previous_selected_tcs = 0
var previous_selected_gst = 0
var previous_selected_total = 0

function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
  }
  
  const useStyles = makeStyles((theme) => ({
    root: {
      width: '100%',
      '& > * + *': {
        marginTop: theme.spacing(2),
      },
    },
  }));

export default function ShareLead() {
    const classes = useStyles();
    let history = useHistory();
    const [successopen, setSuccessopen] = React.useState(false);
    const [customer, setCustomer] = useState('new');
    const [department, setDepartment] = useState('b2c');
    const [country, setCountry] = useState("India");
    const [countryStatesArray, setCountryState] = useState(["Maharashtra", "Tamil Nadu", "Karnataka", "Kerala", "Andaman and Nicobar Islands", "Andhra Pradesh", "Arunachal Pradesh", "Assam", "Bihar", "Chandigarh", "Chhattisgarh", "Dadra and Nagar Haveli", "Daman and Diu", "Delhi", "Goa", "Gujarat", "Haryana", "Himachal Pradesh", "Jammu and Kashmir", "Jharkhand", "Lakshadweep", "Madhya Pradesh", "Manipur", "Meghalaya", "Mizoram", "Nagaland", "Orissa", "Pondicherry", "Punjab", "Rajasthan", "Sikkim", "Tripura", "Uttaranchal", "Uttar Pradesh", "West Bengal"]);

    const [state, setState] = useState("Maharashtra");
    const [city, setCity] = useState(null);
    const [nri,setNri] =useState(false);
    const [name, setName] = useState(null);
    const [firstname, setFirstName] = useState(null);
    const [lastName, setLastName] = useState(null);
    const [panNumber, setPanNumber] = useState(null);
    const [number, setNumber] = useState(null);
    const [inquary, setInquary] = useState(null)
    const [email, setEmail] = useState(null);
    const [leadStatus, setLeadStatus] = useState('2nd-Attempt');
    const [priority, setPriority] = useState('p0');
    const [callbackdate, setCallBackDate] = useState(new Date());
    const [callbackTime, setCallBackTime] = useState(new Date());
    const [itinerariesDetails, setItinerariesDetails] = useState([])
    const [itineraryindexValue, setItineraryIndexValue] = useState([])
    const [previewButtonStatus, setPreviewButtonStatus] = useState(true);
    const [searchOpen, setSearchOpen] = useState(false);

    const [showNights, setshowNight] = useState(false);
    const [showItinerary, setshowItinerary] = useState(false);
    const [showPriceDetails, setshowPriceDetails] = useState(false);
    const [showGuest, setshowGuest] = useState(false);
    const [selectedDate, setSelectedDate]= useState();
    const [departureDate, setDepartureDate] = useState(null)
    const [itineraryindex, setitineraryindex] = useState(0)
    const [limitstatus, setLimitStatus] = useState(false);
    const [adultCount, setAdultCount] = useState(1);
    const [childCount, setChildCount] = useState(0);
    const [infantCount, setInfantCount] = useState(0);
    const [totalGuestSelected, setTotalGuestSelected] = useState(0);
    const [cabins, setCabins] = useState();
    const [cabinStatus, setCabinStatus] = useState([]);
    const [cabinsLoad, setCabinLoad] = useState(false);
    const [showCabin, setshowCabin] = useState(false);
    const [open, setOpen] = useState(false);
    const [searchapiData, setSearchApiData] = useState([])
    const [searchLoading, setSearchLoading] = useState(false)
    const [routeSelected, setRouteSelected] = useState();

    const [selectNight, setSelectNight] = useState();
    const [nights, setNights] = useState([]);
    const [nightLoad, setNightLoad] = useState(false);
    
    const [routes, setRoutes] = useState([]);
    const [itineraries, setItineraries] = useState([])
    const [roomSelected, setRoomSelected] = useState();
    const [showPreview, setShowPreview] = useState(false);

    const [nameError, setNameError] = useState(false);
    const [lastNameError, setLastNameError] = useState(false)
    const [panNumberError, setPanNumberError] = useState(false)
    const [emailError, setEmailError] = useState(false);
    const [numberError, setNumberError] = useState(false);
    const [cityError, setCityError] = useState(false);
    const [commonError, setCommonError] = useState(true);
    const [emailFormatError, setEmailFormatError] = useState(false)
    const [phoneFormatError, setPhoneFormatError] = useState(false)
    const [additionalRequirement, setAdditionalRequirement] = useState()

    const [requstStatus, setRequestStatus] = useState(false)
    const [requestData, setRequestData] = useState([])
    const [requestLoading, setRequestLoading] = useState(false)
    const [requestSelected, setRequestSelected] = useState([])
    const [requestSlectedTotal, setRequestSelectedTotal] = useState(0)
    const [requestSlectedGST, setRequestSelectedGST] = useState(0)
    const [requestSlectedTCS, setRequestSelectedTCS] = useState(0)
    const [requestButtonStatus, setRequestButtonStatus] = useState(false)
    const [sendLoad, setSendLoad] = useState(false)

    const [promoCode, setPromoCode] = useState()

    const momentValue = extendMoment(moment);

    const today = momentValue();
    const [days, setDays] = useState(momentValue.range(today.clone().add(7, "days"), today.clone()));
    const [initialDay, setInitialDay] = useState(true)
    const [todayVal ,setTodayVal] = useState(today)
    let owner = window.localStorage.getItem('name');

    const myRef = useRef(null)
    const priceDetailRef = useRef();

    useEffect(() => {
        tempArray.splice(0, tempArray.length)
        initialApiCalls()
    },[])


    const initialApiCalls = () => {
        fetch(process.env.REACT_APP_API_ENDPOINT+'/staff_portal/enquiries/new_enquiry_number', {
            method: 'GET',
            headers: {
              Accept: "application/json",
              "Content-Type": "application/json",
              Authorization:
              `Bearer ${getToken()}`,
            },
        }).then((response) => { 
              if(response.ok){
                 return response.json()
              }
              throw Error("Verification failed");
             }
        ).then((response) => {
            console.log(response)
            setInquary(response.number)
        })
    }

    const handleCustomer = () => {
        if(customer == "new") {
            setCustomer("existing")
        } else if(customer == "existing") {
            setCustomer("new")
        }
    }

    const searchData = (search) => {
        console.log(search)
        setSearchLoading(true)
        fetch(process.env.REACT_APP_API_ENDPOINT+'/staff_portal/enquiries/customer_search?term='+search, {
            method: 'GET',
            headers: {
              Accept: "application/json",
              "Content-Type": "application/json",
              Authorization:
              `Bearer ${getToken()}`,
            },
        }).then((response) => { 
              if(response.ok){
                 return response.json()
              }
              throw Error("Verification failed");
             }
        ).then((response) => {
            setSearchOpen(true)
            console.log(response)
            setSearchApiData(response)
            setSearchLoading(false)
        })
    }

    const handleCountry = (e) => {
        console.log(e.target.value)
        let x = e.target.value;
        setCountry(x);
        setCountryState(CountryStates.countries.filter((country) => country.country === x)[0].states);
    }

    const handleDepartment = () => {
        if(department == "b2b") {
            setDepartment("b2c")
        } else if(department == "b2c") {
            setDepartment("b2b")
        }
    }

    const handleDateOfTravel = (e) => {
        // console.log(e.target.value)
       console.log(routes)

    }

    const onSelect = (value) => {
        console.log(value)
        setInitialDay(false)
        setNightLoad(true)
        setDays(value)
    }

    const DateRangeApiCall = () => {
        console.log(days.start)
        setshowNight(false)
        setNightLoad(true)
        setshowItinerary(false)
        setSelectNight(null)
        var start_date = days.start.format("YYYY-MM-DD");
        var to_date = days.end.format("YYYY-MM-DD");
        console.log(days.start.format("YYYY-MM-DD"))
        fetch(process.env.REACT_APP_API_ENDPOINT+'/staff_portal/routes?start_date='+days.start.format("YYYY-MM-DD")+'&to_date='+days.end.format("YYYY-MM-DD"), {
            method: 'GET',
            headers: {
              Accept: "application/json",
              "Content-Type": "application/json",
              Authorization:
              `Bearer ${getToken()}`,
            },
        }).then((response) => { 
              if(response.ok){
                 return response.json()
              }
              throw Error("Verification failed");
             }
        ).then((response) => {
            console.log(response)
            let apiResponseRoutes = response.data.routes;
            if(apiResponseRoutes.length == 0) {
                setNightLoad(false)
                setshowNight(false)
                setDepartureDate(null)
                setSelectNight(null)
                setRouteSelected(null)
                setshowItinerary(false)
            } else {
                setDepartureDate(null)
                setRouteSelected(null)
                setshowCabin(false)
                setshowPriceDetails(false)
                setRoutes(apiResponseRoutes);
                var tempArray= [];
                apiResponseRoutes.forEach((ele) => {
                    for (let i = 0; i < ele.itineraries.length; i++) {
                        let obj = {};
                        obj.name = ele.name
                        obj.itiId = ele.id;
                        obj.id = ele.itineraries[i].id;
                        obj.cruice_name = ele.itineraries[i].name;
                        obj.Departure = ele.itineraries[i].ports[0].port.name;
                        obj.destination =
                        ele.itineraries[i].ports[
                            ele.itineraries[i].ports.length - 1
                        ].port.name;
                        obj.departure_date = `${new Date(ele.itineraries[i].startTime)}`;
                        obj.no_days = ele.itineraries[i].nightCount;
                        obj.rem_obj = ele.itineraries[i];
                        // console.log(ele.itineraries[i].nightCount)
                        tempArray.push(obj)
                        obj = {};
                    }
                })

                console.log(tempArray);
                let groupedDays = null;
                groupedDays = tempArray.reduce(function (r, a) {
                    r[a.no_days] = r[a.no_days] || [];
                    r[a.no_days].push(a);
                    return r;
                },Object.create(null))
                for (const key in groupedDays) {
                    groupedDays[key] = groupedDays[key].reduce(function (r, a) {
                    r[a.cruice_name] = r[a.cruice_name] || [];
                    r[a.cruice_name].push(a);
                    return r;
                    }, Object.create(null));
                }

                const onlyUnique = (value, index, self) => {
                    return self.indexOf(value) === index;
                };

                {Object.keys(groupedDays).map((key) => {
                    console.log(key)
                    let temp = null;
                    let monthHere = null
                    let itinerary = routes;
                    temp = [];
                    let count = key || 2;
                    let routesData = groupedDays[count];
                    let arr = Object.values(routesData);
                    for (let i in arr) {
                        if (arr[i]) {
                        let x = null;
                        x = arr[i][0].itiId;
                        for (let j in itinerary) {
                            if (itinerary[j].id === x) {
                            temp.push(itinerary[j]);
                            }
                        }
                        }
                    }
                    let unique = [];
                    let allMonthsItineraries = [];
                    let arrMonth = [];
                    unique = temp.filter(onlyUnique);
                    if(monthHere) {
                    for(let i = 0; i < unique.length; i++) {
                        arrMonth = [];
                        allMonthsItineraries = unique[i].itineraries;
                        for(let j = 0; j < allMonthsItineraries.length; j++) {
                        let allMonthChecker = Number((new Date(allMonthsItineraries[j].startTime)).getMonth()) + 1;
                        if (allMonthChecker === Number(monthHere)) {
                            arrMonth.push(allMonthsItineraries[j]);
                        }
                        }
                        unique[i].itineraries = arrMonth;
                    }
                    }
                })}

                console.log(groupedDays)
                setNights(groupedDays);
                setNightLoad(false)
                setshowNight(true)
            }
        })
    }

    const handleSelectDate = (event,i, departureDate) => {
        console.log('nik-log date select',event.target.value,departureDate,i)
        setSelectedDate(event.target.value)
        setDepartureDate(departureDate)
        setitineraryindex(i);
        setshowGuest(true);

            var dummyArray = [1,2,3,4,5,6,7,8,9,10]
            fetch(process.env.REACT_APP_API_ENDPOINT+'/staff_portal/special_requests/?itinerary='+event.target.value, {
                method: 'GET',
                headers: {
                  Accept: "application/json",
                  "Content-Type": "application/json",
                  Authorization:
                  `Bearer ${getToken()}`,
                },
            }).then((response) => { 
                  if(response.ok){
                     return response.json()
                  }
                  throw Error("Verification failed");
                 }
            ).then((response) => {
                console.log(response.special_request)
                var resposnseRequest;
                var resposnseRequestArray = [];
                Object.keys(response.special_request).map((key,i) => {
                    console.log(Object.keys(response.special_request[key])[0])
                    resposnseRequest = {
                        name: key,
                        isSelected: false,
                        selectedSpecification: response.special_request[key][Object.keys(response.special_request[key])[0]],
                        specificastions: response.special_request[key],
                        selectedSpecificationValue: Object.keys(response.special_request[key])[0],
                        previousSelectedAmount: previous_selected_total,
                        previousSelectedGst: previous_selected_gst,
                        previousSelectedTcs: previous_selected_tcs
                    }
                    resposnseRequestArray.push(resposnseRequest)
                })
                // setInquary(response.number)
                setRequestSelected([])
                setRequestSelectedTotal(0);
                setRequestSelectedGST(0);
                setRequestSelectedTCS(0)
                special_request_gst = 0;
                special_reqst_tcs = 0;
                special_reqst_total = 0;
                setRequestData(resposnseRequestArray)
                setRequestButtonStatus(true)
            })

        console.log('hai selected')
    }

    const selectGuestAdult = (e) => {
        console.log(e.target.value)
        setAdultCount(parseInt(e.target.value,10));
        var adult = parseInt(e.target.value,10);
        var child = childCount
        var infant = infantCount

        console.log(adult)
        console.log(child)
        console.log(infant)

        var total = adult+child+infant;
        console.log(total)
        setTotalGuestSelected(total)
        console.log(totalGuestSelected);
        if(total > 4) {
            setLimitStatus(true)
        } else {
            setLimitStatus(false)
        }
    }

    const selectGuestChild = (e) => {
        console.log(e.target.value)
        setChildCount(parseInt(e.target.value,10));
        var adult = adultCount;
        var child =  parseInt(e.target.value,10)
        var infant = infantCount

        console.log(adult)
        console.log(child)
        console.log(infant)

        var total = adult+child+infant;
        console.log(total)
        setTotalGuestSelected(total)
        if(total > 4) {
            setLimitStatus(true)
        } else {
            setLimitStatus(false)
        }
    }

    const selectGuestInfant = (e) => {
        console.log(e.target.value)
        setInfantCount(parseInt(e.target.value,10));
        var adult = adultCount;
        var child =  childCount
        var infant = parseInt(e.target.value,10)

        console.log(adult)
        console.log(child)
        console.log(infant)

        var total = adult+child+infant;
        console.log(total)
        setTotalGuestSelected(total)
        if(total > 4) {
            setLimitStatus(true)
        } else {
            setLimitStatus(false)
        }
    }

    const checkAvailability = () => {
        setCabinLoad(true);
        setshowCabin(false);
        setshowPriceDetails(false)
        var emptyArray = []
        setCabinStatus(emptyArray)
        setRoomSelected(null)
        tempArray.splice(0, tempArray.length)
        fetch(process.env.REACT_APP_API_ENDPOINT+'/staff_portal/itineraries/'+
        selectedDate +'/check_availability', {
            method: 'POST',
            headers: {
              Accept: "application/json",
              "Content-Type": "application/json",
              Authorization:
              `Bearer ${getToken()}`,
            },
            body:  JSON.stringify({
                visitorID: null,
                itineraryID: selectedDate,
                promoCode: promoCode,
                rooms: [
                    {
                        adults: adultCount,
                        infants: infantCount,
                        children: childCount,
                    }
                ],
                pan: panNumber
            })
        }).then((response) => { 
              if(response.ok){
                 return response.json()
              }
              throw Error("Verification failed");
             }
        ).then((response) => {
            let cabins =[];
            response.data.checkAvailability[0].categories.forEach(category=>{
                cabins.push(category);
                if(category.sub_category){
                    category.sub_category.forEach(subCategory=>{
                        cabins.push(subCategory);
                    })
                }
            })
            setCabins(cabins)
            var productsList;
            var productListArray = [];
            response.data.checkAvailability[0].categories.map((data) => {
                productsList = {
                    name: data.name,
                    isChecked: false,
                    offer: data.is_offer,
                    onBoardOffer: data.onboard_credit_offer,
                    dataOfferText: data.offer_description
                }
                productListArray.push(productsList)
                if(data.sub_category){
                    data.sub_category.map(subCategory=>{
                        productsList = {
                            name: subCategory.name,
                            isChecked: false,
                            offer: subCategory.is_offer,
                            onBoardOffer: subCategory.onboard_credit_offer,
                            dataOfferText: subCategory.offer_description
                        }
                        productListArray.push(productsList)
                    })
                }
            })
            console.log(productListArray);
            setCabinStatus(productListArray)
            setCabinLoad(false)
            setshowCabin(true)
        })
    }

    const openDetailedItinery = (key) => {
        console.log(key)
        setItinerariesDetails(key)
        setOpen(true);
    };

    const selectCabins = (e,i) => {
        console.log(cabinStatus)
        let newArr = [...cabinStatus];
        newArr[e.target.value].isChecked = !newArr[e.target.value].isChecked;
        setCabinStatus(newArr)

        var tempValue = e.target.value;
        console.log(tempValue);
        if(tempArray.length == 0) {
            console.log('haI3')
            obj = {};
            tempArray=[]
            obj[e.target.value] = cabins[e.target.value];
            tempArray.push(obj);
            tempSecArray = new Array(tempArray[0]);
            setshowPriceDetails(true)
            console.log(tempArray);
        } else {
            console.log(tempValue)
            console.log(tempArray[0][tempValue])
            if(tempArray[0][tempValue] === undefined) {
                console.log(tempArray);
                console.log('haI2')
                tempArray=[]
                obj[e.target.value] = cabins[e.target.value];
                tempArray.push(obj);
                setshowPriceDetails(true)
            } else {
                tempArray.map((data) => {
                    delete data[tempValue]
                })
                var dummyArrayCheck = Object.entries(tempArray[0])
                console.log(dummyArrayCheck.length);
                if(dummyArrayCheck.length === 0) {
                    console.log('haI0')
                    setshowPriceDetails(false)
                } else {
                    console.log('haI1')
                    setshowPriceDetails(true)
                }
            }
        }
        console.log('nik-log cabin',Object.entries(tempArray[0]));
        // setshowPriceDetails(false)
        setItinerariesDetails(itinerariesDetails)
        setItineraryIndexValue(itineraryindex)
        setRoomSelected(Object.entries(tempArray[0]));
        setPreviewButtonStatus(false)
        
        getPriceDetails(
            tempArray,
            nights,
            itinerariesDetails,
            itineraryindex, 
            adultCount, 
            childCount,
            infantCount);
        
    }


    const handleNight = (e,nights) => {
        // console.log(e.target.value)
        setSelectNight(e.target.value)
        console.log(nights[e.target.value])
        setItineraries(nights[e.target.value]); 
        setshowItinerary(true);
    }

    const getPriceDetails = (
        data, 
        nights, 
        itinerariesDetails, 
        itineraryindex, 
        adultCount, 
        childCount,
        infantCount) => {
        // setshowPriceDetails(false)
        setItinerariesDetails(itinerariesDetails)
        setItineraryIndexValue(itineraryindex)
        setRoomSelected(Object.entries(data[0]));
        // setshowPriceDetails(true)
        setPreviewButtonStatus(false)
        priceDetailRef.current.defaultSelection(Object.entries(data[0]))
    }

    const showPreviewFunc = () => {
        if(firstname == null || firstname == "") {
            setNameError(true)
            setCommonError(true)
        } 
        if(lastName == null || lastName == "") {
            setLastNameError(true)
            setCommonError(true)
        } 
        if(email ==null || email =="") {
            setEmailError(true)
            setCommonError(true)
        } else {
            var mailformat = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
            if(email.match(mailformat)){
                setEmailFormatError(false)
                var forward = true;
            } else {
                setEmailFormatError(true)
                var forward = false;
            }
        }
        if(number ==null || number =="") {
            setNumberError(true)
            setCommonError(true)
        } else {
            var phone_pattern = /^(\+[0123456789]{2,4}[ ])?[0123456789]{10,12}$/;
            if(number.match(phone_pattern)) {
                setPhoneFormatError(false)
                var forwardMob = true
            } else {
                setPhoneFormatError(true)
                var forwardMob = false
            }
        }

        
        if(city ==null || city =="") {
            setCityError(true)
            setCommonError(true)
        }
        console.log(forward)
        if((firstname == null || firstname == "") || (lastName == null || lastName == "" ) || (email ==null || email =="") || (number ==null || number =="") || (city ==null || city =="") || !forward || !forwardMob) {
            setShowPreview(false)
            myRef.current.scrollIntoView()  
        } else {
            setShowPreview(true)
        }
    }

    const handleFirstName = (e) => {
        if(e.target.value!=""){
            setNameError(false)
        } else {
            setNameError(true)
        }
        setFirstName(e.target.value)
    }

    const handlePanNumber = (e) => {
        if(e.target.value!==""){
            setPanNumberError(false)
        } else {
            setPanNumberError(true)
        }
        setPanNumber(e.target.value)
    }

    const handleLastName = (e) => {
        if(e.target.value!=""){
            setLastNameError(false)
        } else {
            setLastNameError(true)
        }
        setLastName(e.target.value)
    }

    const handleEmail = (e) => {
        if(e.target.value!=""){
            setEmailError(false)
        } else {
            setEmailError(true)
        }
        setEmail(e.target.value)
    }

    const handlePhone = (e) => {
        if(e.target.value!=""){
            setNumberError(false)
        } else {
            setNumberError(true)
        }
        setNumber(e.target.value)
    }

    const handleState = (e) => {
        setState(e.target.value)
    }

    const handleCity = (e) => {
        if(e.target.value!=""){
            setCityError(false)
        } else {
            setCityError(true)
        }
        setCity(e.target.value)
    }

    const handleNri = (e) => {
        setNri(e.target.checked);
    }

    const handleLeadStatus = (e) => {
        setLeadStatus(e.target.value)
    }

    const handlePriority = (e) => {
        setPriority(e.target.value)
    }

    const handleModify = () => {
        setShowPreview(false)
    }

    const handleCallbackdate = (e) => {
        setCallBackDate(e.target.value)
    }

    const handleCallbacktime =(e) => {
        setCallBackTime(e.target.value)
    }

    const handleSend = () => {
        setSendLoad(true)
        console.log(requestSelected)
        var arrayRooms = [];
        var obj;
        roomSelected.map((data)=> {
            arrayRooms.push({
                priceKey:data[1].priceKey,
                promoCode:promoCode,
                special_reqst_total:requestSlectedTotal,
                special_reqst_gst:requestSlectedGST,
                special_reqst_tcs:requestSlectedTCS,
                special_reqst: requestSelected,
                additional_requirement: additionalRequirement
            })
        })
        console.log(firstname)
        var sendingValue = {
            first_name : firstname,
            last_name  : lastName,
            phone_number: number,
            pan: panNumber,
            email: email,
            country: country,
            state: state,
            number: inquary,
            city:city,
            nri: nri,
            lead_status:leadStatus,
            priority: priority,
            callback_time: callbackdate,
            department: department, 
            rooms : arrayRooms,
        }

        console.log(arrayRooms)
        console.log(sendingValue)

        fetch(process.env.REACT_APP_API_ENDPOINT+'/staff_portal/enquiries', {
            method: 'POST',
            headers: {
              Accept: "application/json",
              "Content-Type": "application/json",
              Authorization:
              `Bearer ${getToken()}`,
            },
            body:  JSON.stringify(sendingValue)
        }).then((response) => { 
              if(response.ok){
                 return response.json()
              }
              throw Error("Verification failed");
             }
        ).then((response) => {
           console.log(response)
           if(response.status == "Success") {
            requestObj = new Object;
            setSuccessopen(true);
            
           }
        })
    }

    const schema = Joi.object({
        name,
    })

    const { register, errors, handleSubmit, setError } = useForm({
        resolver: joiResolver(schema),
    });


    const submitHandler = handleSubmit((data) => {
        console.log('test')
    })

    const handleClose = () => {
        setOpen(false);
    };

    const handleOk = () => {
        setOpen(false);
    }

    const handleSearchClose = () => {
        setSearchOpen(false)
    }

    const handleCustomerChange = (data) => {
        if(searchapiData.length > 0) {
            var selectedCustomer = searchapiData[data];
            console.log(selectedCustomer)
            setDepartment(selectedCustomer.department_id)
            setName(selectedCustomer.name)
            setFirstName(selectedCustomer.first_name)
            if(selectedCustomer.last_name != undefined) {
                setLastName(selectedCustomer.last_name)
            }
            setNumber(selectedCustomer.phone_number)
            setEmail(selectedCustomer.email)
        } 
      
        // setCity(selectedCustomer.city);
        // setState(selectedCustomer.state);
        // setCountry(selectedCustomer.country)
        
        // setLeadStatus(selectedCustomer.lead_status)
        // setPriority(selectedCustomer.priority)
        // setCallBackDate(selectedCustomer.callback_time)
        // setCallBackTime(selectedCustomer.callback_time)
        setSearchOpen(false)
    }

    const handleSuccess = () => {
        history.push({
            pathname: '/admin/dashboard',
        })
    }

    const changeCustomer = () => {
        setCustomer("existing")
    }

    const handleRoute = (e, key) => {
        console.log("nik-log handle route",key)
        setRouteSelected(e.target.value)
        setItinerariesDetails(key)
        setOpen(true);
        if(key) {
            const event={target:{value:key[0].id}}
            const departure_date=key[0].departure_date
            handleSelectDate(event,0,departure_date)
        }
    }

    const handleSpecialRequest = () => {
        setRequestLoading(true)
        setRequestStatus(!requstStatus)
        setRequestLoading(false)
    }

    const addRequestSelected = (i) => {
        let newArr = [...requestData];
        console.log(newArr[i])
        var selectedName = newArr[i].name;
        if(newArr[i].isSelected) {
            delete requestObj[selectedName]; 
            console.log(requestObj)
            special_reqst_total = special_reqst_total - newArr[i].selectedSpecification.total_price
            special_request_gst = special_request_gst - newArr[i].selectedSpecification.gst
            special_reqst_tcs = special_reqst_tcs - newArr[i].selectedSpecification.tcs
        } else {
            requestObj[selectedName] = newArr[i].selectedSpecification
            special_reqst_total = special_reqst_total + newArr[i].selectedSpecification.total_price
            special_request_gst = special_request_gst + newArr[i].selectedSpecification.gst
            special_reqst_tcs = special_reqst_tcs + newArr[i].selectedSpecification.tcs
            console.log(requestObj)
        }

        newArr[i].isSelected = !newArr[i].isSelected;
        newArr[i].previousSelectedAmount= newArr[i].selectedSpecification.total_price
        newArr[i].previousSelectedGst= newArr[i].selectedSpecification.gst
        newArr[i].previousSelectedTcs= newArr[i].selectedSpecification.tcs
        setRequestData(newArr)
       
        // setRequestSelected()
    }

    const handleSpecification = (e,i) => {
        console.log(e.target.value)
        let newArr = [...requestData];
        console.log(newArr[i])
        var selectedName = newArr[i].name;
        if(newArr[i].isSelected) {
            console.log('hai')
            delete requestObj[selectedName]; 
            console.log(special_reqst_total)
            special_reqst_total = special_reqst_total - newArr[i].previousSelectedAmount
            special_request_gst = special_request_gst - newArr[i].previousSelectedGst
            special_reqst_tcs = special_reqst_tcs - newArr[i].previousSelectedTcs
            newArr[i].isSelected = !newArr[i].isSelected;
        }
        newArr[i].selectedSpecification = newArr[i].specificastions[e.target.value];
        newArr[i].selectedSpecificationValue = e.target.value
        
        
        setRequestData(newArr)

        // newArr[e.target.value].isChecked = !newArr[e.target.value].isChecked;
        // setCabinStatus(newArr)
    }

    const handleClearAll = () => {
        let newArr = [...requestData];
        newArr.map((data) => {
            data.isSelected = false;
        })
        requestObj = new Object;
        setRequestSelected([])
        setRequestSelectedTotal(0);
        setRequestSelectedGST(0);
        setRequestSelectedTCS(0)
        special_request_gst = 0;
        special_reqst_tcs = 0;
        special_reqst_total = 0;
        setRequestData(newArr)
    }

    const handleSubmitSpecification = () => {
        console.log(special_reqst_total)
        setRequestSelectedTotal(special_reqst_total)
        setRequestSelectedGST(special_request_gst)
        setRequestSelectedTCS(special_reqst_tcs)
        setRequestSelected(requestObj)
        setRequestStatus(false)
    }

    const handleChangeAdditional = (e) => {
        console.log(additionalRequirement)
        setAdditionalRequirement(e.target.value)
    }
   
    const handleSelectPromoCode = (e) => {
        setPromoCode(e.target.value)
    }
    
    return(
        <>
            <div className="h-full" ref={myRef}>
                <main class=" h-full mt-6 mb-6">
                    <div className="pt-2 ml-6 mr-6">
                        <TopNavigation 
                            icon="fas fa-home text-sm text-gray-500"
                            name="Add Lead"
                        />
                    </div>
                    <Dialog
                        disableBackdropClick
                        disableEscapeKeyDown
                        width="lg"
                        aria-labelledby="responsive-dialog-title"
                        open={successopen}
                        onClose={handleSuccess}
                    >
                        <DialogTitle id="responsive-dialog-title">Success!</DialogTitle>
                        <DialogContent>
                            <div className="bg-white w-full mt-2 mb-2">
                                Share Quotation forwarded successfully!!!
                            </div>
                        </DialogContent>
                        <DialogActions>
                            <Button variant="contained" color="primary" onClick={()=>handleSuccess()}>
                                Go to Dashboard
                            </Button>
                        </DialogActions>
                    </Dialog>
                    {!showPreview && <>
                        <CustomerSection
                            customer={customer}
                            handleCustomer={handleCustomer}
                            searchData={searchData}
                            department={department}
                            handleDepartment={handleDepartment}
                            country={country}
                            state={state}
                            handleState={handleState}
                            city={city}
                            handleCity={handleCity}
                            nri={nri}
                            handleNri={handleNri}
                            name={name}
                            firstname={firstname}
                            panNumber={panNumber}
                            number={number}
                            email={email}
                            leadStatus={leadStatus}
                            handleLeadStatus={handleLeadStatus}
                            priority={priority}
                            handlePriority={handlePriority}
                            callbackdate={moment(callbackdate).format("yyyy-MM-DD")}
                            handleCallbackdate={handleCallbackdate}
                            callbacktime={moment(callbackdate).format("h:mm")}
                            handleCallbacktime={handleCallbacktime}
                            handleCountry={handleCountry}
                            countryStatesArray={countryStatesArray}
                            StateCities={StateCities}
                            CountryStates={CountryStates}
                            handleFirstName={handleFirstName}
                            handlePanNumber={handlePanNumber}
                            handleEmail={handleEmail}
                            handlePhone={handlePhone}
                            inquary={inquary}
                            errors={errors}
                            owner={owner}
                            searchOpen={searchOpen}
                            handleSearchClose={handleSearchClose}
                            searchapiData={searchapiData}
                            handleCustomerChange={handleCustomerChange}
                            searchLoading={searchLoading}
                            nameError={nameError}
                            panNumberError={panNumberError}
                            emailError={emailError}
                            numberError={numberError}
                            cityError={cityError}
                            emailFormatError={emailFormatError}
                            changeCustomer={changeCustomer}
                            phoneFormatError={phoneFormatError}
                            lastName={lastName}
                            lastNameError={lastNameError}
                            handleLastName={handleLastName}
                        />
                        <div className="ml-12 mr-12 mt-6 mb-12">
                            <p className="font-bold">Production Details</p>
                            <div className="flex mt-3">
                                    <div className="w-8/12">
                                        <p className="text-sm">Quotation:</p>
                                    </div>
                                    <div className="w-4/12">
                                        <p className="text-sm pl-8">Price Details</p>
                                    </div>
                                </div>
                                <div className="flex mt-1">
                                    <ProductDetails 
                                            handleDateOfTravel={handleDateOfTravel}
                                            selectNight={selectNight}
                                            handleNights={handleNight}
                                            onSelect={onSelect}
                                            DateRangeApiCall={DateRangeApiCall}
                                            days={days}
                                            nightLoad={nightLoad}
                                            showNights={showNights}
                                            nights={nights}
                                            showItinerary={showItinerary}
                                            itineraries={itineraries}
                                            getPriceDetails={getPriceDetails}
                                            handleSelectDate={handleSelectDate}
                                            showGuest={showGuest}
                                            itineraryindex={itineraryindex}
                                            selectedDate={selectedDate}
                                            selectGuestAdult={selectGuestAdult}
                                            selectGuestChild={selectGuestChild}
                                            selectGuestInfant={selectGuestInfant}
                                            limitstatus={limitstatus}
                                            adultCount={adultCount}
                                            childCount={childCount}
                                            infantCount={infantCount}
                                            checkAvailability={checkAvailability}
                                            cabins={cabins}
                                            cabinsLoad={cabinsLoad}
                                            showCabin={showCabin}
                                            selectCabins={selectCabins}
                                            openDetailedItinery={openDetailedItinery}
                                            itinerariesDetails={itinerariesDetails}
                                            open={open}
                                            handleClose={handleClose}
                                            handleOk={handleOk}
                                            todayVal={todayVal}
                                            initialDay={initialDay}
                                            departureDate={departureDate}
                                            routeSelected={routeSelected}
                                            handleRoute={handleRoute}
                                            cabinStatus={cabinStatus}
                                            handleSelectPromoCode={handleSelectPromoCode}
                                            promoCode={promoCode}
                                    />
                                    <div className="w-4/12 pl-4 pt-2">
                                        {showPriceDetails && <>
                                        <PriceDetails 
                                                ref={priceDetailRef}
                                                roomSelected={roomSelected}
                                                requestSlectedTotal={requestSlectedTotal}
                                                requestSlectedTCS={requestSlectedTCS}
                                                requestSlectedGST={requestSlectedGST}
                                        />
                                        </>}
                                        {!showPriceDetails && 
                                            <div className="w-full bg-gray-200">
                                                <p className="text-center text-gray-400 text-sm pt-20 pb-20">No Pricing summary</p>
                                            </div>
                                        }
                                        <div className="grid grid-cols-1 mt-4">
                                            <button
                                                className={previewButtonStatus ? "h-10 items-center rounded transition duration-300 ease-in-out bg-gray-300 text-center text-white ml-4 mr-4" : "h-10 items-center rounded transition duration-300 ease-in-out bg-j-orange text-center text-white ml-4 mr-4"}
                                                disabled={previewButtonStatus}
                                                onClick={showPreviewFunc}
                                            >
                                                Preview & Send
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <div>
                                        <SpecialRequest 
                                            requstStatus={requstStatus}
                                            handleSpecialRequest={handleSpecialRequest} 
                                            requestData={requestData}
                                            addRequestSelected={addRequestSelected}
                                            handleSpecification={handleSpecification}
                                            handleClearAll={handleClearAll}
                                            handleSubmitSpecification={handleSubmitSpecification}
                                            requestButtonStatus={requestButtonStatus}
                                            handleChangeAdditional={handleChangeAdditional}
                                        />
                                </div>
                            </div>
                        </>
                    }
                    {showPreview && <>
                        <ShowPreview 
                            firstname={firstname}
                            lastName={lastName}
                            number={number}
                            email={email}
                            country={country}
                            selectNight={selectNight}
                            roomSelected={roomSelected}
                            itinerariesDetails={itinerariesDetails}
                            itineraryindexValue={itineraryindexValue}
                            adultCount={adultCount}
                            childCount={childCount}
                            infantCount={infantCount}
                            handleModify={handleModify}
                            handleSend={handleSend}
                            inquary={inquary}
                            sendLoad={sendLoad}
                            requestSlectedTotal={requestSlectedTotal}
                            requestSlectedTCS={requestSlectedTCS}
                            requestSlectedGST={requestSlectedGST}
                        />
                    </>}
                </main>
            </div>
        </>
    )
}

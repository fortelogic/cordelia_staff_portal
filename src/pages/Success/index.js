import React, { Component, useContext } from "react";
import Tile from "../../components/Tile/Tile";
import UserContext from "../../store/UserContext";
import SummaryView from "../../components/Booking/General/SummaryView";
import { getToken } from "../../utils/common";
// import Header from "../../components/Header/header";
// import Footer from "../../components/Footer/footer";
// import Cards, { Card } from "../../components/Booking/Cards/cards";
// import { IoCaretDownOutline } from "react-icons/io5";
// import LoadingIcon from "../../components/Loading/LoadingIcon"

class Success extends Component {
  constructor(props) {
    super(props);
    this.state = {
      invoice_url: "",
      displayStatus: true
    };
  }

  componentWillMount() {
    let data = JSON.parse(localStorage.getItem('booking'));
    fetch(`${process.env.REACT_APP_API_ENDPOINT}/agent_portal/bookings/${data.id}`, {
      method: "GET",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: `Bearer ${getToken()}`,
      },
    })
      .then(async (response) => {
        if (response.status === 200) {
          let data = await response.json();
          if(data.invoice_url == null){
            this.setState({displayStatus:false});
          }
          localStorage.getItem('booking', data)
          this.setState({ invoice_url: `${process.env.REACT_APP_API_ENDPOINT}/${data.invoice_url}` });
        } else {
          let data = await response.json();
          alert(data.message);
        }
      })
      .catch((err) => err);
  }

  render() {
    return (
      <>
        <div className="text-white px-6 py-4 border-0 rounded relative mb-4 bg-magenta m-10 h-full">
          <span className="inline-block align-middle mr-8">
            <b className="capitalize">Booking</b> Success
            <h4 className="pb-5">
            <i className="fas fa-check-circle text-j-green text-3xl pr-2 pt-2" />
            We have received your payment.
            </h4>
          </span>
        </div>
          <div className="text-center text-j-magenta">
          <i className="fas fa-download text-3xl pr-2 pt-2" />
            <a href={this.state.invoice_url} target="_blank" className="text-center">Download the Invoice/ E-ticket</a>
          </div>
        <TicketPreview />
      </>
    );
  }
}

const OrderDetail = ({ title, children, big }) => (
    <>
      <h1 className="uppercase pb-0 text-tiny leading-none" style={{color: "black"}}>
        {title}
      </h1>
      <h4  className="pb-6 text-j-magenta">
        {children}
      </h4>
    </>
  );

export default Success;

const TicketPreview = () => {
  const [user, setUser] = useContext(UserContext);
  const booking = user.booking || {};
  let bookigStore = JSON.parse(window.localStorage.getItem('booking'));
  const totalPrice = bookigStore.amount_paid;
  return(
      <div className="m-8">
        <SummaryView booking={booking} />
      </div>
    
  //   <Tile tiny className="mb-5 border border-j-gray-lighter">
  //   <Tile.Inner className="pb-0">
  //       <OrderDetail title="ORDER ID">{booking.id}</OrderDetail>
  //       <OrderDetail title="Ship">
  //         {booking.itinerary.ship.name}
  //       </OrderDetail>

  //     <div className="grid grid-cols-2">
  //       <div>
  //         <OrderDetail title="Departure">
  //           {booking.departure}
  //         </OrderDetail>
  //       </div>
  //       <div>
  //         <OrderDetail title="Arrival">
  //            {booking.destination}
  //         </OrderDetail>
  //       </div>
  //     </div>
  //     {Object.keys(booking.rooms)
  //                 .map((k, index) => 
  //       <OrderDetail title={`Cabin 1`}>
  //       {booking.rooms[index].selected.name}<br/>
  //                       Deck No: {((booking.rooms[index].room_number).toString()).charAt(0)}<br/>
  //                       Room No: {booking.rooms[index].room_number}<br/>
  //                       {booking.rooms[index].adults+booking.rooms[index].children+booking.rooms[index].infants}  Guest{(booking.rooms[index].adults+booking.rooms[index].children+booking.rooms[index].infants) > 1 ? "s" : null}
  //       </OrderDetail>
  //                 )}
  //     <OrderDetail title="Total amount payable" big>
  //       &#x20B9; {totalPrice}
  //     </OrderDetail>
  //   </Tile.Inner>
  // </Tile>
  )
  
}

import React, { Component, useState } from "react";
import Cards from "../../components/Booking/Cards/cards";
import LoadingIcon from "../../components/Loading/LoadingIcon";
import { getToken } from "../../utils/common";
// import RangeCalendar from "../../components/RangeCalendar/RangeCalendar"
// import Select from "../../components/Select/Select"
// import Overlay from "../../components/Overlay/Overlay"
// import View from "../../components/View/View"
// import Button from "../../components/Button/Button"

class Routes extends Component {
  constructor(props) {
    super(props);
    this.state = {
      samplesData: null,
      loading: true,
      backupData: null,
      backupWholeData: null,
      filteredItineraries: [],
      flagDestination: false,
      flagDeparture: false,
      flagNoDays: false,
      destVal: null,
      origVal: null,
      crDurVal: null,
      dest: null,
      orig: null,
      crDur: null,
      calenderItineraries: null,
      filteredItineraries: [],
    };
  }
  handleReset() {
    let backUp = this.state.backupData;
    this.setState({
      samplesData: backUp,
      flagDestination: false,
      flagDeparture: false,
      flagNoDays: false,
      destVal: null,
      origVal: null,
      crDurVal: null,
    });
  }
  handleSearch() {
    this.setState({ loading: true });
    let backUp = this.state.backupData;
    let tempArrBack = [];
    tempArrBack = backUp;
    this.setState({ samplesData: backUp });
    if (this.state.flagDestination) {
      let tempArr = [];
      tempArrBack.forEach((ele) => {
        let obj = {};
        if (
          ele.destination.toLowerCase() === this.state.destVal.toLowerCase()
        ) {
          obj = ele;
          tempArr.push(obj);
        }
      });
      tempArrBack = [];
      tempArrBack = tempArr;
    }
    if (this.state.flagDeparture) {
      let tempArr = [];
      tempArrBack.forEach((ele) => {
        let obj = {};
        if (ele.Departure.toLowerCase() === this.state.origVal.toLowerCase()) {
          obj = ele;
          tempArr.push(obj);
        }
      });
      tempArrBack = [];
      tempArrBack = tempArr;
    }
    if (this.state.flagNoDays) {
      let tempArr = [];
      tempArrBack.forEach((ele) => {
        let obj = {};
        if (ele.no_days === this.state.crDurVal) {
          obj = ele;
          tempArr.push(obj);
        }
      });
      tempArrBack = [];
      tempArrBack = tempArr;
    }
    this.setState({ samplesData: tempArrBack });
    this.setState({ loading: false });
  }
  setFilterDestination(dest) {
    this.setState({ loading: true });
    this.setState({ destVal: dest, flagDestination: true });
    this.setState({ loading: false });
  }
  setFilteredItineraries() {
    this.setState({ filteredItineraries: [] });
  }
  setFilterOrigin(orig) {
    this.setState({ loading: true });
    this.setState({ origVal: orig, flagDeparture: true });
    this.setState({ loading: false });
  }
  setCruiseDuration(cruiseDu) {
    this.setState({ loading: true });
    this.setState({ crDurVal: cruiseDu, flagNoDays: true });
    this.setState({ loading: false });
  }
  uniques = (value, index, self) => self.indexOf(value) === index;
  componentDidMount() {
    fetch(`${process.env.REACT_APP_API_ENDPOINT}/agent_portal/routes`, {
      method: "GET",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: `Bearer ${getToken()}`,
      },
    })
      .then((response) => response.json())
      .then((response) => {
        let dataArr = response.data.routes;
        this.setState({ backupWholeData: dataArr });
        let tempArr = [];
        let destinations = [];
        let origins = [];
        let durations = [];
        destinations = Object.values(dataArr)
          .map(
            (route) =>
              route.ports &&
              route.ports.length &&
              route.ports.slice(1, route.ports.length).map((p) => p.port.name)
          )
          .flat(1)
          .filter(this.uniques);
        destinations.sort();
        origins = Object.values(dataArr)
          .map(
            (route) =>
              route.ports && route.ports.length && route.ports[0].port.name
          )
          .filter(this.uniques);
        origins.sort();
        durations = [2, 3, 4, 5, 6, 7];
        durations.sort();
        this.setState({ dest: destinations });
        this.setState({ orig: origins });
        this.setState({ crDur: durations });
        dataArr.forEach((ele) => {
          for (let i = 0; i < ele.itineraries.length; i++) {
            let obj = {};
            obj.itiId = ele.id;
            obj.id = ele.itineraries[i].id;
            obj.cruice_name = ele.itineraries[i].name;
            obj.Departure = ele.itineraries[i].ports[0].port.name;
            obj.destination =
              ele.itineraries[i].ports[
                ele.itineraries[i].ports.length - 1
              ].port.name;
            obj.departure_date = `${new Date(ele.itineraries[i].startTime)}`;
            obj.no_days = ele.itineraries[i].nightCount;
            obj.rem_obj = ele.itineraries[i];
            tempArr.push(obj);
            obj = {};
          }
        });
        localStorage.setItem("itinerary", JSON.stringify(dataArr));
        this.setState({ samplesData: tempArr });
        this.setState({ backupData: tempArr });
        this.setState({ loading: false });
      })
      .catch((err) => err);
  }

  render() {
    return (
      <>
        <div className="flex flex-col h-screen">
          <main className="flex-grow overflow-y-auto">
            {this.state.loading && (
              <>
                <h1 className="text-4xl">
                  <LoadingIcon className="py-20 text-j-magenta" />
                </h1>
              </>
            )}
            {!this.state.loading && (
              <>
                <div className="ml-10 mr-10 bg-magenta mt-5 rounded-lg">
                  <div className="grid grid-cols-1 lg:grid-cols-5 gap-4 text-center pt-5 pb-5 ml-4">
                    <div className="text-center">
                      <select
                        value={this.state.destVal || "Filter By Destination"}
                        onChange={(e) => {
                          this.setFilterDestination(e.target.value);
                          this.setFilteredItineraries([]);
                        }}
                        className="pr-2 pl-2 justify-between rounded-l-full rounded-r-full h-12 border border-j-gray select-none cursor-pointer"
                      >
                        <option value={null} selected disabled>
                          Filter By Destination
                        </option>
                        {this.state.dest.map((ele) => (
                          <option key={ele} value={ele}>
                            {ele}
                          </option>
                        ))}
                      </select>
                    </div>
                    <div>
                      <select
                        value={this.state.origVal || "Filter By Origin"}
                        onChange={(e) => {
                          this.setFilterOrigin(e.target.value);
                          this.setFilteredItineraries([]);
                        }}
                        className="pr-2 pl-2 rounded-l-full rounded-r-full h-12 border border-j-gray select-none cursor-pointer"
                      >
                        <option value={null} selected disabled>
                          Filter By Origin
                        </option>
                        {this.state.orig.map((ele) => (
                          <option key={ele} value={ele}>
                            {ele}
                          </option>
                        ))}
                      </select>
                    </div>
                    {/* <div>
                      <RangeCalendarSelect
                        itineraries={Object.values(this.state.calenderItineraries)}
                        selected={
                          this.state.filteredItineraries.filter(
                          (i) => !!this.state.calenderItineraries[i.id])
                        }
                        onSelectionChanged={(sel) => {
                          this.setState({filteredItineraries:(sel || [])});
                        }}
                      />
                    </div> */}
                    <div>
                      <button
                        className="bg-j-orange shadow-md text-sm text-white font-bold py-3 md:px-8 px-4 hover:bg-blue-400 rounded uppercase"
                        onClick={() => {
                          this.handleSearch();
                        }}
                      >
                        Search
                      </button>
                    </div>
                    <div>
                      <button
                        className="bg-white shadow-md text-sm text-j-orange font-bold py-3 md:px-8 px-4 hover:bg-blue-400 rounded uppercase"
                        onClick={() => {
                          this.handleReset();
                        }}
                      >
                        Reset
                      </button>
                    </div>
                  </div>
                </div>
                {this.state.samplesData.length === 0 ? (
                  <div class="p-10 grid grid-cols-1 sm:grid-cols-1 md:grid-cols-2 lg:grid-cols-2 xl:grid-cols-2 gap-20 ml-8 mr-8">
                    No Result Found
                  </div>
                ) : (
                  <Cards cardData={this.state.samplesData} />
                )}
              </>
            )}
          </main>
        </div>
      </>
    );
  }
}

// const RangeCalendarSelect = ({ itineraries, selected, onSelectionChanged }) => {
//   const [newSelected, setNewSelected] = useState([]);
//   const [applicable, setApplicable] = useState(false);

//   return (
//     <Select
//       icon="fal fa-calendar-alt"
//       title="Filter by date"
//       overlayTitle="Cruise calendar"
//       optional
//       onSelect={(s) => {
//         onSelectionChanged && onSelectionChanged(s);
//         setApplicable(false);
//       }}
//       selectedItem={(selected && selected.length && selected) || null}
//       renderItem={() => "Dates selected"}
//       items={itineraries}
//     >
//       {({
//         items,
//         selected,
//         setSelected,
//         onSelect,
//         setOpen,
//         onCloseClicked,
//       }) => (
//         <Overlay open className="bg-j-white">
//           <View>
//             <h2 className="flex justify-between" id="cruises">
//               <span className="pt-1">Cruise calendar</span>
//               <i
//                 className="fal fa-times cursor-pointer text-j-orange text-3xl"
//                 onClick={() => {
//                   setApplicable(false);
//                   onCloseClicked();
//                 }}
//               />
//             </h2>
//             <RangeCalendar
//               ranges={items}
//               selected={selected}
//               onSelectionChanged={(sel) => {
//                 setNewSelected(sel);
//                 setApplicable(true);
//               }}
//             />
//             <Button
//               class="mt-4 w-full text-j-white bg-j-ghostwhite"
//               disabled={!applicable}
//               onClick={() => {
//                 const result = newSelected.length ? newSelected : null;
//                 setApplicable(false);
//                 setSelected(result);
//                 onSelect && onSelect(result);
//                 setNewSelected(null);
//                 setOpen(false);
//               }}
//             >
//               Apply
//             </Button>
//           </View>
//         </Overlay>
//       )}
//     </Select>
//   );
// };

export default Routes;

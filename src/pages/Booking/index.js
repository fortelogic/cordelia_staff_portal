import React, { Component, useContext, useState } from "react";
import GuestSelectionView from "../../components/Booking/GuestSelectionView";
import RoomSelectionView from "../../components/Booking/RoomSelectionView";
import GuestDetailsView from "../../components/Booking/GuestDetailsView";
import ConformBookingView from "../../components/Booking/ConformBookingView";
import PaymentSummaryView from "../../components/Booking/PaymentSummaryView";
import UserContext from "../../store/UserContext";
import RoomsCard from "../../components/Booking/Cards/RoomsCard";
import SelectionCard from "../../components/Booking/Cards/SelectionCard";
import GuestsCard from "../../components/Booking/Cards/GuestsCard";
import ProductCard from "../../components/Booking/Cards/ProductCard";

class Booking extends Component {

  state = {
    step: 1,
  };

  nextStep = () => {
    const { step } = this.state;
    this.setState({
      step: step + 1,
    });
  };

  prevStep = () => {
    const { step } = this.state;
    this.setState({
      step: step - 1,
    });
  };
  
  handleModify = (newStep) => {
    const { step } = this.state;
    this.setState({
      step: newStep
    })
  }

  handleChange = (input) => (event) => {
    this.setState({ [input]: event.target.value });
  };

  switchState = () => {
    const { step } = this.state;
    switch (step) {
      case 1:
        return (
          <>
            <HeaderView 
               prevStep={this.prevStep}
               backStatus={false}
               step={step}
               handleModify={this.handleModify}
            />
            <GuestSelectionView
              nextStep={this.nextStep}
              handleChange={this.handleChange}
              // values={values}
            />
          </>
        );
      case 2:
        return (
          <>
            <HeaderView 
               prevStep={this.prevStep}
               backStatus={true}
               step={step}
               handleModify={this.handleModify}
            />
            <RoomSelectionView
              nextStep={this.nextStep}
              prevStep={this.prevStep}
              handleChange={this.handleChange}
              // values={values}
            />
          </>
        );
      case 3:
          return (
            <>
              <HeaderView 
                prevStep={this.prevStep}
                backStatus={true}
                step={step}
                handleModify={this.handleModify}
              />
              <PaymentSummaryView
                nextStep={this.nextStep}
                prevStep={this.prevStep}
                handleChange={this.handleChange}
                // values={values}
              />
            </>
        );
      case 4:
        return (
          <>
            <HeaderView 
                prevStep={this.prevStep}
                backStatus={true}
                step={step}
                handleModify={this.handleModify}
            />
            <GuestDetailsView
              nextStep={this.nextStep}
              prevStep={this.prevStep}
              handleChange={this.handleChange}
              // values={values}
            />
          </>
        );
      case 5:
        return (
          <>
            <HeaderView 
                prevStep={this.prevStep}
                backStatus={true}
                step={step}
                handleModify={this.handleModify}
            />
            <ConformBookingView
              nextStep={this.nextStep}
              prevStep={this.prevStep}
              handleChange={this.handleChange}
              // values={values}
            />
          </>
        );
    }
  };

  render() {
    return (
      <>
        <div className="flex flex-col h-screen">
          {/* <Header /> */}
          <main class="flex-grow overflow-y-auto pr-14 pl-14">
            {this.switchState()}
          </main>
          {/* <Footer /> */}
        </div>
      </>
    );
  }
}

export default Booking;

const HeaderView = ({prevStep, backStatus, step, handleModify}) => {
  const [user, setUser] = useContext(UserContext);
  const booking = user.booking || {};
  const [modifyBooking, setModifyBooking] = useState(false)
  const backbuttonpressed = () => {
    prevStep()
  }

  const toggleModify = () => {
    setModifyBooking(!modifyBooking);
  }


  return(
    <>
      <div className="grid grid-cols-3 border-b p-6">
        <div>
            {backStatus && <button onClick={backbuttonpressed}>
                        Back
            </button>
            }
        </div>
        <div className="text-center text-j-orange font-bold">{booking.itinerary.ship.name}</div>
        <div 
          className="uppercase cursor text-right text-j-magenta"
          onClick={() => toggleModify()}
        >
                Modify Booking
        </div>
      </div>
      <ModifyToggle step={step} modifyBooking={modifyBooking} handleModify={handleModify}/>
    </>
  )
}

const ModifyToggle = ({step, modifyBooking, handleModify}) => {
  const [user, setUser] = useContext(UserContext);
  const booking = user.booking || {};
  const gridClass = step>2 ? "grid grid-cols-1 md:grid-cols-1 gap-4" : "grid grid-cols-1 md:grid-cols-1 gap-4"
  return(
    <div className={gridClass}>
      <div>
        {step > 0 && modifyBooking && (
          <ProductCard booking={booking} handleModify={handleModify} step={step}/>
        )}
        {step > 1 && modifyBooking && (
          <RoomsCard booking={booking} handleModify={handleModify} step={step}/>)
        }
        {step > 2 && modifyBooking && (
          <SelectionCard booking={booking} handleModify={handleModify} step={step}/>)
        }
        {step > 4 && modifyBooking && (
          <GuestsCard booking={booking} handleModify={handleModify} step={step}/>)
        }
      </div>
    </div>
  )
}

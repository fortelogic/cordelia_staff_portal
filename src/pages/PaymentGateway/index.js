import React, { Component, useContext, useState, useEffect, useRef } from 'react';
import Tile from "../../components/Tile/Tile";
import CardPaymentForm from "../../components/Booking/payment-forms/CardPaymentForm";
import BankPaymentForm from "../../components/Booking/payment-forms/BankPaymentForm";
import UPIPaymentForm from "../../components/Booking/payment-forms/UPIPaymentForm";
import WalletPayment from "../../components/Booking/payment-forms/WalletPayment";
import RadioField from "../../components/Form/RadioField";
import scrollTo from "../../utils/scrollTo";
import UserContext from "../../store/UserContext";
import moment from "moment";
import {
    useHistory,
    withRouter,
    useLocation
  } from "react-router-dom";
  import { getToken } from "../../utils/common";

class PaymentGateway extends Component {
    documentData;
    constructor(props) {
        super(props);
        this.state = {
            booking:[],
            selection: []
        }
    }

    handleSubmit = () => {

    }

    render() {
        return(
            <>
                <SummaryViewUI />
            </>
        )
    }
}

export default withRouter(PaymentGateway);

const SummaryViewUI = ({ }) => {
    let history = useHistory();
    const [user, setUser] = React.useContext(UserContext);
    const bookingMain = user.upgradeBooking;
    const bookingArr = bookingMain.rooms;
    const bookingData = user.bookingID || {};
    const [itinerary, setItinerary] = useState(JSON.parse(localStorage.getItem('itinerary')))
    const [isFailed, setFailed] = useState(
        new URLSearchParams(window.location.search).get("fail")
    );
    console.log(itinerary.bookingID)
    const [renderView, setRenderView] = useState(false)
    const [amountStatus, setAmount] = useState(0);
    const [roomData, setRoomData] = useState([]);
    const [bookingidValue, setBookingIdValue] = useState(null)
    useEffect(() => {
        // if(Object.keys(bookingData).length == 0) {
        //     console.log('dfsjhdj')
        //     history.push('/admin/bookingList')
        // } else {
        //     setRenderView(true)
        // }
        if(!itinerary.rechedule) {
            setRoomData(bookingMain.rooms)
        } else {
            setRoomData(itinerary.rooms)
            setBookingIdValue(itinerary.bookingID)
        }
        fetch(`${process.env.REACT_APP_API_ENDPOINT}/agent_portal/wallet/balance`, {
        method: "GET",
        headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization:
            `Bearer ${getToken()}`,
        },
        })
        .then((response) => response.json())
        .then((response) => {
                setAmount(response.amount) 
        })
    },[])
    return (
        <>
             <h1 className="text-xl font-bold pt-4 mr-10 ml-10"  id="payment">{isFailed ? "Request failed!" : "Payment"}</h1>
                {isFailed ? (
                    <div className="bg-j-yellow text-j-black rounded-big p-3 my-4 mr-20 ml-20">
                        <h1 className="text-base font-medium pb-0">
                            <i className="fas fa-exclamation-triangle text-2xl pr-1" />
                            Payment failed. Please try again!
                        </h1>
                        <p className="text-xs leading-relaxed">
                            Somehow, we couldn't receive your payment. Any amount deducted from
                            your account will be refunded in 7 working days.
                        </p>
                    </div>
                ) : null}
                <div className="grid lg:grid-cols-2 md:grid-cols-2 sm:grid-cols-1 gap-6 pt-0 mt-4 mr-10 ml-10">
                    <Tile tiny className="mb-5 border border-j-gray-lighter rounded-lg bg-magenta">
                        <Tile.Inner className="pb-0">
                            {/* <OrderDetail title="Booking Reference no">
                                {bookingData.number}
                            </OrderDetail> */}
                            <OrderDetail title="Ship">
                                {itinerary.itineraries.name}
                            </OrderDetail>
                            <div className="grid grid-cols-3 gap-4">
                                <div>
                                    <OrderDetail title="Departure">
                                        {moment(itinerary.itineraries.start_time).format("ddd, D MMM YYYY, h:mm a")}
                                    </OrderDetail>
                                    </div>
                                    <div>
                                    <OrderDetail title="Arrival">
                                        {moment(itinerary.itineraries.end_time).format("ddd, D MMM YYYY, h:mm a")}
                                    </OrderDetail>
                                </div>
                            </div>
                            {Object.keys(roomData)
                                .map((k, index) => 
                                
                                    <OrderDetail title={`Cabin ${index+1}`}>
                                        {roomData[index].selected == null ? roomData[index].roomCatogory:roomData[index].selected.name}<br/>
                                        Deck No: {((roomData[index].selected == null ? roomData[index].roomNumber:roomData[index].room_number).toString()).charAt(0)}<br/>
                                        Room No: {roomData[index].selected == null ? roomData[index].roomNumber:roomData[index].room_number}<br/>
                                        {roomData[index].adults+roomData[index].children+roomData[index].infants}  Guest{(roomData[index].adults+roomData[index].children+roomData[index].infants) > 1 ? "s" : null}
                                    </OrderDetail>
                                )}
                            <OrderDetail title="Amount Paid">
                                {itinerary.amount}
                            </OrderDetail>
                            {/* <OrderDetail title="Status">
                                {bookingData.status}
                            </OrderDetail>
                            <OrderDetail title="Total Amount">
                                {bookingData.total_amount}
                            </OrderDetail>
                            <OrderDetail title="Amount Paid">
                                {bookingData.amount_paid}
                            </OrderDetail>
                            <OrderDetail title="Balance amount">
                                {bookingData.pending_amount}
                            </OrderDetail> */}
                        </Tile.Inner>
                    </Tile>
                    <div>
                            <ConformBooking amount={itinerary.amount} billingID={bookingidValue}/>
                    </div>
                </div>
                </>
    )
}

const ConformBooking = ({amountValue,billingID}) => {
    const submitRef = useRef(null);
    const [amount, setAmount] = useState(null)
    const [billingData, setBillingData] = useState(null);
    const [isFailed, setFailed] = useState(
        new URLSearchParams(window.location.search).get("fail")
    );
    const setFailedError = (error) => {
        setFailed(error);
        if (error) scrollTo("payment");
    };

    console.log(billingID)

    return(
        <>
            <Tile tiny className="mb-5 border border-j-gray-lighter rounded-lg bg-magenta" >
                <Tile.Inner className="pb-0">
                <span className="text-j-gray" style={{width: "500px"}}>
                            <RadioField
                                name="paymentOption"
                                defaultValue="bank"
                                paymentStatus= {true}
                                options={[
                                    {
                                        value: "card",
                                        label: "Credit Card / Debit Card",
                                        view: (
                                          <CardPaymentForm
                                            paymentPath="booking"
                                            billingData={billingData}
                                            partialStatus={false}
                                            submitRef={submitRef}
                                            setFailed={setFailedError}
                                            walletStatus={false}
                                            paymentOptionID= {null}
                                            className="bg-black"
                                          />
                                        ),
                                    },
                                    {
                                        value: "bank",
                                        label: "Net Banking",
                                        view: (
                                          <BankPaymentForm
                                            paymentPath="booking"
                                            billingData={billingData}
                                            partialStatus={false}
                                            submitRef={submitRef}
                                            setFailed={setFailedError}
                                            walletStatus={false}
                                            paymentOptionID={null}
                                          />
                                        ),
                                      },
                                      {
                                        value: "upi",
                                        label: "UPI",
                                        view: (
                                          <UPIPaymentForm
                                            paymentPath="booking"
                                            billingData={billingData}
                                            partialStatus={false}
                                            submitRef={submitRef}
                                            setFailed={setFailedError}
                                            walletStatus={false}
                                            paymentOptionID={null}
                                          />
                                        ),
                                      },
                                      {
                                        value: "wallet",
                                        label: "Cordelia Wallet",
                                        view: (
                                          <WalletPayment
                                            amount={null}
                                            booking=""
                                            bookingID={billingID}
                                            paymentPath="wallet"
                                            billingData=""
                                            partialStatus= {false}
                                            submitRef={submitRef}
                                            setFailed={setFailedError}
                                            walletStatus={false}
                                            normalBooking= {false}
                                            totalPrice= {amountValue}
                                          />
                                        ),
                                      },
                                ]}
                            />
                        </span>
                </Tile.Inner>
            </Tile>
        </>
    )
}

const OrderDetail = ({ title, children, big }) => (
    <div className="mb-6">
      <h1 className="uppercase pb-0 text-j-gray text-tiny leading-none">
        {title}
      </h1>
      <h4
        className={
          big ? "text-j-magenta text-3xl font-bold pt-4" : "text-j-magenta pt-2 text-lg font-semibold"
        }
      >
        {children}
      </h4>
    </div>
  );
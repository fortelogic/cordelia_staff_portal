import React, { Component, useRef } from "react";
import Input from "../../components/Form/LoginInput";
import DisabledInput from "../../components/Form/DisabledInput";
import { getToken } from "../../utils/common";
import Tile from "../../components/Tile/Tile"

class AgentProfile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      profileObj: {},
    };
  }

  componentWillMount() {
    fetch(`${process.env.REACT_APP_API_ENDPOINT}/agent_portal/profile`, {
      method: "GET",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: `Bearer ${getToken()}`,
      },
    })
      .then(async (response) => {
        if (response.status === 200) {
          let data = await response.json();
          this.setState({ profileObj: data });
        } else {
          let data = await response.json();
          alert(data.message);
        }
      })
      .catch((err) => err);
  }

  render() {
    return (
      <><ProfileComp data={this.state.profileObj} onChange={this.onChange}/></>
    );
  }
}

const updateProfileAPI = (payload) => {
  let updateProfilePayload = {};
  updateProfilePayload = payload
  updateProfilePayload["travel_agency_attributes"] = {
    "address_line_1": payload["address_line_1"],
    "address_line_2": payload["address_line_2"],
    "area": payload["area"],
    "city": payload["city"],
    "district": payload["district"],
    "state": payload["state"],
    "pincode": payload["pincode"],
    "gstin": payload["gstin"],
    "owner": payload["name"],
    "phone_number": payload["phone_number"],
    "email": payload["email"],
    "portal_agent_type": payload["portal_agent_type"],
    "bank_accounts_attributes":  [{"bank_name": payload["bank_name"], "account_number": payload["account_number"], "beneficiary_name": payload["beneficiary_name"], "ifsc_code": payload["ifsc_code"], "branch_name": payload["branch_name"]}]
  }
  fetch(`${process.env.REACT_APP_API_ENDPOINT}/agent_portal/update_profile`, {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
        Authorization: `Bearer ${getToken()}`,
      },
      body: JSON.stringify(updateProfilePayload),
    })
      .then(async (response) => {
        if (response.status === 200) {
          let data = await response.json();
          alert(data.message);
        } else {
          let data = await response.json();
          alert(data.message);
        }
      })
      .catch((err) => err);
};

const ProfileComp = ({data}) => {

  let form = useRef(null);
  let profileDataVar = null;
  profileDataVar = data;
  const updateProfile = (event) => {
    event.preventDefault();
    const form_data = new FormData(form.current);
    let payload = {};
    form_data.forEach(function (value, key) {
      payload[key] = value;
    });
    updateProfileAPI(payload);
  };
  if(Object.keys(profileDataVar).length !== 0) {
    var storageProfile= JSON.parse(localStorage.getItem('UserProfile'));
    return (
        <form
          ref={form}
          onSubmit={updateProfile}
          className="w-full text-j-magenta p-10"
        >
            <h3 className="text-2xl sm:text-3xl xl:text-2xl font-bold leading-tight mb-6">
                    My Profile
            </h3>
           <div tiny className=" border border-j-gray-lighter rounded-lg bg-j-orange p-10" >
              <div className="grid lg:grid-cols-2 sm:grid-cols-1 gap-2 ">
                <div className="px-2 flex flex-col items-center justify-center">
                  <h3 className="text-2xl sm:text-3xl xl:text-2xl font-bold leading-tight">
                    {storageProfile.name}
                  </h3>
                  <div className="mt-12 w-full px-2 sm:px-6">
                    <div className="grid grid-cols-1">
                      <label className="text-white">Code</label>
                      <DisabledInput
                        id="code"
                        name="code"
                        placeholder="Code"
                        type="text"
                        value={profileDataVar.code}
                      />
                    </div>
                    <div className="grid grid-cols-2 gap-2 mt-6">
                      <div>
                        <label className="text-white">Name</label>
                        <DisabledInput
                          id="name"
                          name="name"
                          placeholder="Name"
                          type="text"
                          value={profileDataVar.name}
                        />
                      </div>
                      <div>
                        <label className="text-white">Email</label>
                        <DisabledInput
                          id="email"
                          name="email"
                          placeholder="Email"
                          type="text"
                          value={profileDataVar.email}
                        />
                      </div>
                    </div>
                    <div className="grid grid-cols-2 gap-4 mt-6">
                      <div>
                        <label className="text-white">GSTIN</label>
                        <DisabledInput
                          id="gstin"
                          name="gstin"
                          placeholder="GSTIN"
                          type="text"
                          value={profileDataVar.agency["gstin"] || "GSTIN"}
                        />
                      </div>
                      <div>
                        <label className="text-white">PAN</label>
                        <DisabledInput
                          id="pan"
                          name="pan"
                          placeholder="PAN"
                          type="text"
                          value={profileDataVar.pan}
                        />
                      </div>
                    </div>
                    <div className="grid grid-cols-2 gap-4 mt-6">
                      <div>
                        <label className="text-white">Gender</label>
                        <DisabledInput
                          id="gender"
                          name="gender"
                          placeholder="Gender"
                          type="text"
                          value={profileDataVar.gender}
                        />
                      </div>
                      <div>
                        <label className="text-white">Phone Number</label>
                        <DisabledInput
                          id="phone"
                          name="phone"
                          placeholder="Phone Number"
                          type="text"
                          value={profileDataVar.agency["phone_number"] || "Phone Number"}
                        />
                      </div>
                    </div>
                  </div>
                </div>
                <div className="mt-12 w-full px-2 sm:px-6">
                    <label className="text-white">Address Line - 1</label>
                    <Input
                        id="address_line_1"
                        name="address_line_1"
                        // placeholder="Address Line - 1"
                        type="text"
                        defaultValue={profileDataVar.agency["address_line_1"] || "Address Line 1"}
                      />
                    <div className="grid grid-cols-2 gap-4 mt-6">
                      <div>
                        <label className="text-white">Address Line - 2</label>
                        <Input
                          id="address_line_2"
                          name="address_line_2"
                          // placeholder="Address Line - 2"
                          type="text"
                          defaultValue={profileDataVar.agency["address_line_2"] || "Address Line 2"}
                          
                        />
                      </div>
                      <div>
                        <label className="text-white">Area</label>
                        <Input id="area" name="area" type="text" defaultValue={profileDataVar.agency["area"] || "Area"} />
                      </div>
                    </div>
                    <div className="grid grid-cols-2 gap-4 mt-6">
                      <div>
                        <label className="text-white">City</label>
                        <Input id="city" name="city" type="text" defaultValue={profileDataVar.agency["city"] || "City"} />
                      </div>
                      <div>
                        <label className="text-white">District</label>
                        <Input
                          id="district"
                          name="district"
                          // placeholder="District"
                          type="text"
                          defaultValue={profileDataVar.agency["district"] || "District"}
                          
                        />
                      </div>
                    </div>
                    <div className="grid grid-cols-2 gap-4 mt-6 mt-6">
                      <div>
                        <label className="text-white">State</label>
                        <Input id="state" name="state" type="text" defaultValue={profileDataVar.agency["state"] || "State"} />
                      </div>
                      <div>
                        <label className="text-white">PINCODE</label>
                        <Input
                          id="pincode"
                          name="pincode"
                          placeholder="PINCODE"
                          type="number"
                          defaultValue={profileDataVar.agency["pincode"] || "Pincode"}
                          
                        />
                      </div>
                    </div>
                    <div className="mt-6">
                        <label className="text-white">Agent type</label>
                        <select
                          name="portal_agent_type"
                          id="portal_agent_type"
                          style={{
                            width: "100%",
                            padding: "0.625rem",
                            borderRadius: "0.625rem",
                            boxShadow:
                              "0 1px 3px 0 rgba(0, 0, 0, 0.1), 0 1px 2px 0 rgba(0, 0, 0, 0.06)",
                            marginTop: "10px",
                          }}
                          defaultValue={profileDataVar.agency["portal_agent_type"] || "Portal Agent Type"}
                          
                        >
                          <option defaultValue="mice">
                            Mice
                          </option>
                          <option defaultValue="b2b">B2B</option>
                          <option defaultValue="leisure">Leisure</option>
                          <option defaultValue="consolidator">Consolidator</option>
                          <option defaultValue="weddings">Weddings</option>
                        </select>
                    </div>
                    <div className="mt-6">
                        <label className="text-white">Bank Name</label>
                        <Input
                          id="bank_name"
                          name="bank_name"
                          type="text"
                          defaultValue={profileDataVar.agency["bank_accounts"][0].bank_name || "Bank Name"}
                          
                        />
                    </div>
                    <div className="mt-6">
                        <label className="text-white">Account Number</label>
                        <Input
                          id="account_number"
                          name="account_number"
                          type="number"
                          defaultValue={profileDataVar.agency["bank_accounts"][0].account_number || "Account Number"}
                          
                        />
                    </div>
                    <div className="mt-6">
                        <label className="text-white">Beneficiary Name</label>
                        <Input
                          id="beneficiary_name"
                          name="beneficiary_name"
                          type="text"
                          defaultValue={profileDataVar.agency["bank_accounts"][0].beneficiary_name || "Beneficiary Name"}
                          
                        />
                    </div>
                    <div className="grid grid-cols-2 gap-4 mt-6">
                      <div>
                        <label className="text-white">IFSC</label>
                        <Input
                          id="ifsc_code"
                          name="ifsc_code"
                          type="text"
                          defaultValue={profileDataVar.agency["bank_accounts"][0].ifsc_code || "IFSC"}
                          
                        />
                      </div>
                      <div>
                        <label className="text-white">Branch Name</label>
                        <Input
                          id="branch_name"
                          name="branch_name"
                          placeholder="Branch Name"
                          type="text"
                          defaultValue={profileDataVar.agency["bank_accounts"][0].branch_name || "Branch Name"}
                          
                        />
                      </div>
                    </div>
                    <div className="px-2 sm:px-6 mt-6">
                      <button className="focus:outline-none w-full bg-magenta transition duration-150 ease-in-out hover:bg-gray-200 rounded text-white px-8 py-3 text-sm mt-6">
                        Update Profile
                      </button>
                    </div>
                  </div>
              </div>
            </div>
        </form>
  );
  } else {
    return(<></>)
  }
};

export default AgentProfile;

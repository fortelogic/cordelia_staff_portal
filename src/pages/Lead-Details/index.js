import React, { Component, useRef, useMemo, useState, useEffect } from "react";
import TopNavigation from "../../components/TopNavigation/index";
import LeadDetails from "../../components/Leads/LeadDetails"

export default function MyLead() {
    const [paginationstatus, setPaginationStatus] = useState(false);
    const [count, setCount] = useState(1);
    const [dataTable, setDataTable] = useState([]);
    const [loading, setLoading] = useState(true);
    const [totalPages, setTotalPages] = useState(0)
    const [notes, setNotes] =useState();

    useEffect(() => {
        bookingListApi(count)
    }, [paginationstatus]);

    const bookingListApi = () => {
        console.log('test')
    }

    const handlePageChange = () => {

    }

    return(
      <>
        <div className="h-full scroll-to-top">
              <main class=" h-full m-6">
                  <TopNavigation 
                    icon="fas fa-home text-sm text-gray-500"
                    name="Lead Details"
                  />
                  <div className="text-left mt-10">
                    <LeadDetails handlePageChange={handlePageChange}/>
                  </div>
              </main>
        </div>
      </>
    )
}

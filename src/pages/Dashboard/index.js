import React from "react";
import DashboardCard from "../../components/Cards/DashboardCard";
import TopNavigation from "../../components/TopNavigation/index";
import Leads from "../../components/Leads/leads";

export default function Dashboard() {

    let userName = window.localStorage.getItem('user');

    return (
        <>
            <div className="h-full">
                    <main class=" h-full m-6">
                        <div className="pt-2">
                            <TopNavigation 
                                icon="fas fa-home text-sm text-gray-500"
                                name="Dashboard"
                            />
                        </div>
                        <div>
                            <DashboardCard />
                        </div>
                        <div className="mt-6 mr-2">
                            <Leads />
                        </div>
                    </main>
            </div>
            
        </>
    )
}
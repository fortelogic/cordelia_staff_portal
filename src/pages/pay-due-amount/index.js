import React, { Component, useContext, useState, useEffect, useRef } from 'react';
import Tile from "../../components/Tile/Tile";
import CardPaymentForm from "../../components/Booking/payment-forms/CardPaymentForm";
import BankPaymentForm from "../../components/Booking/payment-forms/BankPaymentForm";
import UPIPaymentForm from "../../components/Booking/payment-forms/UPIPaymentForm";
import WalletPayment from "../../components/Booking/payment-forms/WalletPayment";
import RadioField from "../../components/Form/RadioField";
import scrollTo from "../../utils/scrollTo";
import UserContext from "../../store/UserContext";
import moment from "moment";
import {
    useHistory,
    withRouter,
    useLocation
  } from "react-router-dom";
  import { getToken } from "../../utils/common";

class PaymentDue extends Component {
    documentData;
    constructor(props) {
        super(props);
        this.state = {
            booking:[],
            selection: []
        }
    }

    handleSubmit = () => {

    }

    render() {
        return(
            <>
                <SummaryViewUI />
            </>
        )
    }
}

export default withRouter(PaymentDue);

const SummaryViewUI = ({ }) => {
    let history = useHistory();
    const [user, setUser] = React.useContext(UserContext);
    const bookingData = user.bookingID || {};
    const [isFailed, setFailed] = useState(
        new URLSearchParams(window.location.search).get("fail")
    );
    const [renderView, setRenderView] = useState(false)
    const [amountStatus, setAmount] = useState(0);
    useEffect(() => {
        if(Object.keys(bookingData).length == 0) {
            console.log('dfsjhdj')
            history.push('/admin/bookingList')
        } else {
            setRenderView(true)
        }
        fetch(`${process.env.REACT_APP_API_ENDPOINT}/agent_portal/wallet/balance`, {
        method: "GET",
        headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization:
            `Bearer ${getToken()}`,
        },
        })
        .then((response) => response.json())
        .then((response) => {
                setAmount(response.amount) 
        })
    },[])
    let startTime = new Date(bookingData.itinerary.startTime);
    startTime =   new Date( startTime.getTime() + ( startTime.getTimezoneOffset() * 60000 ) );
    bookingData.itinerary.startTime = startTime
    let endTime = new Date(bookingData.itinerary.endTime);
    bookingData.itinerary.endTime = endTime
    endTime =   new Date( endTime.getTime() + ( endTime.getTimezoneOffset() * 60000 ) );
    return (
        <>
            {renderView && <> <h1 className="text-xl font-bold pt-4 mr-20 ml-20"  id="payment">{isFailed ? "Request failed!" : "Payment"}</h1>
                {isFailed ? (
                    <div className="bg-j-yellow text-j-black rounded-big p-3 my-4 mr-20 ml-20">
                        <h1 className="text-base font-medium pb-0">
                            <i className="fas fa-exclamation-triangle text-2xl pr-1" />
                            Payment failed. Please try again!
                        </h1>
                        <p className="text-xs leading-relaxed">
                            Somehow, we couldn't receive your payment. Any amount deducted from
                            your account will be refunded in 7 working days.
                        </p>
                    </div>
                ) : null}
                <div className="grid lg:grid-cols-2 md:grid-cols-2 sm:grid-cols-1 gap-6 pt-0 mt-4 mr-20 ml-20">
                    <Tile tiny className="mb-5 border border-j-gray-lighter rounded-lg bg-magenta">
                        <Tile.Inner className="pb-0">
                            <OrderDetail title="Booking Reference no">
                                {bookingData.number}
                            </OrderDetail>
                            <OrderDetail title="Ship">
                                {bookingData.itinerary.name}
                            </OrderDetail>
                            <div className="grid grid-cols-3 gap-4">
                                <div>
                                    <OrderDetail title="Departure">
                                        {moment(bookingData.itinerary.start_time).format("ddd, D MMM YYYY, h:mm a")}
                                    </OrderDetail>
                                    </div>
                                    <div>
                                    <OrderDetail title="Arrival">
                                        {moment(bookingData.itinerary.end_time).format("ddd, D MMM YYYY, h:mm a")}
                                    </OrderDetail>
                                </div>
                            </div>
                            <OrderDetail title="Status">
                                {bookingData.status}
                            </OrderDetail>
                            <OrderDetail title="Total Amount">
                                {bookingData.total_amount}
                            </OrderDetail>
                            <OrderDetail title="Amount Paid">
                                {bookingData.amount_paid}
                            </OrderDetail>
                            <OrderDetail title="Balance amount">
                                {bookingData.pending_amount}
                            </OrderDetail>
                        </Tile.Inner>
                    </Tile>
                    <div>
                            <ConformBooking bookingData={bookingData} amountStatus={amountStatus}/>
                    </div>
                </div>
                </>
            }
        </>
    )
}

const ConformBooking = ({bookingData, amountStatus}) => {
    const submitRef = useRef(null);
    const [amount, setAmount] = useState(null)
    const [bookingID, setBookingID] = useState(bookingData.id)
    const [isFailed, setFailed] = useState(
        new URLSearchParams(window.location.search).get("fail")
    );
    const setFailedError = (error) => {
        setFailed(error);
        if (error) scrollTo("payment");
    };

    return(
        <>
            <Tile tiny className="mb-5 border border-j-gray-lighter rounded-lg bg-magenta" >
                <Tile.Inner className="pb-0">
                <span className="text-j-gray" style={{width: "500px"}}>
                            <RadioField
                                name="paymentOption"
                                defaultValue="bank"
                                paymentStatus= {true}
                                options={[
                                    {
                                        value: "card",
                                        label: "Credit Card / Debit Card",
                                        view: (
                                          <CardPaymentForm
                                            amount={amount}
                                            bookingID={bookingID}
                                            paymentPath="wallet"
                                            booking=""
                                            billingData=""
                                            partialStatus=""
                                            submitRef={submitRef}
                                            setFailed={setFailedError}
                                            walletStatus={false}
                                            className="bg-black"
                                          />
                                        ),
                                    },
                                    {
                                        value: "bank",
                                        label: "Net Banking",
                                        view: (
                                          <BankPaymentForm
                                            amount={amount}
                                            bookingID={bookingID}
                                            paymentPath="wallet"
                                            booking=""
                                            billingData=""
                                            partialStatus=""
                                            submitRef={submitRef}
                                            setFailed={setFailedError}
                                            walletStatus={false}
                                          />
                                        ),
                                      },
                                      {
                                        value: "upi",
                                        label: "UPI",
                                        view: (
                                          <UPIPaymentForm
                                            amount={amount}
                                            bookingID={bookingID}
                                            paymentPath="wallet"
                                            booking=""
                                            billingData=""
                                            partialStatus=""
                                            submitRef={submitRef}
                                            setFailed={setFailedError}
                                            walletStatus={false}
                                          />
                                        ),
                                      },
                                      {
                                        value: "wallet",
                                        label: "Cordelia Wallet",
                                        view: (
                                          <WalletPayment
                                            booking=""
                                            bookingID={bookingID}
                                            amount={amount}
                                            paymentPath="wallet"
                                            billingData=""
                                            partialStatus= {false}
                                            amountStatus={amountStatus}
                                            submitRef={submitRef}
                                            setFailed={setFailedError}
                                            walletStatus={false}
                                            totalPrice={bookingData.pending_amount}
                                          />
                                        ),
                                      },
                                ]}
                            />
                        </span>
                </Tile.Inner>
            </Tile>
        </>
    )
}

const OrderDetail = ({ title, children, big }) => (
    <div className="mb-6">
      <h1 className="uppercase pb-0 text-j-gray text-tiny leading-none">
        {title}
      </h1>
      <h4
        className={
          big ? "text-j-magenta text-3xl font-bold pt-4" : "text-j-magenta pt-2 text-lg font-semibold"
        }
      >
        {children}
      </h4>
    </div>
  );
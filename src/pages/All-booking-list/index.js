import React, { Component, useRef, useMemo, useState, useEffect } from "react";
import { Link } from "react-router-dom";
import Header from "../../components/Header/header";
import Footer from "../../components/Footer/footer";
import Table from "../../components/Table/table";
import { useHistory } from "react-router-dom";
import { getToken } from "../../utils/common";
import ReactTable from "../../components/Table/reactTable";
import ReactTableComponent from "../../components/Table/ReactTablecomponent";
import TopNavigation from "../../components/TopNavigation/index";
import Search from "../../components/Search/search";
import Filter from "../../components/Filter/filter";
import moment from "moment";
import { extendMoment } from "moment-range";

export default function AllbookingList() {
    const [filterStatus, setFilterStatus] = useState(false);
    const [searchValue, setSearchValue] = useState();
    const [cruiceDate, setCruiceDate] = useState();
    const [bookingSource, setBookingSource] = useState();
    const [paginationstatus, setPaginationStatus] = useState(false);
    const [count, setCount] = useState(1);
    const [dataTable, setDataTable] = useState([]);
    const [loading, setLoading] = useState(false);
    const [totalPages, setTotalPages] = useState(0)
    const [clearStatus, setClearStatus] = useState(true);
    const [page, setPage] = useState(1);
    let history = useHistory();

    const momentValue = extendMoment(moment);
    const today = momentValue();
    const [days, setDays] = useState();

    const columns = React.useMemo(
      () => [
        {
          Header: () => null,
          id: 'expander',
          Cell: ({ row }) => (
            <span {...row.getToggleRowExpandedProps()}>
              {row.isExpanded ? <i className="fas fa-sort-up"></i> : <i className="fas fa-caret-down"></i>}
            </span>
          ),
        },
        {
          Header: 'Ref Number',
          accessor: 'number',
        },
        {
          Header: 'Cruise name',
          accessor: 'name',
          maxWidth: 150,
          Cell: (props) => {
            return <p style={{width:'150px', wordBreak:'break-word'}}>{props.value}</p>
          },
          style: { width:'100px' }
        },
        {
          Header: 'Booking Date',
          accessor: 'booked_date',
        },
        {
          Header: 'Status',
          accessor: 'status', 
          Cell: (props) => {
            return (<>
              {props.value=="RESERVED" &&  <>
                <i className="fas fa-circle text-orange-500 mr-2"></i>Reserved
              </>}
              {props.value=="CONFIRMED" &&  <>
                <i className="fas fa-circle text-green-500 mr-2"></i>Confirmed
              </>}
              {props.value=="CANCELLED" &&  <>
                <i className="fas fa-circle text-red-500 mr-2"></i>Cancelled
              </>}
              {props.value=="MODIFIED" &&  <>
                <i className="fas fa-circle text-blue-500 mr-2"></i>Modified
              </>}
            </>)
          },
        },
        {
          Header: "",
          accessor: "id",
          Cell: (props) => {
            return (
              <div>
                  <button className="bg-j-orange hover:bg-magenta text-white p-2 rounded-xl text-xs" onClick={()=>{
                      handleDetails(props.value)
                  }}>Details<span className="pl-1"><i className="fas fa-arrow-circle-right"></i></span></button>
              </div>
            );
          },
        },
      ],
      []
    )

  const handleDetails = (id) => {
      // history.push('/admin/lead-details')
      history.push({
        pathname: '/admin/booking-details',
        state: { id: id }
      })
  }

  const renderRowSubComponent = ({ i }) => {
        return(
          <div
            style={{
              fontSize: '14px',
            }}
          >
            {/* <code>{JSON.stringify({ values: tableData[i] }, null, 2)}</code> */}
              <div className="grid grid-cols-3 gap-4 pl-20 pt-4 pr-14 pb-4">
                <p className="text-grey-700 border-b pb-4"><i className="fas fa-user-alt"></i> <span className="pl-2">{dataTable[i].contact_name}</span></p>
                <p className="text-grey-700 border-b pb-4"><i className="fas fa-envelope"></i> <span className="pl-2">{dataTable[i].contact_email}</span></p>
                <p className="text-grey-700 border-b pb-4"><i className="fas fa-phone-square-alt"></i><span className="pl-2">{dataTable[i].contact_phone}</span></p>
                <p className="text-grey-700 border-b pb-4">Source of Booking: <span className="font-bold">{dataTable[i].booked_source}</span></p>
                <p className="text-grey-700 border-b pb-4">Amount Paid: <span className="font-bold text-red-500">&#x20B9;{dataTable[i].amount_paid}</span></p>
                <p className="text-grey-700 border-b pb-4">Total Guest: <span className="font-bold">{dataTable[i].total_guest}</span></p>
              </div>
          </div>
        )
  }

  const data= dataTable;

    const ScrollToTopOnMount = () => {
      console.log('test')
      useEffect(() => {
        window.scrollTo(0, 0);
      }, []);
    
      return null;
    }

    const bookingListApi = (count) => {
        setLoading(true)
        var params ='&';
        if(searchValue!=null && searchValue != undefined && searchValue!="") {
          params +='search='+searchValue+'&'
        } 
        if(!clearStatus) {
          var start_date = days.start.format("YYYY-MM-DD");
          var to_date = days.end.format("YYYY-MM-DD");
          params +='from_date='+start_date+'&'
          params +='to_date='+to_date+'&'
        }
        if(cruiceDate!="" && cruiceDate != undefined && cruiceDate!=null) {
          params +='cruise_date='+cruiceDate+'&'
        }
        
        console.log(bookingSource)
        if(bookingSource!="" && bookingSource != undefined && bookingSource!=null) {
          params +='created_via_filter='+bookingSource+'&'
        }
        fetch(process.env.REACT_APP_API_ENDPOINT+'/staff_portal/bookings?page='+count+params, {
          method: 'GET',
          headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization:
            `Bearer ${getToken()}`,
          },
        })
        .then((response) => { 
            if(response.ok){
               return response.json()
            }
            throw Error("Verification failed");
           }
          )
        .then((response) => {
          console.log(response)
          var dummySetdata = [];
          console.log(response.bookings);
            // if(moreData) {
            //   var responseData= dummySetdata.concat(response.bookings)
            // } else {
            //   var responseData= response.bookings;
            // }
        //   console.log()
        var responseData= response.bookings;
           responseData.map((data) => {
            dummySetdata.push(
              {
                "number": data.number,
                "name": data.itinerary.name,
                "booked_date": data.booked_on,
                "status": data.status,
                "booked_source": data.created_via,
                "id": data.id,
                "amount_paid": data.amount_paid,
                "total_guest": data.total_guests,
                "contact_name": data.contact.name,
                "contact_email": data.contact.email,
                "contact_phone": data.contact.phone_number,
                // "subRows": [{
                //   "booked_source": data.created_via,
                // }]
              }
            )
          })
          setTotalPages(response.pagination.total_pages);
          console.log(dummySetdata)
          setDataTable(dummySetdata);
          setLoading(false);
          // setSubmitted(false);
          // modalClose()
  
        }).catch((err) =>  console.log(err));
    }

    const handleFilterStatus = () => {
      setFilterStatus(!filterStatus);
    }

    const onChangeSearch = (e) => {
      console.log(e.target.value)
      setSearchValue(e.target.value)
    }

    const handleSearch = () => {
      console.log(searchValue)
      if(searchValue == "" || searchValue == undefined || searchValue == null) {

      } else {
        bookingListApi(1)
      }
    }

    const onChangeFilterbyDate = (value) => {
      setClearStatus(false)
      setDays(value)
    }

    const handleFilterByDate = () => {
      console.log(days)
      if(!clearStatus) {
        bookingListApi(1)
      }
    }

    const onChangeFilterbyCruiceDate = (e) => {
      setCruiceDate(e.target.value)
    }

    const handleFilterByCruiceDate = () => {
      if(cruiceDate!="" && cruiceDate != undefined && cruiceDate!=null) {
        bookingListApi(1)
      }
    }

    const onChangeFilterbySource = (e) => {
      setBookingSource(e.target.value)
    }

    const handleFilterBySource = () => {
      console.log(bookingSource)
      if(bookingSource!="" && bookingSource != undefined && bookingSource!=null) {
        bookingListApi(1)
      }
    }

    const handleClearByDate = () => {
      console.log('hai')
      setClearStatus(true)
      setDays(moment.range(today.clone(), today.clone()))
    }

    const handlePageChange = (event, value) => {
      console.log(value)
        setPage(value);
        bookingListApi(value);
    };
    
    
    return(
      <>
        <div className="h-full scroll-to-top">
          <main class=" h-full m-6">
            <TopNavigation 
              icon="fas fa-home text-sm text-gray-500"
              name="All Bookings"
            />
                <div className="text-left mt-4">
                    <div className="flex ml-14">
                        <div className="w-10/12">
                            <Search 
                              onChangeSearch={onChangeSearch}
                              handleSearch={handleSearch}
                              defaultValue=""
                              placeHolder="Try port name or reference number"
                            />
                        </div>
                        <div className="w-2/12">
                          <i className="fas fa-filter text-2xl pt-14 text-center pl-4 cursor-pointer text-j-magenta hover:text-j-orange" onClick={()=>handleFilterStatus()}></i>
                        </div>
                    </div>
                    <div>
                        {filterStatus && 
                          <Filter 
                            onChangeFilterbyDate={onChangeFilterbyDate}
                            days={days}
                            handleFilterByDate={handleFilterByDate}
                            onChangeFilterbyCruiceDate={onChangeFilterbyCruiceDate}
                            handleFilterByCruiceDate={handleFilterByCruiceDate}
                            onChangeFilterbySource={onChangeFilterbySource}
                            handleFilterBySource={handleFilterBySource}
                            handleClearByDate={handleClearByDate}
                          /> 
                        }
                    </div>
                    <div>
                        <ReactTableComponent 
                            columns={columns}
                            data={data}
                            loading={loading}
                            renderRowSubComponent={renderRowSubComponent}
                            count={totalPages}
                            page={page}
                            handlePageChange={handlePageChange}
                        />
                    </div>
                </div>
              
                      {/* <ReactTable 
                          tableData={dataTable}
                          loading={loading}
                          renderRowSubComponent={renderRowSubComponent}
                          totalPages={totalPages}
                          bookingListApi={bookingListApi}
                      /> */}
            </main>
        </div>
      </>
    )
}
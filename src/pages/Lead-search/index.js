import React, { Component, useRef, useMemo, useState, useEffect } from "react";
import TopNavigation from "../../components/TopNavigation/index";
import Search from "../../components/Search/search";
import Filter from "../../components/Filter/filter";
import ReactTableComponent from "../../components/Table/ReactTablecomponent";
import {Link, useLocation, useHistory} from "react-router-dom";
import { getToken } from "../../utils/common";

export default function LeadSearch() {

    let location = useLocation();
    let history = useHistory();
    const [searchValue, setSearchValue] = useState(location.state.number);
    const [loading, setLoading] = useState(false); 
    const [dataTable, setDataTable] = useState([]); 

    const columns = React.useMemo(
        () => [
          {
            Header: () => null,
            id: 'expander',
            Cell: ({ row }) => (
              <span {...row.getToggleRowExpandedProps()}>
                {row.isExpanded ? <i className="fas fa-sort-up"></i> : <i className="fas fa-caret-down"></i>}
              </span>
            ),
          },
          {
            Header: 'Ref Number',
            accessor: 'number',
          },
          {
            Header: 'Cruice name',
            accessor: 'name',
            maxWidth: 150,
            Cell: (props) => {
              return <p style={{width:'150px', wordBreak:'break-word'}}>{props.value}</p>
            },
            style: { width:'100px' }
          },
          {
            Header: 'Booking Date',
            accessor: 'booked_date',
          },
          {
            Header: 'Status',
            accessor: 'status', 
            Cell: (props) => {
              return (<>
                {props.value=="RESERVED" &&  <>
                  <i className="fas fa-circle text-orange-500 mr-2"></i>Reserved
                </>}
                {props.value=="CONFIRMED" &&  <>
                  <i className="fas fa-circle text-green-500 mr-2"></i>Confirmed
                </>}
                {props.value=="CANCELLED" &&  <>
                  <i className="fas fa-circle text-red-500 mr-2"></i>Cancelled
                </>}
                {props.value=="MODIFIED" &&  <>
                  <i className="fas fa-circle text-blue-500 mr-2"></i>Modified
                </>}
                {props.value=="ON_HOLD" &&  <>
                  <i className="fas fa-circle text-gray-500 mr-2"></i>On Hold
                </>}
              </>)
            },
          },
          {
            Header: "",
            accessor: "id",
            Cell: (props) => {
              return (
                <div>
                    <button className="bg-j-orange hover:bg-magenta text-white p-2 rounded-xl text-xs" onClick={()=>{
                        handleDetails(props.value)
                    }}>Details<span className="pl-1"><i className="fas fa-arrow-circle-right"></i></span></button>
                </div>
              );
            },
          },
        ],
        []
    )

    const handleDetails = (id) => {
        //history.push('/admin/lead-details')
        history.push({
          pathname: '/admin/booking-details',
          state: { id: id }
        })
    }

    const renderRowSubComponent = ({ i }) => {
      return(
        <div
          style={{
            fontSize: '14px',
          }}
        >
          {/* <code>{JSON.stringify({ values: tableData[i] }, null, 2)}</code> */}
            <div className="grid grid-cols-3 gap-4 pl-20 pt-4 pr-14 pb-4">
              <p className="text-grey-700 border-b pb-4"><i className="fas fa-user-alt"></i> <span className="pl-2">{dataTable[i].contact_name}</span></p>
              <p className="text-grey-700 border-b pb-4"><i className="fas fa-envelope"></i> <span className="pl-2">{dataTable[i].contact_email}</span></p>
              <p className="text-grey-700 border-b pb-4"><i className="fas fa-phone-square-alt"></i><span className="pl-2">{dataTable[i].contact_phone}</span></p>
              <p className="text-grey-700 border-b pb-4">Source of Booking: <span className="font-bold">{dataTable[i].booked_source}</span></p>
              <p className="text-grey-700 border-b pb-4">Amount Paid: <span className="font-bold text-red-500">&#x20B9;{dataTable[i].amount_paid}</span></p>
              <p className="text-grey-700 border-b pb-4">Total Guest: <span className="font-bold">{dataTable[i].total_guest}</span></p>
            </div>
        </div>
      )
    }

    const data= dataTable;

    const handlePageChange = () => {

    }

    const onChangeSearch = (e) => {
        console.log(e.target.value)
        setSearchValue(e.target.value)
    }
  
    const handleSearch = () => {
        console.log(searchValue)
        if(searchValue == "" || searchValue == undefined || searchValue == null) {
  
        } else {
          bookingListApi(1)
        }
    }

    const bookingListApi = (count) => {
        setLoading(true)
        var params ='&';
        if(searchValue!=null && searchValue != undefined && searchValue!="") {
          params = searchValue;
        }
        fetch(process.env.REACT_APP_API_ENDPOINT+'/staff_portal/enquiries/bookings?number='+params, {
            method: 'GET',
            headers: {
              Accept: "application/json",
              "Content-Type": "application/json",
              Authorization:
              `Bearer ${getToken()}`,
            },
          })
          .then((response) => { 
              if(response.ok){
                 return response.json()
              }
              throw Error("Verification failed");
             }
            )
          .then((response) => {
            console.log(response)
            var dummySetdata = [];
            console.log(response.bookings);
              // if(moreData) {
              //   var responseData= dummySetdata.concat(response.bookings)
              // } else {
              //   var responseData= response.bookings;
              // }
          //   console.log()
          var responseData= response.bookings;
             responseData.map((data) => {
              dummySetdata.push(
                {
                  "number": data.number,
                  "name": data.itinerary.name,
                  "booked_date": data.booked_on,
                  "status": data.status,
                  "booked_source": data.created_via,
                  "id": data.id,
                  "amount_paid": data.amount_paid,
                  "total_guest": data.total_guests,
                  "contact_name": data.contact.name,
                  "contact_email": data.contact.email,
                  "contact_phone": data.contact.phone_number,
                  // "subRows": [{
                  //   "booked_source": data.created_via,
                  // }]
                }
              )
            })

            setDataTable(dummySetdata);
          })
    }

    return(
        <>
            <div className="h-full scroll-to-top">
                <main class=" h-full m-6">
                    <TopNavigation 
                        icon="fas fa-home text-sm text-gray-500"
                        name="Lead Search"
                    />
                    <div className="text-left mt-4">
                        <div className="flex ml-14">
                            <div className="w-11/12">
                                <Search 
                                    onChangeSearch={onChangeSearch}
                                    handleSearch={handleSearch}
                                    defaultValue={location.state.number}
                                />
                            </div>
                            {/* <div className="w-2/12">
                                <i className="fas fa-filter text-2xl pt-14 text-center pl-4 cursor-pointer text-j-magenta"></i>
                            </div> */}
                        </div>
                        {/* <div>
                            <Filter />
                        </div> */}
                        <div>
                            <ReactTableComponent 
                                columns={columns}
                                data={data}
                                loading={false}
                                renderRowSubComponent={renderRowSubComponent}
                                count={1}
                                page={1}
                                handlePageChange={handlePageChange}
                            />
                        </div>
                    </div>
                </main>
            </div>
        </>
    )
}
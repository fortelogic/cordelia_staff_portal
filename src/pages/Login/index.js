import React, { Component, useRef } from "react";
import { useHistory } from "react-router-dom";
import styles from "./login.module.css";
import LoginImage from "../../assets/images/login-banner.jpg";
import Input from "../../components/Form/LoginInput";
import { setUserSession } from "../../utils/common";
import LogoImage from "../../assets/images/logo.png";

class Login extends Component {
  state = {
    step: 1,
    isChecked: false,
    email: "",
  };

  componentDidMount() {
    const username = localStorage.getItem("username");
    const checkbox = localStorage.getItem("checkbox");
    if (checkbox && username !== "") {
      this.setState({
        isChecked: true,
        email: localStorage.username,
      });
    }
  }

  onChangeCheckbox = (event) => {
    this.setState({
      isChecked: event.target.checked,
    });
  };

  onChangeValue = (event) => {
    this.setState({
      [event.target.name]: event.target.value,
    });
  };

  onClick = (val) => {
    this.setState({
      step: val,
    });
  };

  switchState = () => {
    const { step } = this.state;
    switch (step) {
      case 1:
        return (
          <LoginForm
            onClick={this.onClick}
            onChangeCheckbox={this.onChangeCheckbox}
            onState={this.state}
            onChangeValue={this.onChangeValue}
          />
        );
      // case 2:
      //   return <CheckGSTINComp onClick={this.onClick} />;
      // case 3:
      //   return <CheckPANComp onClick={this.onClick} />;
      // case 4:
      //   return <VerifyGSTDetails onClick={this.onClick} />;
      // case 5:
      //   return <StartSignUpComp onClick={this.onClick} />;
      // case 6:
      //   return <AddressComp onClick={this.onClick} />;
      // case 7:
      //   return <VerifySignUpComp onClick={this.onClick} />;
      // case 8:
      //   return <CheckForgetPasswordComp onClick={this.onClick} />;
      // case 9:
      //   return <VerifyForgetPasswordComp onClick={this.onClick} />;
    }
  };

  render() {
    return <>{this.switchState()}</>;
  }
}

var verifyGSTINDataVar = {};

const LoginApi = (payload, history) => {
  fetch(`${process.env.REACT_APP_API_ENDPOINT}/staff_portal/login`, {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
    body: JSON.stringify(payload),
  })
    .then(async (response) => {
      if (response.status === 200) {
        let data = await response.json();
        setUserSession(data.token, payload.email, data.exp, data.name);
        localStorage.setItem('UserProfile', JSON.stringify(data.profile));
        history.push("/admin/dashboard");
      } else {
        let data = await response.json();
        if(response.status === 401)
        {
          alert('Incorrect Username or Password');
        } else {
          alert(data.message);
        }
      }
    })
    .catch((err) => err);
};

const gstinAPI = (payload, onClick) => {
  verifyGSTINDataVar = {};
  let obj1 = {};
  obj1.gstin = payload.gstin;
  fetch(`${process.env.REACT_APP_API_ENDPOINT}/agent_portal/verify_gstin`, {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
    body: JSON.stringify(obj1),
  })
    .then(async (response) => {
      if (response.status === 200) {
        let data = await response.json();
        verifyGSTINDataVar = data;
        verifyGSTINDataVar["pan_flag"] = false;
        onClick(4);
      } else {
        let data = await response.json();
        alert(data.message);
      }
    })
    .catch((err) => err);
};

const panAPI = (payload, onClick) => {
  verifyGSTINDataVar = {};
  verifyGSTINDataVar["pan"] = payload.pan;
  fetch(`${process.env.REACT_APP_API_ENDPOINT}/agent_portal/verify_pan`, {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
    body: JSON.stringify(payload),
  })
    .then(async (response) => {
      if (response.status === 200) {
        let data = await response.json();
        verifyGSTINDataVar["pan_status"] = data.status;
        verifyGSTINDataVar["pan_flag"] = true;
        onClick(4);
      } else {
        let data = await response.json();
        alert(data.message);
      }
    })
    .catch((err) => err);
};

const startSignUpAPI = (payload, onClick) => {
  payload.gstin = verifyGSTINDataVar.gstin || null;
  payload.pan = verifyGSTINDataVar["pan"];
  verifyGSTINDataVar.email = payload.email;
  verifyGSTINDataVar.phone_number = payload.phone_number;
  verifyGSTINDataVar.name = payload.name;
  verifyGSTINDataVar.password = payload.password;
  onClick(6);
};
const AddressCompAPI = (payload, onClick) => {
  if(verifyGSTINDataVar["pan_flag"]) {
    verifyGSTINDataVar["travel_agency_attributes"] = {
      "address_line_1": payload["address_line_1"],
      "address_line_2": payload["address_line_2"],
      "area": payload["area"],
      "city": payload["city"],
      "district": payload["district"],
      "state": payload["state"],
      "pincode": payload["pincode"],
      "owner": verifyGSTINDataVar["name"],
      "phone_number": verifyGSTINDataVar["phone_number"],
      "email": verifyGSTINDataVar["email"],
      "portal_agent_type": payload["portal_agent_type"],
      "commission_model": payload["commission_model"],
      "bank_accounts_attributes":  [{"bank_name": payload["bank_name"], "account_number": payload["account_number"], "beneficiary_name": payload["beneficiary_name"], "ifsc_code": payload["ifsc_code"], "branch_name": payload["branch_name"]}]
    }
  } else {
    verifyGSTINDataVar["travel_agency_attributes"] = {
      "address_line_1": verifyGSTINDataVar["address_line_1"],
      "address_line_2": verifyGSTINDataVar["address_line_2"],
      "area": verifyGSTINDataVar["area"],
      "city": verifyGSTINDataVar["city"],
      "district": verifyGSTINDataVar["district"],
      "state": verifyGSTINDataVar["state"],
      "pincode": verifyGSTINDataVar["pincode"],
      "owner": verifyGSTINDataVar["name"],
      "phone_number": verifyGSTINDataVar["phone_number"],
      "email": verifyGSTINDataVar["email"],
      "portal_agent_type": payload["portal_agent_type"],
      "commission_model": payload["commission_model"],
      "bank_accounts_attributes":  [{"bank_name": payload["bank_name"], "account_number": payload["account_number"], "beneficiary_name": payload["beneficiary_name"], "ifsc_code": payload["ifsc_code"], "branch_name": payload["branch_name"]}]
    }
  }
  fetch(`${process.env.REACT_APP_API_ENDPOINT}/agent_portal/register`, {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
    body: JSON.stringify(verifyGSTINDataVar),
  })
    .then(async (response) => {
      if (response.status === 200) {
        let data = await response.json();
        if(data) {
          onClick(7);
        }
      } else {
        let data = await response.json();
        alert(data.message);
      }
    })
    .catch((err) => err);
};

const verifySignUpAPI = (payload, onClick) => {
  payload.email = verifyGSTINDataVar.email;
  payload.phone_number = verifyGSTINDataVar.phone_number;
  fetch(`${process.env.REACT_APP_API_ENDPOINT}/agent_portal/verify`, {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
    body: JSON.stringify(payload),
  })
    .then(async (response) => {
      if (response.status === 200) {
        onClick(1);
      } else {
        let data = await response.json();
        alert(data.message);
      }
    })
    .catch((err) => err);
};

const startForgetPasswordAPI = (payload, onClick) => {
  verifyGSTINDataVar = {};
  verifyGSTINDataVar.email = payload.email;
  fetch(`${process.env.REACT_APP_API_ENDPOINT}/agent_portal/reset_password`, {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
    body: JSON.stringify(payload),
  })
    .then(async (response) => {
      if (response.status === 200) {
        onClick(9);
      } else {
        let data = await response.json();
        alert(data.message);
      }
    })
    .catch((err) => err);
};

const verifyForgetPasswordAPI = (payload, onClick) => {
  payload.email = verifyGSTINDataVar.email;
  fetch(`${process.env.REACT_APP_API_ENDPOINT}/agent_portal/verify_reset_password`, {
    method: "POST",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
    },
    body: JSON.stringify(payload),
  })
    .then(async (response) => {
      if (response.status === 200) {
        onClick(1);
      } else {
        let data = await response.json();
        alert(data.message);
      }
    })
    .catch((err) => err);
};

const LoginForm = ({ onClick, onChangeCheckbox, onState, onChangeValue }) => {
  let form = useRef(null);
  let history = useHistory();
  const { email, password, isChecked } = onState;

  const handleLoginSubmit = (event) => {
    if (isChecked && email !== "") {
      localStorage.setItem("username", email);
      localStorage.setItem("checkbox", isChecked);
    }
    event.preventDefault();
    const form_data = new FormData(form.current);
    let payload = {};
    form_data.forEach(function (value, key) {
      payload[key] = value;
    });
    LoginApi(payload, history);
  };
  return (
    <section className="grid grid-cols-1 sm:grid-cols-1 md:grid-cols-2 lg:grid-cols-2 gap-1 overflow-auto h-full">
      <div className="h-full hidden sm:block overflow-auto">
        <img src={LoginImage} className={styles.signinsignup} />
      </div>
      <div className="bg-gray-100 {-- h-screen --} flex justify-center lg:items-center h-full overflow-auto">
        <form
          ref={form}
          onSubmit={handleLoginSubmit}
          className="w-full text-j-magenta pt-2 pl-20 pr-20 pb-2"
        >
          <div className="pt-0 px-2 flex flex-col items-center justify-center">
            <img src={LogoImage } className="h-32 text-center"/>
          </div>
          <div className="pt-4 px-2 flex flex-col items-center justify-center">
            <h3 className="text-2xl sm:text-2xl xl:text-2xl font-bold leading-tight">
              Login To Your Account
            </h3>
          </div>
          <div className="mt-8 w-full px-2 sm:px-6">
            <div>
              <Input
                id="email"
                name="email"
                placeholder="Email"
                type="email"
                value={email}
                onChangeValue={onChangeValue}
              />
            </div>
            <div className="mt-6">
              <Input
                id="password"
                name="password"
                placeholder="Password"
                type="password"
              />
            </div>
          </div>
          <div className="pt-6 w-full flex justify-between px-2 sm:px-6 cursor-pointer">
            <div className="flex items-center">
              <input
                id="rememberme"
                className="w-3 h-3 mr-2"
                type="checkbox"
                checked={isChecked}
                onChange={onChangeCheckbox}
              />
              <label htmlFor="rememberme" className="text-xs">
                Remember Me
              </label>
            </div>
            {/* <a
              className="text-xs"
              onClick={() => {
                onClick(8);
              }}
            >
              Forgot Password?
            </a> */}
          </div>
          <div className="px-2 sm:px-6">
            <button className="focus:outline-none w-full bg-magenta transition duration-150 ease-in-out hover:bg-j-orange rounded text-white px-8 py-3 text-sm mt-6">
              Login
            </button>
            {/* <p className="mt-8 text-xs text-center cursor-pointer">
              Don’t Have An Account?{" "}
              <a
                className="underline"
                onClick={() => {
                  onClick(2);
                }}
              >
                Sign Up
              </a>
            </p>
            <p className="mt-6 text-xs text-center">
              <a>
                <sup>*</sup> To Sign - Up you will need a GSTIN/PAN
              </a>
            </p> */}
          </div>
          <div className="flex grid grid-cols-6 text-center gap-4 justify-center mt-4">
            <a
              className="cursor-pointer text-3xl"
              href="https://www.linkedin.com/company/cordelia-cruises/mycompany/?viewAsMember=true"
              target="_blank"
            >
              <i className="fab fa-linkedin"></i>
            </a>
            <a
              href="https://www.facebook.com/cordeliacruises/"
              target="_blank"
              className="cursor-pointer text-3xl"
            >
              <i className="fab fa-facebook-square"></i>
            </a>
            <a
              href="https://www.instagram.com/cordeliacruisesindia/?hl=en"
              target="_blank"
              className="cursor-pointer text-3xl"
            >
              <i className="fab fa-instagram-square"></i>
            </a>
            <a
              href="https://twitter.com/CordeliaCruises"
              target="_blank"
              className="cursor-pointer text-3xl"
            >
              <i className="fab fa-twitter-square"></i>
            </a>
            <a
              href="https://blog.cordeliacruises.com/"
              target="_blank"
              className="cursor-pointer text-3xl"
            >
              <i className="fab fa-blogger"></i>
            </a>
            <a
              href="https://www.youtube.com/channel/UCIGZzyqWsbCH1-VNFsXrY9g"
              target="_blank"
              className="cursor-pointer text-3xl"
            >
              <i className="fab fa-youtube-square"></i>
            </a>
          </div>
        </form>
      </div>
    </section>
  );
};

const CheckGSTINComp = ({ onClick }) => {
  let form = useRef(null);
  const checkGSTIN = (event) => {
    event.preventDefault();
    const form_data = new FormData(form.current);
    let payload = {};
    form_data.forEach(function (value, key) {
      payload[key] = value;
    });
    gstinAPI(payload, onClick);
  };
  return (
    <section className="grid grid-cols-1 sm:grid-cols-1 md:grid-cols-2 lg:grid-cols-2 gap-1 overflow-auto h-full">
      <div className="h-full hidden sm:block overflow-auto">
        <img src={LoginImage} className={styles.signinsignup} />
      </div>
      <div className="bg-gray-100 {-- h-screen --} flex justify-center lg:items-center h-full overflow-auto">
        <form
          ref={form}
          onSubmit={checkGSTIN}
          className="w-full text-j-magenta p-20"
        >
          <div className="pt-0 px-2 flex flex-col items-center justify-center">
            <img src={LogoImage} className="h-48 text-center" />
          </div>
          <div className="pt-8 px-2 flex flex-col items-center justify-center">
            <h3 className="text-2xl sm:text-3xl xl:text-2xl font-bold leading-tight">
              Sign - Up - Step - 1
            </h3>
          </div>
          <div className="mt-12 w-full px-2 sm:px-6">
            <Input id="gstin" name="gstin" placeholder="GSTIN" type="text" />
          </div>
          <div className="px-2 sm:px-6 cursor-pointer cursor-pointer">
            <p className="mt-16 text-xs text-center">
              <a
                className="underline"
                onClick={() => {
                  onClick(3);
                }}
              >
                Continue to Sign - Up with just PAN
              </a>
            </p>
          </div>
          <div className="px-2 sm:px-6">
            <button className="focus:outline-none w-full bg-magenta transition duration-150 ease-in-out hover:bg-j-orange rounded text-white px-8 py-3 text-sm mt-6">
              Check GSTIN
            </button>
            <p className="mt-16 text-xs text-center cursor-pointer">
              <a
                className="underline"
                onClick={() => {
                  onClick(1);
                }}
              >
                Login
              </a>
            </p>
          </div>
        </form>
      </div>
    </section>
  );
};

const CheckPANComp = ({ onClick }) => {
  let form = useRef(null);
  const checkPAN = (event) => {
    event.preventDefault();
    const form_data = new FormData(form.current);
    let payload = {};
    form_data.forEach(function (value, key) {
      payload[key] = value;
    });
    panAPI(payload, onClick);
  };
  return (
    <section className="grid grid-cols-1 sm:grid-cols-1 md:grid-cols-2 lg:grid-cols-2 gap-1 overflow-auto h-full">
      <div className="h-full hidden sm:block overflow-auto">
        <img src={LoginImage} className={styles.signinsignup} />
      </div>
      <div className="bg-gray-100 {-- h-screen --} flex justify-center lg:items-center h-full overflow-auto">
        <form
          ref={form}
          onSubmit={checkPAN}
          className="w-full text-j-magenta p-20"
        >
          <div className="pt-0 px-2 flex flex-col items-center justify-center">
            <img src={LogoImage} className="h-48 text-center" />
          </div>
          <div className="pt-8 px-2 flex flex-col items-center justify-center">
            <h3 className="text-2xl sm:text-3xl xl:text-2xl font-bold leading-tight">
              Sign - Up - Step - 1
            </h3>
          </div>
          <div className="mt-12 w-full px-2 sm:px-6">
            <Input id="pan" name="pan" placeholder="PAN" type="text" />
          </div>
          <div className="px-2 sm:px-6 cursor-pointer">
            <p className="mt-16 text-xs text-center">
              <a
                className="underline"
                onClick={() => {
                  onClick(2);
                }}
              >
                Continue to Sign - Up with GSTIN
              </a>
            </p>
          </div>
          <div className="px-2 sm:px-6">
            <button className="focus:outline-none w-full bg-magenta transition duration-150 ease-in-out hover:bg-j-orange rounded text-white px-8 py-3 text-sm mt-6">
              Check PAN
            </button>
            <p className="mt-16 text-xs text-center cursor-pointer">
              <a
                className="underline"
                onClick={() => {
                  onClick(1);
                }}
              >
                Login
              </a>
            </p>
          </div>
        </form>
      </div>
    </section>
  );
};

const VerifyGSTDetails = ({ onClick }) => {
  let verificationData = verifyGSTINDataVar;
  let form = useRef(null);
  const moveToSignUp = (event) => {
    event.preventDefault();
    onClick(5);
  };
  return (
    <section className="grid grid-cols-1 sm:grid-cols-1 md:grid-cols-2 lg:grid-cols-2 gap-1 overflow-auto h-full">
      <div className="h-full hidden sm:block overflow-auto">
        <img src={LoginImage} className={styles.signinsignup} />
      </div>
      <div className="bg-gray-100 {-- h-screen --} flex justify-center lg:items-center h-full overflow-auto">
        <form
          ref={form}
          onSubmit={moveToSignUp}
          className="w-full text-j-magenta p-20"
        >
          <div className="pt-0 px-2 flex flex-col items-center justify-center">
            <img src={LogoImage} className="h-48 text-center" />
          </div>
          <div className="pt-8 px-2 flex flex-col items-center justify-center">
            <h3 className="text-2xl sm:text-3xl xl:text-2xl font-bold leading-tight">
              Sign - Up - Step - 2
            </h3>
          </div>
          {verificationData && (
            <div>
              <div className="mt-12 w-full px-2 sm:px-6">
                {verificationData["id"] && (
                  <div className="grid grid-cols-2">
                    <div className="grid grid-cols-2">
                      <span>ID</span>
                      <span>:</span>
                    </div>
                    <span>{verificationData["id"].toUpperCase()}</span>
                  </div>
                )}
                {verificationData["gstin"] && (
                  <div className="grid grid-cols-2">
                    <div className="grid grid-cols-2">
                      <span>GSTIN</span>
                      <span>:</span>
                    </div>
                    <span>{verificationData["gstin"].toUpperCase()}</span>
                  </div>
                )}
                {verificationData["pan"] && (
                  <div className="grid grid-cols-2">
                    <div className="grid grid-cols-2">
                      <span>REGD. PAN</span>
                      <span>:</span>
                    </div>
                    <span>{verificationData["pan"].toUpperCase()}</span>
                  </div>
                )}
                <div
                  className="grid grid-cols-2"
                  style={{
                    paddingBottom: "2rem",
                    borderBottom: "1px dashed",
                  }}
                >
                  <div className="grid grid-cols-2">
                    <span>PAN STATUS</span>
                    <span>:</span>
                  </div>
                  {verificationData["pan_status"] && (
                    <span>
                      {verificationData["pan_status"].toUpperCase() ||
                        "SUCCESS"}
                    </span>
                  )}
                </div>
              </div>
              <div className="mt-12 w-full px-2 sm:px-6">
                {verificationData["name"] && (
                  <div className="grid grid-cols-2">
                    <div className="grid grid-cols-2">
                      <span>NAME</span>
                      <span>:</span>
                    </div>
                    <span>{verificationData["name"].toUpperCase()}</span>
                  </div>
                )}
                {verificationData["address_line_1"] && (
                  <div className="grid grid-cols-2">
                    <div className="grid grid-cols-2">
                      <span>ADDRESS:</span>
                      <span>:</span>
                    </div>
                    <span>
                      {verificationData["address_line_1"].toUpperCase()}
                    </span>
                  </div>
                )}
                {verificationData["address_line_2"] && (
                  <div className="grid grid-cols-2">
                    <div className="grid grid-cols-2">
                      <span>ADDRESS (2):</span>
                      <span>:</span>
                    </div>
                    <span>
                      {verificationData["address_line_2"].toUpperCase()}
                    </span>
                  </div>
                )}
                {verificationData["area"] && (
                  <div className="grid grid-cols-2">
                    <div className="grid grid-cols-2">
                      <span>AREA</span>
                      <span>:</span>
                    </div>
                    {verificationData["area"] && (
                      <span>{verificationData["area"].toUpperCase()}</span>
                    )}
                  </div>
                )}
                {verificationData["city"] && (
                  <div className="grid grid-cols-2">
                    <div className="grid grid-cols-2">
                      <span>CITY</span>
                      <span>:</span>
                    </div>
                    <span>{verificationData["city"].toUpperCase()}</span>
                  </div>
                )}
                {verificationData["district"] && (
                  <div className="grid grid-cols-2">
                    <div className="grid grid-cols-2">
                      <span>DISTRICT</span>
                      <span>:</span>
                    </div>
                    <span>{verificationData["district"].toUpperCase()}</span>
                  </div>
                )}
                {verificationData["state"] && (
                  <div className="grid grid-cols-2">
                    <div className="grid grid-cols-2">
                      <span>STATE</span>
                      <span>:</span>
                    </div>
                    <span>{verificationData["state"].toUpperCase()}</span>
                  </div>
                )}
                {verificationData["pincode"] && (
                  <div
                    className="grid grid-cols-2"
                    style={{
                      paddingBottom: "2rem",
                      borderBottom: "1px dashed",
                    }}
                  >
                    <div className="grid grid-cols-2">
                      <span>PINCODE</span>
                      <span>:</span>
                    </div>
                    <span>{verificationData["pincode"].toUpperCase()}</span>
                  </div>
                )}
              </div>
            </div>
          )}
          <div className="px-2 sm:px-6">
            <button className="focus:outline-none w-full bg-magenta transition duration-150 ease-in-out hover:bg-j-orange rounded text-white px-8 py-3 text-sm mt-6">
              Verify GSTIN/PAN Status
            </button>
            <p className="text-xs text-center">
              <a
                className="underline"
                onClick={() => {
                  onClick(2);
                }}
              >
                Go Back
              </a>
            </p>
          </div>
        </form>
      </div>
    </section>
  );
};

const StartSignUpComp = ({ onClick }) => {
  let form = useRef(null);

  const handleSignUpSubmit = (event) => {
    event.preventDefault();
    const form_data = new FormData(form.current);
    let payload = {};
    form_data.forEach(function (value, key) {
      payload[key] = value;
    });
    startSignUpAPI(payload, onClick);
  };
  return (
    <section className="grid grid-cols-1 sm:grid-cols-1 md:grid-cols-2 lg:grid-cols-2 gap-1 overflow-auto h-full">
      <div className="h-full hidden sm:block overflow-auto">
        <img src={LoginImage} className={styles.signinsignup} />
      </div>
      <div className="bg-gray-100 {-- h-screen --} flex justify-center lg:items-center h-full overflow-auto">
        <form
          ref={form}
          onSubmit={handleSignUpSubmit}
          className="w-full text-j-magenta p-20"
        >
          <div className="pt-0 px-2 flex flex-col items-center justify-center">
            <img src={LogoImage} className="h-48 text-center" />
          </div>
          <div className="pt-8 px-2 flex flex-col items-center justify-center">
            <h3 className="text-2xl sm:text-3xl xl:text-2xl font-bold leading-tight">
              Sign - Up - Step - 3
            </h3>
          </div>
          <div className="mt-12 w-full px-2 sm:px-6">
            <div>
              <label>Email</label>
              <Input id="email" name="email" type="email" />
            </div>
            <div className="mt-6">
              <label>Name</label>
              <Input id="name" name="name" type="text" />
            </div>
            <div className="mt-6">
              <label>Phone Number</label>
              <Input
                id="phone_number"
                name="phone_number"
                type="number"
              />
            </div>
            <div className="mt-6">
              <label>Password</label>
              <Input
                id="password"
                name="password"
                placeholder="Password"
                type="password"
              />
            </div>
          </div>
          <div className="px-2 sm:px-6 mt-6">
            <button className="focus:outline-none w-full bg-magenta transition duration-150 ease-in-out hover:bg-j-orange rounded text-white px-8 py-3 text-sm mt-6">
              Sign - Up
            </button>
            <p className="mt-16 text-xs text-center">
              <a
                className="underline"
                onClick={() => {
                  onClick(1);
                }}
              >
                Cancel
              </a>
            </p>
          </div>
        </form>
      </div>
    </section>
  );
};

const AddressComp = ({ onClick }) => {
  let form = useRef(null);
  let verificationData = verifyGSTINDataVar;
  const detailsSignUpSubmit = (event) => {
    event.preventDefault();
    const form_data = new FormData(form.current);
    let payload = {};
    form_data.forEach(function (value, key) {
      payload[key] = value;
    });
    AddressCompAPI(payload, onClick);
  };
  return (
    <section className="grid grid-cols-1 sm:grid-cols-1 md:grid-cols-2 lg:grid-cols-2 gap-1 overflow-auto h-full">
      <div className="h-full hidden sm:block overflow-auto">
        <img src={LoginImage} className={styles.signinsignup} />
      </div>
      <div className="bg-gray-100 {-- h-screen --} flex justify-center lg:items-center h-full overflow-auto">
        <form
          ref={form}
          onSubmit={detailsSignUpSubmit}
          className="w-full text-j-magenta p-20"
        >
          <div className="pt-0 px-2 flex flex-col items-center justify-center">
            <img src={LogoImage} className="h-48 text-center" />
          </div>
          <div className="pt-8 px-2 flex flex-col items-center justify-center">
            <h3 className="text-2xl sm:text-3xl xl:text-2xl font-bold leading-tight">
              Input Your Details
            </h3>
          </div>
          {verificationData["pan_flag"] && (
            <div className="mt-12 w-full px-2 sm:px-6">
              <div className="grid grid-cols-2 gap-4">
                <Input
                  id="address_line_1"
                  name="address_line_1"
                  placeholder="Address Line - 1"
                  type="text"
                />
                <Input
                  id="address_line_2"
                  name="address_line_2"
                  placeholder="Address Line - 2"
                  type="text"
                />
              </div>
              <Input id="area" name="area" placeholder="Area" type="text" />
              <div className="grid grid-cols-2 gap-4">
                <Input id="city" name="city" placeholder="City" type="text" />
                <Input
                  id="district"
                  name="district"
                  placeholder="District"
                  type="text"
                />
              </div>
              <div className="grid grid-cols-2 gap-4">
                <Input id="state" name="state" placeholder="State" type="text" />
                <Input
                  id="pincode"
                  name="pincode"
                  placeholder="PINCODE"
                  type="number"
                />
              </div>
              <select
                name="portal_agent_type"
                id="portal_agent_type"
                style={{
                  width: "100%",
                  padding: "0.625rem",
                  borderRadius: "0.625rem",
                  boxShadow:
                    "0 1px 3px 0 rgba(0, 0, 0, 0.1), 0 1px 2px 0 rgba(0, 0, 0, 0.06)",
                  marginTop: "2rem",
                }}
              >
                <option selected value="mice">
                  Mice
                </option>
                <option value="b2b">B2B</option>
                <option value="leisure">Leisure</option>
                <option value="consolidator">Consolidator</option>
                <option value="weddings">Weddings</option>
              </select>
              <select
                name="commission_model"
                id="commission_model"
                style={{
                  width: "100%",
                  padding: "0.625rem",
                  borderRadius: "0.625rem",
                  boxShadow:
                    "0 1px 3px 0 rgba(0, 0, 0, 0.1), 0 1px 2px 0 rgba(0, 0, 0, 0.06)",
                  marginTop: "2rem",
                }}
              >
                <option value="b2b">P2P</option>
                <option value="leisure">P2A</option>
              </select>
              <Input
                id="bank_name"
                name="bank_name"
                placeholder="Bank Name"
                type="text"
              />
              <Input
                id="account_number"
                name="account_number"
                placeholder="Account Number"
                type="number"
              />
              <Input
                id="beneficiary_name"
                name="beneficiary_name"
                placeholder="Beneficiary Name"
                type="text"
              />
              <div className="grid grid-cols-2 gap-4">
                <Input
                  id="ifsc_code"
                  name="ifsc_code"
                  placeholder="IFSC"
                  type="text"
                />
                <Input
                  id="branch_name"
                  name="branch_name"
                  placeholder="Branch Name"
                  type="text"
                />
              </div>
            </div>
          )}
          {!verificationData["pan_flag"] && (
            <div className="mt-12 w-full px-2 sm:px-6">
              <select
                name="portal_agent_type"
                id="portal_agent_type"
                style={{
                  width: "100%",
                  padding: "0.625rem",
                  borderRadius: "0.625rem",
                  boxShadow:
                    "0 1px 3px 0 rgba(0, 0, 0, 0.1), 0 1px 2px 0 rgba(0, 0, 0, 0.06)",
                  marginTop: "2rem",
                }}
              >
                <option selected value="mice">
                  Mice
                </option>
                <option value="b2b">B2B</option>
                <option value="leisure">Leisure</option>
                <option value="consolidator">Consolidator</option>
                <option value="weddings">Weddings</option>
              </select>
              <select
                name="commission_model"
                id="commission_model"
                style={{
                  width: "100%",
                  padding: "0.625rem",
                  borderRadius: "0.625rem",
                  boxShadow:
                    "0 1px 3px 0 rgba(0, 0, 0, 0.1), 0 1px 2px 0 rgba(0, 0, 0, 0.06)",
                  marginTop: "2rem",
                }}
              >
                <option value="b2b">P2P</option>
                <option value="leisure">P2A</option>
              </select>
              <Input
                id="bank_name"
                name="bank_name"
                placeholder="Bank Name"
                type="text"
              />
              <Input
                id="account_number"
                name="account_number"
                placeholder="Account Number"
                type="number"
              />
              <Input
                id="beneficiary_name"
                name="beneficiary_name"
                placeholder="Beneficiary Name"
                type="text"
              />
              <div className="grid grid-cols-2 gap-4">
                <Input
                  id="ifsc_code"
                  name="ifsc_code"
                  placeholder="IFSC"
                  type="text"
                />
                <Input
                  id="branch_name"
                  name="branch_name"
                  placeholder="Branch Name"
                  type="text"
                />
              </div>
            </div>
          )}
          <div className="px-2 sm:px-6">
            <button className="focus:outline-none w-full bg-magenta transition duration-150 ease-in-out hover:bg-j-orange rounded text-white px-8 py-3 text-sm mt-6">
              Continue
            </button>
            <p className="mt-16 text-xs text-center">
              <a
                className="underline"
                onClick={() => {
                  onClick(1);
                }}
              >
                Cancel
              </a>
            </p>
          </div>
        </form>
      </div>
    </section>
  );
};

const VerifySignUpComp = ({ onClick }) => {
  let form = useRef(null);
  const verifySignUpSubmit = (event) => {
    event.preventDefault();
    const form_data = new FormData(form.current);
    let payload = {};
    form_data.forEach(function (value, key) {
      payload[key] = value;
    });
    verifySignUpAPI(payload, onClick);
  };
  return (
    <section className="grid grid-cols-1 sm:grid-cols-1 md:grid-cols-2 lg:grid-cols-2 gap-1 overflow-auto h-full">
      <div className="h-full hidden sm:block overflow-auto">
        <img src={LoginImage} className={styles.signinsignup} />
      </div>
      <div className="bg-gray-100 {-- h-screen --} flex justify-center lg:items-center h-full overflow-auto">
        <form
          ref={form}
          onSubmit={verifySignUpSubmit}
          className="w-full text-j-magenta p-20"
        >
          <div className="pt-0 px-2 flex flex-col items-center justify-center">
            <img src={LogoImage} className="h-48 text-center" />
          </div>
          <div className="pt-8 px-2 flex flex-col items-center justify-center">
            <h3 className="text-2xl sm:text-3xl xl:text-2xl font-bold leading-tight">
              Verify Sign - Up
            </h3>
          </div>
          <div className="mt-12 w-full px-2 sm:px-6">
            <Input
              id="email_otp"
              name="email_otp"
              placeholder="Email OTP"
              type="number"
            />
            <Input
              id="phone_otp"
              name="phone_otp"
              placeholder="Mobile OTP"
              type="number"
            />
          </div>
          <div className="px-2 sm:px-6">
            <button className="focus:outline-none w-full bg-magenta transition duration-150 ease-in-out hover:bg-j-orange rounded text-white px-8 py-3 text-sm mt-6">
              Complete Sign - Up
            </button>
            <p className="mt-16 text-xs text-center">
              <a
                className="underline"
                onClick={() => {
                  onClick(1);
                }}
              >
                Cancel
              </a>
            </p>
          </div>
        </form>
      </div>
    </section>
  );
};

const CheckForgetPasswordComp = ({ onClick }) => {
  let form = useRef(null);

  const checkForgetPassword = (event) => {
    event.preventDefault();
    const form_data = new FormData(form.current);
    let payload = {};
    form_data.forEach(function (value, key) {
      payload[key] = value;
    });
    startForgetPasswordAPI(payload, onClick);
  };

  return (
    <section className="grid grid-cols-1 sm:grid-cols-1 md:grid-cols-2 lg:grid-cols-2 gap-1 overflow-auto h-full">
      <div className="h-full hidden sm:block overflow-auto">
        <img src={LoginImage} className={styles.signinsignup} />
      </div>
      <div className="bg-gray-100 {-- h-screen --} flex justify-center lg:items-center h-full overflow-auto">
        <form
          ref={form}
          onSubmit={checkForgetPassword}
          className="w-full text-j-magenta p-20"
        >
          <div className="pt-0 px-2 flex flex-col items-center justify-center">
            <img src={LogoImage} className="h-48 text-center" />
          </div>
          <div className="pt-8 px-2 flex flex-col items-center justify-center">
            <h3 className="text-2xl sm:text-3xl xl:text-2xl font-bold leading-tight">
              Enter Your Email
            </h3>
          </div>
          <div className="mt-12 w-full px-2 sm:px-6">
            <Input id="email" name="email" placeholder="Email" type="email" />
          </div>
          <div className="pt-6 w-full flex justify-between px-2 sm:px-6">
            <a
              className="text-xs"
              onClick={() => {
                onClick(1);
              }}
            >
              Cancel
            </a>
          </div>
          <div className="px-2 sm:px-6">
            <button className="focus:outline-none w-full bg-magenta transition duration-150 ease-in-out hover:bg-j-orange rounded text-white px-8 py-3 text-sm mt-6">
              Submit
            </button>
          </div>
        </form>
      </div>
    </section>
  );
};

const VerifyForgetPasswordComp = ({ onClick }) => {
  let form = useRef(null);
  const verifyForgetPassword = (event) => {
    event.preventDefault();
    const form_data = new FormData(form.current);
    let payload = {};
    form_data.forEach(function (value, key) {
      payload[key] = value;
    });
    verifyForgetPasswordAPI(payload, onClick);
  };
  return (
    <section className="grid grid-cols-1 sm:grid-cols-1 md:grid-cols-2 lg:grid-cols-2 gap-1 overflow-auto h-full">
      <div className="h-full hidden sm:block overflow-auto">
        <img src={LoginImage} className={styles.signinsignup} />
      </div>
      <div className="bg-gray-100 {-- h-screen --} flex justify-center lg:items-center h-full overflow-auto">
        <form
          ref={form}
          onSubmit={verifyForgetPassword}
          className="w-full text-j-magenta p-20"
        >
          <div className="pt-0 px-2 flex flex-col items-center justify-center">
            <img src={LogoImage} className="h-48 text-center" />
          </div>
          <div className="pt-8 px-2 flex flex-col items-center justify-center">
            <h3 className="text-2xl sm:text-3xl xl:text-2xl font-bold leading-tight">
              Verify Yourself
            </h3>
          </div>
          <div className="mt-12 w-full px-2 sm:px-6">
            <Input
              id="password"
              name="password"
              placeholder="New Password"
              type="password"
            />
            <Input
              id="reset_otp"
              name="reset_otp"
              placeholder="OTP"
              type="number"
            />
          </div>
          <div className="pt-6 w-full flex justify-between px-2 sm:px-6">
            <a
              className="text-xs"
              onClick={() => {
                onClick(1);
              }}
            >
              Cancel
            </a>
          </div>
          <div className="px-2 sm:px-6">
            <button className="focus:outline-none w-full bg-magenta transition duration-150 ease-in-out hover:bg-j-orange rounded text-white px-8 py-3 text-sm mt-6">
              Submit
            </button>
          </div>
        </form>
      </div>
    </section>
  );
};

export default Login;
